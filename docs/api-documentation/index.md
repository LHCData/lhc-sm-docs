!!! note
    Detailed python lhc-sm-api documentation is available at: [https://cern.ch/lhc-sm-api](https://cern.ch/lhc-sm-api)

The API is a collection of five modules for:

1. signal, system, circuit naming (Metadata);
2. signal and event references (Reference);
3. embedded domain specific language for signal/feature/event query and processing, signal assertion, and feature engineering;
4. query, analysis, and plot of signals logged for the LHC hardware (Analysis);
5. graphical user interfaces for browsing of historical signal features (GUI).

<center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-api/raw/master/figures/lhc-sm-api-architecture.png" width="50%"></center>
