# LHC Signal Monitoring API Package
<img src="https://gitlab.cern.ch/LHCData/lhc-sm-api/raw/master/figures/logo.png" width=25%>

This is a package with an API for signal access and processing for the LHC Signal Monitoring project.

## Installation
There are two ways of using the API in your code:

1. Loading preinstalled packages from an EOS project folder (in SWAN environment)
2. Manual installation (in any environment)

The first option guarantees the use of the most recent code version without manual installation. The second one is more time consuming, however, works in environments with no access to the EOS folder (e.g., Apache Airflow scheduler). In addition, the second method allows to install a selected version (`pip install package_name=version`).

### Preinstalled Packages
To use the set of pre-installed packages please follow these three steps:

0. Contact the Signal Monitoring team (mailto:lhc-signal-monitoring@cern.ch) in order to get read access to the EOS folder with pre-installed packages.
1. (optional) Uninstall existing packages.  
Historically, the initial way of installing the packages was by manual installation discussed in Section Manual Installation.
Thus, to avoid double reference to a package, please uninstall (with `pip uninstall package_name`) all packages needed for the API to work (tzlocal, tqdm, influxdb, plotly, lhcsmapi). This operation has to be done only once provided that the packages were installed (to check if a package was installed use `pip list | grep package_name` in SWAN Command Line Interface).
2. While logging to SWAN service, please add the environment script as `/eos/project/l/lhcsm/public/packages.sh`

<img src="https://gitlab.cern.ch/LHCData/lhc-sm-api/raw/master/figures/swan_environment_script.png" width=25%>

### Manual Installation
In order to use the API, it has to be installed with a python package installer as

```python
pip install --user lhcsmapi
```
Check the latest version at <a href="https://pypi.org/project/lhcsmapi/">https://pypi.org/project/lhcsmapi/</a>

The API relies on several external python packages which have to be installed in a similar manner. The list of packages is stored in the <u><i>requirements.txt</i></u> file.

If you use SWAN, the service provides a set of pre-installed python packages through CVMFS. The LHC-SM notebooks require installation of several additional packages on top of CVMFS. In order to install a package, please open a SWAN Terminal by clicking [>_] icon in the top right corner.

![SWAN CLI Button](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/swan-cli-button.png)

Five additional python packages have to be installed:

- tzlocal - for time zone convertion
- tqdm - for progress bar to track queries
- plotly - for interactive plotting of circuit schematics 
- lhcsmapi - for LHC-SM API

In order to install a package please execute the following command
```
$ pip install --user package_name
```

The expected output, after installing all packages, is presented in five figures below.

- SWAN Terminal output after successful installation of tzlocal package.
![SWAN pip install tzlocal](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/swan-pip-install-tzlocal.png)

- SWAN Terminal output after successful installation of tqdm package.
![SWAN pip install tqdm](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/swan-pip-install-tqdm.png)

- SWAN Terminal output after successful installation of influxdb package.
![SWAN pip install influxdb](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/swan-pip-install-influxdb.png)

- SWAN Terminal output after successful installation of plotly package.
![SWAN pip install plotly](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/swan-pip-install-plotly.png)

- SWAN Terminal output after successful installation of lhcsmapi package.
![SWAN pip install lhcsmapi](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/swan-pip-install-lhcsmapi.png)

### Updating lhcsmapi Package

Please note that the first four packages (tzlocal, tqdm, influxdb, plotly) have to be installed only once while the last one is still in the development phase and subject to frequent updates. Please send us an e-mail request (mailto:lhc-signal-monitoring@cern.ch) if you want to subscribe for updates. In order to update the lhcsmapi package, please execute the following command.
```
$ pip install --user --upgrade lhcsmapi
```

### Known Issues
At times, in order to update the lhcsmapi package one has to execute the command
```
pip install --user --upgrade lhcsmapi
```
twice while using the SWAN terminal (cf. an error message in the figure below).
<img src="https://gitlab.cern.ch/LHCData/lhc-sm-api/raw/master/figures/double_reinstallation_error.png">


In case this command returns an error, please try to execute it again. Should that operation also fail, please uninstall the package by executing
```
$ pip uninstall lhcsmapi
```

and performing a fresh installation the package
```
$ pip install --user lhcsmapi
```

Should you experience any further issues with installing a package, please contact <a href="https://swan.web.cern.ch">SWAN support</a> or use the preinstalled package with the environment script.

### NXCALS Access with SWAN
The API allows to perform a query of signals from PM and NXCALS. The NXCALS database requires an assignment of dedicated access rights for a user. 
If you want to query NXCALS with the API, please follow a procedure below on how to request the NXCALS access.

1. Go to http://nxcals-docs.web.cern.ch/current/user-guide/data-access/nxcals-access-request/ for most updated procedure
2. Send an e-mail to mailto:acc-logging-support@cern.ch with the following pieces of information:

 - your NICE username
 - system: WinCCOA, CMW
 - NXCALS environment: PRO
 
Optionally one can mention that the NXCALS database will be accessed through SWAN.
Once the access is granted, you can use NXCALS with SWAN.

# Time
**Time** class provides a set of methods for conversion between various time formats. In addition it provides a generator for days between given periods. Main methods are:

- `Time.to_unix_timestamp(input, unit='ns')`
- `Time.to_pandas_timestamp(input, unit='ns')`
- `Time.to_string(input, unit='ns')`
- `Time.to_datetime(input, unit='ns')`

The unit argument is optional. Its default value is 'ns', i.e., nanosecond.

- `Time.to_unix_timestamp(input, unit='ns')`

This method is useful to obtain a timestamp to query PM database


```python
from lhcsmapi.Time import Time
date_time_str = "2018-05-01 00:00:00"
Time.to_unix_timestamp(date_time_str, unit="ns")
```




    1525125600000000000




```python
from lhcsmapi.Time import Time
from datetime import datetime
date_time_dt = datetime(2018, 5, 1, 0, 0, 0, 0)
Time.to_unix_timestamp(date_time_dt, unit="ns")
```




    1525125600000000000




```python
from lhcsmapi.Time import Time
timestamp = 1525125600000000000
Time.to_unix_timestamp(timestamp, unit="ns")
```




    1525125600000000000



Any method also works without the unit. The default unit is ns.


```python
from lhcsmapi.Time import Time
timestamp = 1525125600000000000
Time.to_unix_timestamp(timestamp)
```




    1525125600000000000




```python
from lhcsmapi.Time import Time
import pandas as pd
timestamp_pd = pd.Timestamp(2018, 5, 1, 0)
Time.to_unix_timestamp(timestamp_pd, unit="ns")
```




    1525125600000000000



- `Time.to_pandas_timestamp(input, unit='ns')`


```python
from lhcsmapi.Time import Time
date_time_str = "2019-07-10 10:23:27.013"
Time.to_pandas_timestamp(date_time_str, unit="ns")
```




    Timestamp('2019-07-10 10:23:27.013000+0200', tz='Europe/Zurich')




```python
from lhcsmapi.Time import Time
from datetime import datetime
date_time_dt = datetime(2019, 7, 10, 10, 23, 27, 13)
Time.to_pandas_timestamp(date_time_dt, unit="ns")
```




    Timestamp('2019-07-10 10:23:27.000013+0200', tz='Europe/Zurich')




```python
from lhcsmapi.Time import Time
timestamp = 1562747007013000000
Time.to_pandas_timestamp(timestamp, unit="ns")
```




    Timestamp('2019-07-10 10:23:27.013000+0200', tz='Europe/Zurich')




```python
from lhcsmapi.Time import Time
import pandas as pd
timestamp_pd = pd.Timestamp(2019, 7, 10, 10, 23, 27, 13000)
Time.to_pandas_timestamp(timestamp_pd, unit="ns")
```




    Timestamp('2019-07-10 10:23:27.013000+0200', tz='Europe/Zurich')



- `Time.to_string(input, unit='ns')`


```python
from lhcsmapi.Time import Time
date_time_str = "2017-07-02 01:23:27"
Time.to_string(date_time_str, unit="ns")
```




    '2017-07-02 01:23:27+02:00'




```python
from lhcsmapi.Time import Time
from datetime import datetime
date_time_dt = datetime(2017, 7, 2, 1, 23, 27, 0)
Time.to_string(date_time_dt, unit="ns")
```




    '2017-07-02 01:23:27+02:00'




```python
from lhcsmapi.Time import Time
timestamp = 1498951407000000000
Time.to_string(timestamp, unit="ns")
```




    '2017-07-02 01:23:27+02:00'




```python
from lhcsmapi.Time import Time
import pandas as pd
timestamp_pd = pd.Timestamp(2018, 5, 1, 1, 23, 27)
Time.to_string(timestamp_pd, unit="ns")
```




    '2018-05-01 01:23:27+02:00'



There is also a version of this method returning a string without the time zone


```python
from lhcsmapi.Time import Time
import pandas as pd
timestamp_pd = pd.Timestamp(2018, 5, 1, 1, 23, 27)
Time.to_string_short(timestamp_pd, unit="ns")
```




    '2018-05-01 01:23:27'



- `Time.to_datetime(input, unit='ns')`


```python
from lhcsmapi.Time import Time
date_time_str = "2019-01-02 23:59:11"
Time.to_datetime(date_time_str, unit="ns")
```




    datetime.datetime(2019, 1, 2, 23, 59, 11, tzinfo=<DstTzInfo 'Europe/Zurich' CET+1:00:00 STD>)




```python
from lhcsmapi.Time import Time
from datetime import datetime
date_time_dt = datetime(2019, 1, 2, 23, 59, 11, 0)
Time.to_datetime(date_time_dt, unit="ns")
```




    datetime.datetime(2019, 1, 2, 23, 59, 11, tzinfo=<DstTzInfo 'Europe/Zurich' CET+1:00:00 STD>)




```python
from lhcsmapi.Time import Time
timestamp = 1546469951000000000
Time.to_datetime(timestamp, unit="ns")
```




    datetime.datetime(2019, 1, 2, 23, 59, 11, tzinfo=<DstTzInfo 'Europe/Zurich' CET+1:00:00 STD>)




```python
from lhcsmapi.Time import Time
import pandas as pd
timestamp_pd = pd.Timestamp(2019, 1, 2, 23, 59, 11, 0)
Time.to_datetime(timestamp_pd, unit="ns")
```




    datetime.datetime(2019, 1, 2, 23, 59, 11, tzinfo=<DstTzInfo 'Europe/Zurich' CET+1:00:00 STD>)



- `Time.to_unix_timestamp_in_sec(input, tz)`


```python
from lhcsmapi.Time import Time
timestamp = 1426220469491000000
Time.to_unix_timestamp_in_sec(timestamp)
```




    1426220469.4910002



- `Time.get_query_period_in_unix_time(t_start, t_end, duration, tz)`

    There are three ways of executing this method:
    - start time and end time


```python
from lhcsmapi.Time import Time
time_date_start = 1426220459491000000
time_date_end = 1426220479491000000
Time.get_query_period_in_unix_time(time_date_start, time_date_end, None, tz='Europe/Zurich')
```




    (1426220459491000000, 1426220479491000000)



   - start time, duration with a single element list to indicate end time after the start time


```python
from lhcsmapi.Time import Time
time_date_start = 1426220459491000000
duration = [(20, 's')]
Time.get_query_period_in_unix_time(time_date_start, None, duration, tz='Europe/Zurich')
```




    (1426220459491000000, 1426220479491000000)



- start time, duration with a two element list to indicate end time befor (1st element) and after (2nd element) the start time


```python
from lhcsmapi.Time import Time
time_date_start = 1426220469491000000
duration =  [(10, 's'), (10, 's')]
Time.get_query_period_in_unix_time(time_date_start, None, duration, tz='Europe/Zurich')
```




    (1426220459491000000, 1426220479491000000)



- `Time.daterange(time_date_start, time_date_end)`

Useful to iterate over days while e.g. sequentially querying PM database for events (there is a limit of 24h query duration in this case).


```python
from datetime import date
time_date_start = date(2014, 1, 1)
time_date_end = date(2014, 1, 3)
for single_date in Time.daterange(time_date_start, time_date_end):
    print(single_date)
```

    2014-01-01
    2014-01-02


In case the start date is later than the end date, the generator does not return output


```python
from datetime import date
time_date_start = date(2014, 1, 5)
time_date_end = date(2014, 1, 3)
for single_date in Time.daterange(time_date_start, time_date_end):
    print(single_date)
```

# Timer
**Timer** class provides a base functionality for measuring code execution time.


```python
from lhcsmapi.Timer import Timer
t = Timer()

with Timer():
    for i in range(1000000):
        i = i+1
```

    Elapsed: 0.165 s.


equivalent to


```python
import time
t = time.time()
for i in range(1000000):
        i = i+1
time.time() - t
```




    0.16869902610778809



# 1. Metadata
The Metadata (`lhcsmapi.metadata`) module contains methods to retrieve various signal and circuit names.  
In order to avoid storing full names and enable updating signal names based on time. A signal hierarchy is encoded with a dictionary and can be accessed through links in the table below

|Circuit type|Hyperlink|
|------------|---------|
|RB          |https://gitlab.cern.ch/LHCData/lhc-sm-api/blob/master/lhcsmapi/metadata/RB_METADATA.json|
|RQ          |https://gitlab.cern.ch/LHCData/lhc-sm-api/blob/master/lhcsmapi/metadata/RQ_METADATA.json|
|IT          |https://gitlab.cern.ch/LHCData/lhc-sm-api/blob/master/lhcsmapi/metadata/IT_METADATA.json|
|IPQ2        |https://gitlab.cern.ch/LHCData/lhc-sm-api/blob/master/lhcsmapi/metadata/IPQ2_METADATA.json|
|IPQ4        |https://gitlab.cern.ch/LHCData/lhc-sm-api/blob/master/lhcsmapi/metadata/IPQ4_METADATA.json|
|IPQ8        |https://gitlab.cern.ch/LHCData/lhc-sm-api/blob/master/lhcsmapi/metadata/IPQ8_METADATA.json|
|IPD2        |https://gitlab.cern.ch/LHCData/lhc-sm-api/blob/master/lhcsmapi/metadata/IPD2_METADATA.json|
|IPD2_B1B2   |https://gitlab.cern.ch/LHCData/lhc-sm-api/blob/master/lhcsmapi/metadata/IPD2_B1B2_METADATA.json|
|60A         |https://gitlab.cern.ch/LHCData/lhc-sm-api/blob/master/lhcsmapi/metadata/60A_METADATA.json|
|80-120A     |https://gitlab.cern.ch/LHCData/lhc-sm-api/blob/master/lhcsmapi/metadata/80-120A_METADATA.json|

### 1.1. Signal Metadata 
**SignalMetadata** class stores information about signal and circuit names as well as corresponding metadata.

- get beam mode description


```python
from lhcsmapi.metadata.SignalMetadata import SignalMetadata

SignalMetadata.get_beam_mode_details()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Mode</th>
      <th>Name</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>NOMODE</td>
      <td>No mode, data is not available, not set</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>SETUP</td>
      <td>Setup</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>INJPILOT</td>
      <td>Pilot injection</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>INJINTR</td>
      <td>Intermediate injection</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>INJNOMN</td>
      <td>Nominal injection</td>
    </tr>
    <tr>
      <th>5</th>
      <td>6</td>
      <td>PRERAMP</td>
      <td>Before ramp</td>
    </tr>
    <tr>
      <th>6</th>
      <td>7</td>
      <td>RAMP</td>
      <td>Ramp</td>
    </tr>
    <tr>
      <th>7</th>
      <td>8</td>
      <td>FLATTOP</td>
      <td>Flat top</td>
    </tr>
    <tr>
      <th>8</th>
      <td>9</td>
      <td>SQUEEZE</td>
      <td>Squeeze</td>
    </tr>
    <tr>
      <th>9</th>
      <td>10</td>
      <td>ADJUST</td>
      <td>Adjust beam on flat top</td>
    </tr>
    <tr>
      <th>10</th>
      <td>11</td>
      <td>STABLE</td>
      <td>Stable beam for physics</td>
    </tr>
    <tr>
      <th>11</th>
      <td>12</td>
      <td>UNSTABLE</td>
      <td>Unstable beam</td>
    </tr>
    <tr>
      <th>12</th>
      <td>13</td>
      <td>BEAMDUMP</td>
      <td>Beam dump</td>
    </tr>
    <tr>
      <th>13</th>
      <td>14</td>
      <td>RAMPDOWN</td>
      <td>Ramp down</td>
    </tr>
    <tr>
      <th>14</th>
      <td>15</td>
      <td>RECOVERY</td>
      <td>Recovering</td>
    </tr>
    <tr>
      <th>15</th>
      <td>16</td>
      <td>INJDUMP</td>
      <td>Inject and dump</td>
    </tr>
    <tr>
      <th>16</th>
      <td>17</td>
      <td>CIRCDUMP</td>
      <td>Circulate and dump</td>
    </tr>
    <tr>
      <th>17</th>
      <td>18</td>
      <td>ABORT</td>
      <td>Recovery after a beam permit flag drop</td>
    </tr>
    <tr>
      <th>18</th>
      <td>19</td>
      <td>CYCLING</td>
      <td>Pre-cycle before injection, no beam</td>
    </tr>
    <tr>
      <th>19</th>
      <td>20</td>
      <td>WBDUMP</td>
      <td>Warning beam dump</td>
    </tr>
    <tr>
      <th>20</th>
      <td>21</td>
      <td>NOBEAM</td>
      <td>No beam or preparation for beam</td>
    </tr>
  </tbody>
</table>
</div>



- get circuit types


```python
from lhcsmapi.metadata.SignalMetadata import SignalMetadata

SignalMetadata.get_circuit_types()
```




    ['RB',
     'RQ',
     'IT',
     'IPD2_B1B2',
     'IPD2',
     'IPQ2',
     'IPQ4',
     'IPQ8',
     '600A',
     '60A',
     '80-120A']



- get circuit names


```python
from lhcsmapi.metadata.SignalMetadata import SignalMetadata

SignalMetadata.get_circuit_names('RB')
```




    ['RB.A12',
     'RB.A23',
     'RB.A34',
     'RB.A45',
     'RB.A56',
     'RB.A67',
     'RB.A78',
     'RB.A81']



- get signal name for PM


```python
from lhcsmapi.metadata.SignalMetadata import SignalMetadata

SignalMetadata.get_signal_name('RB', 'RB.A12', 'PC', 'PM', 'I_MEAS')
```




    'STATUS.I_MEAS'



- get signal name for NXCALS


```python
from lhcsmapi.metadata.SignalMetadata import SignalMetadata

SignalMetadata.get_signal_name('RB', 'RB.A12', 'PC', 'NXCALS', 'I_MEAS')
```




    'I_MEAS'



- get metadata for PM


```python
from lhcsmapi.metadata.SignalMetadata import SignalMetadata

SignalMetadata.get_circuit_signal_database_metadata('RB', 'RB.A12', 'PC', 'PM')
```




    {'system': 'FGC',
     'source': 'RPTE.UA23.RB.A12',
     'className': '51_self_pmd',
     'I_REF': 'STATUS.I_REF',
     'I_MEAS': 'STATUS.I_MEAS',
     'V_REF': 'STATUS.V_REF',
     'V_MEAS': 'STATUS.V_MEAS',
     'I_EARTH': 'IEARTH.IEARTH',
     'I_EARTH_PCNT': 'STATUS.I_EARTH_PCNT',
     'I_A': 'IAB.I_A',
     'I_B': 'IAB.I_B',
     'ACTION': 'EVENTS.ACTION',
     'PROPERTY': 'EVENTS.PROPERTY',
     'SYMBOL': 'EVENTS.SYMBOL'}



- get metadata for NXCALS


```python
from lhcsmapi.metadata.SignalMetadata import SignalMetadata

SignalMetadata.get_circuit_signal_database_metadata('RB', 'RB.A12', 'PC', 'NXCALS')
```




    {'system': 'CMW',
     'device': 'RPTE.UA23.RB.A12',
     'property': 'SUB',
     'I_MEAS': 'I_MEAS',
     'I_EARTH_MA': 'I_EARTH_MA',
     'I_REF': 'I_REF',
     'I_ERR_MA': 'I_ERR_MA'}



**SignalMetadata** is parameterized with time in order to account for differences across the metadata. E.g., during LS2 there was an upgrade of RQ QPS. With the upgrade the current is also logged in PM (before it was not done; cf. error below).


```python
from lhcsmapi.metadata.SignalMetadata import SignalMetadata

SignalMetadata.get_signal_name('RQ', 'RQD.A12', 'QH', 'PM', 'I_HDS', timestamp_query='2018-01-01 00:00:00')
```


    ---------------------------------------------------------------------------

    KeyError                                  Traceback (most recent call last)

    <ipython-input-151-ddfcc0e656b0> in <module>()
          1 from lhcsmapi.metadata.SignalMetadata import SignalMetadata
          2 
    ----> 3 SignalMetadata.get_signal_name('RQ', 'RQD.A12', 'QH', 'PM', 'I_HDS', timestamp_query='2018-01-01 00:00:00')
    

    /eos/project/l/lhcsm/venv/lhcsmapi/metadata/SignalMetadata.py in get_signal_name(circuit_type, circuit_name, system, database, signal, timestamp_query)
        376             for vn in signal:
        377                 var_names = SignalMetadata. \
    --> 378                     _update_wildcards_in_signal_name_per_circuit_name(circuit_type, cn, database, system, vn, timestamp_query)
        379 
        380                 if isinstance(var_names, list):


    /eos/project/l/lhcsm/venv/lhcsmapi/metadata/SignalMetadata.py in _update_wildcards_in_signal_name_per_circuit_name(circuit_type, circuit_name, database, system, signal, timestamp_query)
        390         circuit_signal_type_db_metadata = SignalMetadata. \
        391             get_circuit_signal_database_metadata(circuit_type, circuit_name, system, database, timestamp_query)
    --> 392         signal = circuit_signal_type_db_metadata[signal]
        393         # if signal is a list
        394         if isinstance(signal, list):


    KeyError: 'I_HDS'



```python
from lhcsmapi.metadata.SignalMetadata import SignalMetadata

SignalMetadata.get_signal_name('RQ', 'RQD.A12', 'QH', 'PM', 'I_HDS', timestamp_query='2021-01-10 00:00:00')
```




    ['%CELL%:I_HDS_1', '%CELL%:I_HDS_2']



### 1.2. Mapping Metadata
Some signal names obtained with SignalMetadata functions have a wildcard in order to save space and exploit signal naming convention.  
**MappingMetadata** class stores information about circuit topology, e.g., order and names of magnets in a particular circuit.  
There is a collection of csv files containing circuit topology summarised in the table below.

|System type|Hyperlink|
|------------|---------|
|beam mode   |https://gitlab.cern.ch/LHCData/lhc-sm-api/tree/master/lhcsmapi/metadata/beam_mode|
|busbar      |https://gitlab.cern.ch/LHCData/lhc-sm-api/tree/master/lhcsmapi/metadata/busbar|
|magnet      |https://gitlab.cern.ch/LHCData/lhc-sm-api/tree/master/lhcsmapi/metadata/magnet|
|qps_crate   |https://gitlab.cern.ch/LHCData/lhc-sm-api/tree/master/lhcsmapi/metadata/qps_crate|

- QPS crate name for a magnet 


```python
from lhcsmapi.metadata.MappingMetadata import MappingMetadata

MappingMetadata.get_crate_name_from_magnet_name('RB', 'MB.A16L2')
```




    'B16L2'



- QPS crates for RB


```python
from lhcsmapi.metadata.MappingMetadata import MappingMetadata

MappingMetadata.get_crates_for_circuit_names('RB', 'RB.A12')
```




    ['B8R1',
     'B9R1',
     'B10R1',
     'B11R1',
     'B12R1',
     'B13R1',
     'B14R1',
     'B15R1',
     'B16R1',
     'B17R1',
     'B18R1',
     'B19R1',
     'B20R1',
     'B21R1',
     'B22R1',
     'B23R1',
     'B24R1',
     'B25R1',
     'B26R1',
     'B27R1',
     'B28R1',
     'B29R1',
     'B30R1',
     'B31R1',
     'B32R1',
     'B33R1',
     'B34R1',
     'B34L2',
     'B33L2',
     'B32L2',
     'B31L2',
     'B30L2',
     'B29L2',
     'B28L2',
     'B27L2',
     'B26L2',
     'B25L2',
     'B24L2',
     'B23L2',
     'B22L2',
     'B21L2',
     'B20L2',
     'B19L2',
     'B18L2',
     'B17L2',
     'B16L2',
     'B15L2',
     'B14L2',
     'B13L2',
     'B12L2',
     'B11L2',
     'B10L2',
     'B9L2',
     'B8L2']



# 2. Reference
The Reference module stores information about reference signal profiles and features.

|System type|Hyperlink|
|-----------------|---------|
|Energy Extraction|https://gitlab.cern.ch/LHCData/lhc-sm-api/tree/master/lhcsmapi/reference/ee|
|Current Leads    |https://gitlab.cern.ch/LHCData/lhc-sm-api/tree/master/lhcsmapi/reference/leads|
|Power Converter  |https://gitlab.cern.ch/LHCData/lhc-sm-api/tree/master/lhcsmapi/reference/pc|
|Quench Heaters   |https://gitlab.cern.ch/LHCData/lhc-sm-api/tree/master/lhcsmapi/reference/qh|

References useful for analysis, so that we can measure deviation of signals.  
References change over time, so we need to keep track of the changes.

- Get Reference Quench Heater Discharge Timestamp


```python
from lhcsmapi.reference.Reference import Reference

Reference.get_quench_heater_reference_discharge('RQ', '16L2')
```




    1417079205409000000



- Get Reference Power Converter Timestamp


```python
from lhcsmapi.reference.Reference import Reference

Reference.get_power_converter_reference_fpa('RQ', 'RQD.A12', 'fgcPm')
```




    '2018-03-18 11:58:33.600'



- Get Reference Energy Extraction Timestamp


```python
from lhcsmapi.reference.Reference import Reference

Reference.get_power_converter_reference_fpa('RQ', 'RQD.A12', 'eePm')
```




    '2018-03-18 11:58:33.675'



- Get Reference Energy Extraction Parameters


```python
from lhcsmapi.reference.Reference import Reference

Reference.read_energy_extraction_reference_features('RB', '2018-03-18 11:58:33.675')
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>min</th>
      <th>max</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>R_odd</th>
      <td>0.0675</td>
      <td>0.0825</td>
    </tr>
    <tr>
      <th>R_even</th>
      <td>0.0675</td>
      <td>0.0825</td>
    </tr>
    <tr>
      <th>t_delay_ee_odd</th>
      <td>0.0500</td>
      <td>0.1500</td>
    </tr>
    <tr>
      <th>t_delay_ee_even</th>
      <td>0.5500</td>
      <td>0.6500</td>
    </tr>
    <tr>
      <th>tau_u_dump_res_odd</th>
      <td>110.0000</td>
      <td>130.0000</td>
    </tr>
    <tr>
      <th>tau_u_dump_res_even</th>
      <td>110.0000</td>
      <td>130.0000</td>
    </tr>
  </tbody>
</table>
</div>




```python
from lhcsmapi.reference.Reference import Reference

Reference.read_energy_extraction_reference_features('RQ', '2018-03-18 11:58:33.675')
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>min</th>
      <th>max</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>R</th>
      <td>0.005</td>
      <td>0.010</td>
    </tr>
    <tr>
      <th>t_delay_ee</th>
      <td>0.085</td>
      <td>0.115</td>
    </tr>
    <tr>
      <th>tau_u_dump_res</th>
      <td>25.000</td>
      <td>35.000</td>
    </tr>
  </tbody>
</table>
</div>



# 3. pyeDSL
The pyeDSL module builds on top of metadata and reference allowing for a simplified signal query, processing, plotting, and feature engineering. It is divided into two modules: `dbsignal` (with AFT, PM, and NXCALS low-level API), `dbquery` for creation of queries with pyeDSL. In addition, there are several classes implementing an embedded Domain Specific Language in python (pyeDSL).

# 3.0. Low-Level API

We begin with an observation that there are several logging databases for the hardware used in the LHC accelerator.

First of all, for ad-hoc queries and exploration for signal hierarchy we recommend general purpose browsers:

- The new PM web browser is available at the following link: http://post-mortem-paas.web.cern.ch/
- The LabVIEW PM browser
- The new TIMBER NXCALS web browser is available at: https://timber.cern.ch/
- In order to find the mapping between CALS variables and NXCALS variable and metadata (along with the data migration status), please consult: https://ccde.cern.ch/dataBrowser/search?acwReportName=CALS-to-NXCALS%20Data%20Migration&domainName=CALS


In order to programatically query PM, CALS, or NXCALS database, a dedicated API along with parameters is required. An overview is summarised in the table below.

| &nbsp;          | PM                        | PM                                | CALS*         | CALS*            | NXCALS                             | NXCALS                                       |
|-----------------|---------------------------|-----------------------------------|--------------|------------------|------------------------------------|----------------------------------------------|
| query type      | event                     | signal                            | signal       | feature          | signal                             | feature                                      |
| input           | system, source, className | system, source, className, signal | signal       | signal, features | system, (device, property), signal | system, (device, property), signal, features |
| time definition | time range                | timestamp                         | time range   | time range       | time range                         | time range                                   |
| time unit       | ns                        | ns                                | s            | s                | ns                                 | ns                                           |
| return type     | json                      | json                              | dict of list | dict of list     | spark dataframe                    | spark dataframe                              |
| execution time  | fast                      | fast                              | can be slow  | fast             | slow                               | fast                                         |
| execution type  | serial                    | serial                            | serial       | ?                | serial                             | parallel                                     |
| use             | simple                    | simple                            | simple       | simple           | simple                             | requires good knowledge of spark             |


\*CALS was used during Run1 and Run2 to log accelerator data. It was replaced by NXCALS as of HWC 2021.

The DbSignal provides a unified access to PM, CALS, and NXCALS databases in order to return a common data format, a pandas DataFrame. This enables a more advanced signal processing capabilities. The DbSignal module uses native API for each database:
- PM REST API: http://pm-api-pro/
- NXCALS python API: http://nxcals-docs.web.cern.ch/0.4.11/

Furthermore, we support Accelerator Fault Tracking (AFT) database.

### 3.0.1. Low-level PM signal query


```python
import requests
import json
import pandas as pd

response = requests.get('http://pm-api-pro/v2/pmdata/signal?system=FGC&className=51_self_pmd&source=RPTE.UA47.RB.A45&timestampInNanos=1426220469520000000&signal=STATUS.I_MEAS')
json_response = json.loads(response.text)

name = json_response['content'][0]['namesAndValues'][0]['name']
time = json_response['content'][0]['namesAndValues'][1]['value']
value = json_response['content'][0]['namesAndValues'][0]['value']

pd.DataFrame(value, time, [name]).plot()
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fdcc88f9278>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_78_1.png)
    


### 3.0.2. Low-level NXCALS signal query


```python
from cern.nxcals.pyquery.builders import *
import pandas as pd

i_meas_df = DevicePropertyQuery.builder(spark).system('CMW') \
    .startTime(pd.Timestamp('2015-03-13T04:20:59.491000000').to_datetime64())\
    .endTime(pd.Timestamp('2015-03-13T04:22:49.491000000')) \
    .entity().device('RPTE.UA47.RB.A45').property('SUB') \
    .buildDataset().select('acqStamp', 'I_MEAS').dropna().sort("acqStamp").toPandas()

i_meas_df.set_index(i_meas_df['acqStamp'], inplace=True)
i_meas_df.drop(columns='acqStamp', inplace=True)
i_meas_df.plot()
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fdcc816beb8>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_80_1.png)
    


### 3.0.3. Low-level AFT context and fault query


```python
import pandas as pd
import requests
import getpass
```


```python
user = 'mmacieje'
password = getpass.getpass('Type your NICE password: ')

# we start sessions
session = requests.Session()
 
# we authenticate to get token
authentication = session.post('https://aft.cern.ch/api/acw/login', json={'login': user, 'password': password}, verify=False)
 
# request status
print(authentication.status_code)
 
# list of cookies - contains ACW token
print(authentication.cookies.get_dict())
```

    Type your NICE password: ········


    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)
    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)


    200
    {'JSESSIONID': '1DF95351C1B574C856A9A6224187580A'}


1. `/api/public/v1/accelerators`

Retrieve all available accelerators


```python
accelerators = session.get('https://aft.cern.ch/api/public/v1/accelerators', verify=False)
pd.DataFrame(accelerators.json())
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>ISO_HRS</td>
      <td>ISOLDE HRS</td>
    </tr>
    <tr>
      <th>1</th>
      <td>LINAC4</td>
      <td>LINAC4</td>
    </tr>
    <tr>
      <th>2</th>
      <td>CLEAR</td>
      <td>CLEAR</td>
    </tr>
    <tr>
      <th>3</th>
      <td>LEIR</td>
      <td>LEIR</td>
    </tr>
    <tr>
      <th>4</th>
      <td>ISO_GPS</td>
      <td>ISOLDE GPS</td>
    </tr>
    <tr>
      <th>5</th>
      <td>SPS</td>
      <td>SPS</td>
    </tr>
    <tr>
      <th>6</th>
      <td>LINAC2</td>
      <td>LINAC2</td>
    </tr>
    <tr>
      <th>7</th>
      <td>AD</td>
      <td>AD</td>
    </tr>
    <tr>
      <th>8</th>
      <td>ELENA</td>
      <td>ELENA</td>
    </tr>
    <tr>
      <th>9</th>
      <td>LINAC3</td>
      <td>LINAC3</td>
    </tr>
    <tr>
      <th>10</th>
      <td>PS</td>
      <td>PS</td>
    </tr>
    <tr>
      <th>11</th>
      <td>PSB</td>
      <td>PSB</td>
    </tr>
    <tr>
      <th>12</th>
      <td>LHC</td>
      <td>LHC</td>
    </tr>
    <tr>
      <th>13</th>
      <td>ISO_REX_HIE</td>
      <td>ISOLDE REX-HIE</td>
    </tr>
  </tbody>
</table>
</div>



2. `/api/public/v1/accelerators/{acceleratorId}/properties`

Retrieve the properties which are specific to the given accelerator.


```python
accelerators_lhc = session.get('https://aft.cern.ch/api/public/v1/accelerators/%s/properties' % 'LHC', verify=False)
pd.DataFrame(accelerators_lhc.json())
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>group</th>
      <th>id</th>
      <th>name</th>
      <th>pattern</th>
      <th>values</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>LHC Fault Context</td>
      <td>303161</td>
      <td>Beam Mode</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>1</th>
      <td>LHC Fault Context</td>
      <td>303160</td>
      <td>Operational Mode</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Impact</td>
      <td>193273</td>
      <td>Prevents Injection</td>
      <td>None</td>
      <td>[No, Yes]</td>
    </tr>
    <tr>
      <th>3</th>
      <td>R2E Status</td>
      <td>266828</td>
      <td>R2E Status</td>
      <td>None</td>
      <td>[R2E_CANDIDATE, NOT_R2E_RELATED, R2E_REJECTED,...</td>
    </tr>
    <tr>
      <th>4</th>
      <td>LHC Fault Context</td>
      <td>303158</td>
      <td>Accelerator Mode</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>5</th>
      <td>LHC Fault Context</td>
      <td>303163</td>
      <td>Time in Fill (ms)</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>6</th>
      <td>LHC Fault Context</td>
      <td>303166</td>
      <td>Injection Scheme</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>7</th>
      <td>LHC Fault Context</td>
      <td>303164</td>
      <td>Time in Beam Mode</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>8</th>
      <td>Impact</td>
      <td>193272</td>
      <td>Precycle Needed</td>
      <td>None</td>
      <td>[No, Yes]</td>
    </tr>
    <tr>
      <th>9</th>
      <td>LHC Fault Context</td>
      <td>303159</td>
      <td>Fill No</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>10</th>
      <td>Impact</td>
      <td>193271</td>
      <td>RP Needed</td>
      <td>None</td>
      <td>[No, Yes]</td>
    </tr>
    <tr>
      <th>11</th>
      <td>LHC Fault Context</td>
      <td>303165</td>
      <td>Time in Beam Mode (ms)</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>12</th>
      <td>LHC Fault Context</td>
      <td>303162</td>
      <td>Time in Fill</td>
      <td>None</td>
      <td>[]</td>
    </tr>
  </tbody>
</table>
</div>



3. `/api/public/v1/accelerators/{acceleratorId}/systems`

Retrieve the systems of the given accelerator.


```python
accelerators_lhc = session.get('https://aft.cern.ch/api/public/v1/accelerators/%s/systems' % 'LHC', verify=False)
pd.DataFrame(accelerators_lhc.json())
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>children</th>
      <th>id</th>
      <th>name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>[{'id': 31395, 'name': 'Access Infrastructure ...</td>
      <td>83803</td>
      <td>Access Infrastructure</td>
    </tr>
    <tr>
      <th>1</th>
      <td>[{'id': 371383, 'name': 'Machine Interlock Sys...</td>
      <td>14</td>
      <td>Machine Interlock Systems</td>
    </tr>
    <tr>
      <th>2</th>
      <td>[]</td>
      <td>16739</td>
      <td>Beam-induced Quench</td>
    </tr>
    <tr>
      <th>3</th>
      <td>[{'id': 68, 'name': 'Cryogenics » Equipment', ...</td>
      <td>16</td>
      <td>Cryogenics</td>
    </tr>
    <tr>
      <th>4</th>
      <td>[{'id': 30655, 'name': 'LBDS » TCDQ', 'childre...</td>
      <td>10</td>
      <td>LBDS</td>
    </tr>
    <tr>
      <th>5</th>
      <td>[{'id': 55, 'name': 'Collimation » Interlocks ...</td>
      <td>13</td>
      <td>Collimation</td>
    </tr>
    <tr>
      <th>6</th>
      <td>[{'id': 89, 'name': 'Transverse Damper » Contr...</td>
      <td>21</td>
      <td>Transverse Damper</td>
    </tr>
    <tr>
      <th>7</th>
      <td>[{'id': 31171, 'name': 'Cooling and Ventilatio...</td>
      <td>83789</td>
      <td>Cooling and Ventilation</td>
    </tr>
    <tr>
      <th>8</th>
      <td>[{'id': 86, 'name': 'Radio Frequency » Hardwar...</td>
      <td>20</td>
      <td>Radio Frequency</td>
    </tr>
    <tr>
      <th>9</th>
      <td>[{'id': 106, 'name': 'SIS » Controls', 'childr...</td>
      <td>26</td>
      <td>SIS</td>
    </tr>
    <tr>
      <th>10</th>
      <td>[{'id': 107, 'name': 'QPS » Controller', 'chil...</td>
      <td>27</td>
      <td>QPS</td>
    </tr>
    <tr>
      <th>11</th>
      <td>[{'id': 79, 'name': 'Beam Injection » Oscillat...</td>
      <td>18</td>
      <td>Beam Injection</td>
    </tr>
    <tr>
      <th>12</th>
      <td>[{'id': 29456, 'name': 'Beam Exciters » Apertu...</td>
      <td>29453</td>
      <td>Beam Exciters</td>
    </tr>
    <tr>
      <th>13</th>
      <td>[{'id': 21232, 'name': 'Injection Systems » MK...</td>
      <td>21231</td>
      <td>Injection Systems</td>
    </tr>
    <tr>
      <th>14</th>
      <td>[{'id': 66, 'name': 'Accelerator Controls » CM...</td>
      <td>15</td>
      <td>Accelerator Controls</td>
    </tr>
    <tr>
      <th>15</th>
      <td>[{'id': 45, 'name': 'Beam Losses » Other', 'ch...</td>
      <td>11</td>
      <td>Beam Losses</td>
    </tr>
    <tr>
      <th>16</th>
      <td>[{'id': 47, 'name': 'Beam Instrumentation » BP...</td>
      <td>12</td>
      <td>Beam Instrumentation</td>
    </tr>
    <tr>
      <th>17</th>
      <td>[{'id': 31267, 'name': 'Electrical Network » B...</td>
      <td>83823</td>
      <td>Electrical Network</td>
    </tr>
    <tr>
      <th>18</th>
      <td>[{'id': 84, 'name': 'Operation » Operational e...</td>
      <td>19</td>
      <td>Operation</td>
    </tr>
    <tr>
      <th>19</th>
      <td>[{'id': 73, 'name': 'Experiments » ATLAS', 'ch...</td>
      <td>17</td>
      <td>Experiments</td>
    </tr>
    <tr>
      <th>20</th>
      <td>[]</td>
      <td>83801</td>
      <td>Ventilation Doors</td>
    </tr>
    <tr>
      <th>21</th>
      <td>[{'id': 101, 'name': 'Injector Complex » Beam ...</td>
      <td>24</td>
      <td>Injector Complex</td>
    </tr>
    <tr>
      <th>22</th>
      <td>[{'id': 21228, 'name': 'Magnet circuits » Eart...</td>
      <td>16738</td>
      <td>Magnet circuits</td>
    </tr>
    <tr>
      <th>23</th>
      <td>[{'id': 32132, 'name': 'IT Services » Network'...</td>
      <td>32124</td>
      <td>IT Services</td>
    </tr>
    <tr>
      <th>24</th>
      <td>[]</td>
      <td>29</td>
      <td>Other</td>
    </tr>
    <tr>
      <th>25</th>
      <td>[{'id': 31, 'name': 'Access System » Controls'...</td>
      <td>9</td>
      <td>Access System</td>
    </tr>
    <tr>
      <th>26</th>
      <td>[{'id': 95, 'name': 'Orbit » Reference', 'chil...</td>
      <td>22</td>
      <td>Orbit</td>
    </tr>
    <tr>
      <th>27</th>
      <td>[]</td>
      <td>28</td>
      <td>Power Converters</td>
    </tr>
    <tr>
      <th>28</th>
      <td>[{'id': 29452, 'name': 'Access Management » Ac...</td>
      <td>29449</td>
      <td>Access Management</td>
    </tr>
    <tr>
      <th>29</th>
      <td>[{'id': 103, 'name': 'Vacuum » Hardware', 'chi...</td>
      <td>25</td>
      <td>Vacuum</td>
    </tr>
  </tbody>
</table>
</div>



4. `/api/public/v1/faults`

Search for faults fulfilling the given criteria.


```python
params = {'acceleratorId': 'LHC', 
          'accessNeeded': True, 
          'startTime': '2016-01-13T00:00:00Z', 
          'endTime': '2016-05-13T00:00:00Z'}
pd.DataFrame(session.get(url='https://aft.cern.ch/api/public/v1/faults?', params=params).json())
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>acceleratorName</th>
      <th>acceleratorPropertyInstances</th>
      <th>accessNeeded</th>
      <th>description</th>
      <th>displayLabel</th>
      <th>duration</th>
      <th>effectiveDuration</th>
      <th>endTime</th>
      <th>faultyElementNames</th>
      <th>id</th>
      <th>labelNames</th>
      <th>parentId</th>
      <th>parentSystemName</th>
      <th>reviewedByAwg</th>
      <th>reviewedByExpert</th>
      <th>startTime</th>
      <th>stateChanges</th>
      <th>systemName</th>
      <th>systemPropertyInstances</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Fill', 'value': '02...</td>
      <td>False</td>
      <td>access sectors 4,5,6,7 and LHCb indicate "blue...</td>
      <td>None</td>
      <td>4890000</td>
      <td>4890000</td>
      <td>2016-05-12T16:06:12Z</td>
      <td>[patrols lost]</td>
      <td>26594</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-12T14:44:42Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-1...</td>
      <td>Access System » Hardware</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>1</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Injection Scheme', 'value':...</td>
      <td>False</td>
      <td>electrical problem on switchboard</td>
      <td>None</td>
      <td>10123000</td>
      <td>10123000</td>
      <td>2016-05-12T15:10:42Z</td>
      <td>[RQ9.L2B1]</td>
      <td>26599</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-12T12:21:59Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-1...</td>
      <td>Electrical Network » Distribution » 400 kV</td>
      <td>[{'propertyName': 'TI Fault Type', 'value': 'G...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Beam Mode (ms)', 'v...</td>
      <td>False</td>
      <td>Ventilation doors in IP7, they do not close, n...</td>
      <td>None</td>
      <td>3011000</td>
      <td>3011000</td>
      <td>2016-05-06T13:23:42Z</td>
      <td>[Ventilation doors in IP7]</td>
      <td>26462</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-06T12:33:31Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-0...</td>
      <td>Ventilation Doors</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>3</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Fill No', 'value': '4885'},...</td>
      <td>False</td>
      <td>RQT13.R7B1 water problem</td>
      <td>None</td>
      <td>18288000</td>
      <td>18288000</td>
      <td>2016-05-06T12:33:31Z</td>
      <td>[RQT13.R7B1, RPMBA.RR77.RQT13.R7B1]</td>
      <td>26459</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-06T07:28:43Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-0...</td>
      <td>Power Converters</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>4</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Accelerator Mode', 'value':...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>12347000</td>
      <td>12347000</td>
      <td>2016-05-06T07:28:43Z</td>
      <td>[ROF.A45B1]</td>
      <td>26456</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-06T04:02:56Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-0...</td>
      <td>QPS » Controller</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>5</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Beam Mode', 'value': 'INJPR...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>9298000</td>
      <td>9298000</td>
      <td>2016-05-05T20:15:30Z</td>
      <td>[RPMBB.UA63.RSF1.A56B1, RSF1.A56B1]</td>
      <td>26450</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-05T17:40:32Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-0...</td>
      <td>Power Converters</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>6</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Beam Mode', 'value': 'INJPR...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>3817000</td>
      <td>3817000</td>
      <td>2016-05-05T15:39:44Z</td>
      <td>[RPMBB.UA63.RSF1.A56B1, RSF1.A56B1]</td>
      <td>26444</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-05T14:36:07Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-0...</td>
      <td>Power Converters</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>7</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Fill', 'value': '05...</td>
      <td>False</td>
      <td>cannot close door R74</td>
      <td>None</td>
      <td>6114000</td>
      <td>6114000</td>
      <td>2016-05-05T09:52:50Z</td>
      <td>[IP7 enclosure door]</td>
      <td>26441</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-05T08:10:56Z</td>
      <td>[{'stateId': 'NON_BLOCKING_OP', 'time': '2016-...</td>
      <td>Ventilation Doors</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>8</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Beam Mode', 'value'...</td>
      <td>False</td>
      <td>opened while in beam ON</td>
      <td>None</td>
      <td>16471000</td>
      <td>16471000</td>
      <td>2016-04-26T13:39:48Z</td>
      <td>[UL44]</td>
      <td>26260</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-04-26T09:05:17Z</td>
      <td>[{'stateId': 'NON_BLOCKING_OP', 'time': '2016-...</td>
      <td>Ventilation Doors</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>9</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Beam Mode', 'value'...</td>
      <td>False</td>
      <td>The door opened because the closing mechanism ...</td>
      <td>None</td>
      <td>5433000</td>
      <td>5433000</td>
      <td>2016-04-12T07:57:09Z</td>
      <td>[door YCPZ01=PM25]</td>
      <td>25760</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-04-12T06:26:36Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-04-1...</td>
      <td>Access Management » Patrol Lost</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>10</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Beam Mode', 'value'...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>3091000</td>
      <td>3091000</td>
      <td>2016-04-08T09:07:23Z</td>
      <td>[TCL.5R1B1]</td>
      <td>25662</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-04-08T08:15:52Z</td>
      <td>[{'stateId': 'NON_BLOCKING_OP', 'time': '2016-...</td>
      <td>Collimation » Controls</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>11</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Operational Mode', 'value':...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>80000</td>
      <td>80000</td>
      <td>2016-04-07T16:04:39Z</td>
      <td>[RU.L4]</td>
      <td>25940</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-04-07T16:03:19Z</td>
      <td>[{'stateId': 'NON_BLOCKING_OP', 'time': '2016-...</td>
      <td>QPS » Controller</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>12</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Accelerator Mode', 'value':...</td>
      <td>False</td>
      <td>RCBYV4.R1B1: Vin DC over voltage\nRQ7.R1 (AC f...</td>
      <td>None</td>
      <td>14998000</td>
      <td>14998000</td>
      <td>2016-04-01T05:16:05Z</td>
      <td>[RPLB.RR17.RCBYV4.R1B1, RPHGA.RR17.RQ7.R1B2, R...</td>
      <td>25500</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-04-01T01:06:07Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-04-0...</td>
      <td>Power Converters</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>13</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Beam Mode', 'value': 'SQUEE...</td>
      <td>False</td>
      <td>RB.A34 warm cable cooling (interlock on water ...</td>
      <td>None</td>
      <td>7654000</td>
      <td>7654000</td>
      <td>2016-03-30T22:44:40Z</td>
      <td>[RB.A34]</td>
      <td>25460</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-03-30T20:37:06Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-03-3...</td>
      <td>Cooling and Ventilation » Cooling » Deminerali...</td>
      <td>[{'propertyName': 'TI Major Event Id', 'value'...</td>
    </tr>
    <tr>
      <th>14</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Injection Scheme', 'value':...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>17483000</td>
      <td>17483000</td>
      <td>2016-03-28T08:00:42Z</td>
      <td>[RPHFC.UA83.RQX.L8, RQX.L8]</td>
      <td>25478</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-03-28T03:09:19Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-03-2...</td>
      <td>Power Converters</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>15</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Injection Scheme', 'value':...</td>
      <td>False</td>
      <td>broken cable on thermo switch</td>
      <td>None</td>
      <td>10820000</td>
      <td>10820000</td>
      <td>2016-03-25T19:00:15Z</td>
      <td>[RD34.LR3]</td>
      <td>25380</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-03-25T15:59:55Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-03-2...</td>
      <td>Magnet circuits » Other</td>
      <td>[]</td>
    </tr>
  </tbody>
</table>
</div>



5. `/api/public/v1/faults/{faultId}`

Retrieve one particular fault.


```python
faults_id = session.get('https://aft.cern.ch/api/public/v1/faults/%d' % 26594, verify=False)
faults_id.json()
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





    {'id': 26594,
     'acceleratorName': 'LHC',
     'systemName': 'Access System » Hardware',
     'startTime': '2016-05-12T14:44:42Z',
     'endTime': '2016-05-12T16:06:12Z',
     'duration': 4890000,
     'effectiveDuration': 4890000,
     'description': 'access sectors 4,5,6,7 and LHCb indicate "blue" door statuses and have lost patrol',
     'displayLabel': None,
     'stateChanges': [{'stateId': 'BLOCKING_OP', 'time': '2016-05-12T14:44:42Z'},
      {'stateId': 'OP_ENDED', 'time': '2016-05-12T16:06:12Z'}],
     'accessNeeded': True,
     'reviewedByAwg': True,
     'reviewedByExpert': True,
     'labelNames': ['TIOC'],
     'parentId': None,
     'parentSystemName': None,
     'systemPropertyInstances': [],
     'acceleratorPropertyInstances': [{'propertyName': 'Time in Fill',
       'value': '02h 13min 44s'},
      {'propertyName': 'Injection Scheme',
       'value': '2nominals_10pilots_RomanPot_Alignment'},
      {'propertyName': 'Accelerator Mode', 'value': 'Proton Physics'},
      {'propertyName': 'R2E Status', 'value': 'NOT_R2E_RELATED'},
      {'propertyName': 'Fill No', 'value': '4917'},
      {'propertyName': 'Time in Beam Mode (ms)', 'value': '2392997'},
      {'propertyName': 'Operational Mode', 'value': 'No Beams'},
      {'propertyName': 'Precycle Needed', 'value': 'No'},
      {'propertyName': 'Prevents Injection', 'value': 'Yes'},
      {'propertyName': 'Time in Beam Mode', 'value': '39min 52s'},
      {'propertyName': 'Beam Mode', 'value': 'NOBEAM'},
      {'propertyName': 'RP Needed', 'value': 'No'},
      {'propertyName': 'Time in Fill (ms)', 'value': '8024318'}],
     'faultyElementNames': ['patrols lost']}



6. `/api/public/v1/faults/labels`

Retrieve all labels attached to any fault.


```python
faults_labels = session.get('https://aft.cern.ch/api/public/v1/faults/labels', verify=False)
pd.DataFrame(faults_labels.json())
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>152937</td>
      <td>60A BPM Interaction</td>
    </tr>
    <tr>
      <th>1</th>
      <td>302613</td>
      <td>T4</td>
    </tr>
    <tr>
      <th>2</th>
      <td>83888</td>
      <td>TIOC</td>
    </tr>
    <tr>
      <th>3</th>
      <td>284995</td>
      <td>Access PS/PSB</td>
    </tr>
    <tr>
      <th>4</th>
      <td>372881</td>
      <td>LBE</td>
    </tr>
    <tr>
      <th>5</th>
      <td>262457</td>
      <td>PSU</td>
    </tr>
    <tr>
      <th>6</th>
      <td>370386</td>
      <td>klystron instability + modulator fault</td>
    </tr>
    <tr>
      <th>7</th>
      <td>255578</td>
      <td>AWG Notable Fault</td>
    </tr>
    <tr>
      <th>8</th>
      <td>372879</td>
      <td>Power supply</td>
    </tr>
    <tr>
      <th>9</th>
      <td>189853</td>
      <td>BLM Sanity Checks</td>
    </tr>
    <tr>
      <th>10</th>
      <td>302604</td>
      <td>LEIR</td>
    </tr>
    <tr>
      <th>11</th>
      <td>189865</td>
      <td>False Dump</td>
    </tr>
    <tr>
      <th>12</th>
      <td>114933</td>
      <td>TE-EPC</td>
    </tr>
    <tr>
      <th>13</th>
      <td>372379</td>
      <td>Major Event</td>
    </tr>
    <tr>
      <th>14</th>
      <td>260311</td>
      <td>SR7.C</td>
    </tr>
    <tr>
      <th>15</th>
      <td>248740</td>
      <td>PIC PLC</td>
    </tr>
    <tr>
      <th>16</th>
      <td>256924</td>
      <td>Ions</td>
    </tr>
    <tr>
      <th>17</th>
      <td>259150</td>
      <td>BLETC</td>
    </tr>
    <tr>
      <th>18</th>
      <td>281492</td>
      <td>pole-phase winding</td>
    </tr>
    <tr>
      <th>19</th>
      <td>302618</td>
      <td>frev</td>
    </tr>
    <tr>
      <th>20</th>
      <td>152921</td>
      <td>MPE-EPC Interface</td>
    </tr>
    <tr>
      <th>21</th>
      <td>260308</td>
      <td>SEM</td>
    </tr>
    <tr>
      <th>22</th>
      <td>249289</td>
      <td>human error</td>
    </tr>
    <tr>
      <th>23</th>
      <td>358893</td>
      <td>BLECF</td>
    </tr>
    <tr>
      <th>24</th>
      <td>87033</td>
      <td>Septum down</td>
    </tr>
    <tr>
      <th>25</th>
      <td>259163</td>
      <td>SR5.L.CD11.B</td>
    </tr>
    <tr>
      <th>26</th>
      <td>302607</td>
      <td>CPS RF</td>
    </tr>
    <tr>
      <th>27</th>
      <td>302570</td>
      <td>Flowmeter problem</td>
    </tr>
    <tr>
      <th>28</th>
      <td>302592</td>
      <td>LINAC3</td>
    </tr>
    <tr>
      <th>29</th>
      <td>302599</td>
      <td>source</td>
    </tr>
    <tr>
      <th>30</th>
      <td>372381</td>
      <td>Major Machine Protection Event</td>
    </tr>
    <tr>
      <th>31</th>
      <td>300628</td>
      <td>REX</td>
    </tr>
    <tr>
      <th>32</th>
      <td>289715</td>
      <td>RFQ RF protection</td>
    </tr>
    <tr>
      <th>33</th>
      <td>372877</td>
      <td>Ion pump</td>
    </tr>
    <tr>
      <th>34</th>
      <td>191632</td>
      <td>MENA-20 (BE-CO)</td>
    </tr>
    <tr>
      <th>35</th>
      <td>376535</td>
      <td>TAILCLIPPER TIMING</td>
    </tr>
  </tbody>
</table>
</div>



7. `/api/public/v1/faults/states`

Retrieve all possible states a fault can be in.


```python
faults_states = session.get('https://aft.cern.ch/api/public/v1/faults/states', verify=False)
pd.DataFrame(faults_states.json())
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>NON_BLOCKING_OP</td>
      <td>Non-Blocking OP</td>
    </tr>
    <tr>
      <th>1</th>
      <td>CANCELLED</td>
      <td>Cancelled</td>
    </tr>
    <tr>
      <th>2</th>
      <td>UNDERSTOOD</td>
      <td>Understood</td>
    </tr>
    <tr>
      <th>3</th>
      <td>SUSPENDED</td>
      <td>Suspended</td>
    </tr>
    <tr>
      <th>4</th>
      <td>SYSTEM_EXPERT_ENDED</td>
      <td>System Expert Ended</td>
    </tr>
    <tr>
      <th>5</th>
      <td>OP_ENDED</td>
      <td>OP Ended</td>
    </tr>
    <tr>
      <th>6</th>
      <td>BLOCKING_OP</td>
      <td>Blocking OP</td>
    </tr>
  </tbody>
</table>
</div>



8. `/api/public/v1/faulty-element-types`

Retrieve all faulty element types in use in AFT.


```python
fault_element_types = session.get('https://aft.cern.ch/api/public/v1/faulty-element-types', verify=False)
pd.DataFrame(fault_element_types.json())
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>PXACC40</td>
    </tr>
    <tr>
      <th>1</th>
      <td>PXRKDIO</td>
    </tr>
    <tr>
      <th>2</th>
      <td>HCTCSG_</td>
    </tr>
    <tr>
      <th>3</th>
      <td>L4LAINTLKSRC v.3.1.1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>PXBTVBM004</td>
    </tr>
    <tr>
      <th>5</th>
      <td>HCRPZES</td>
    </tr>
    <tr>
      <th>6</th>
      <td>HCRPZEQ</td>
    </tr>
    <tr>
      <th>7</th>
      <td>HCTCDIV</td>
    </tr>
    <tr>
      <th>8</th>
      <td>HCRPZEO</td>
    </tr>
    <tr>
      <th>9</th>
      <td>PXLMK__003</td>
    </tr>
    <tr>
      <th>10</th>
      <td>RCBV33</td>
    </tr>
    <tr>
      <th>11</th>
      <td>PXRK___</td>
    </tr>
    <tr>
      <th>12</th>
      <td>RNFH</td>
    </tr>
    <tr>
      <th>13</th>
      <td>HCCFI__</td>
    </tr>
    <tr>
      <th>14</th>
      <td>HCRPZEG</td>
    </tr>
    <tr>
      <th>15</th>
      <td>HCRPZEH</td>
    </tr>
    <tr>
      <th>16</th>
      <td>RCBV31</td>
    </tr>
    <tr>
      <th>17</th>
      <td>HCRPZEF</td>
    </tr>
    <tr>
      <th>18</th>
      <td>HCBVCRA001</td>
    </tr>
    <tr>
      <th>19</th>
      <td>HCRPZEB</td>
    </tr>
    <tr>
      <th>20</th>
      <td>PXACH__</td>
    </tr>
    <tr>
      <th>21</th>
      <td>RAC</td>
    </tr>
    <tr>
      <th>22</th>
      <td>PXDHZ__8AF</td>
    </tr>
    <tr>
      <th>23</th>
      <td>PXKHZ__</td>
    </tr>
    <tr>
      <th>24</th>
      <td>HCETH__</td>
    </tr>
    <tr>
      <th>25</th>
      <td>MKController_Virtual v.0.1.0</td>
    </tr>
    <tr>
      <th>26</th>
      <td>LEBT v.2.0.4</td>
    </tr>
    <tr>
      <th>27</th>
      <td>RCBV27</td>
    </tr>
    <tr>
      <th>28</th>
      <td>PXMCVEBHWC</td>
    </tr>
    <tr>
      <th>29</th>
      <td>RAR</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th>407</th>
      <td>HCTCL__</td>
    </tr>
    <tr>
      <th>408</th>
      <td>PXDVT__00T</td>
    </tr>
    <tr>
      <th>409</th>
      <td>HCLY___129</td>
    </tr>
    <tr>
      <th>410</th>
      <td>HCLY___005</td>
    </tr>
    <tr>
      <th>411</th>
      <td>LTIM v.4.1.13</td>
    </tr>
    <tr>
      <th>412</th>
      <td>HCRS___033</td>
    </tr>
    <tr>
      <th>413</th>
      <td>PXSTP__</td>
    </tr>
    <tr>
      <th>414</th>
      <td>RQT12</td>
    </tr>
    <tr>
      <th>415</th>
      <td>HCEAV__</td>
    </tr>
    <tr>
      <th>416</th>
      <td>PXCK___</td>
    </tr>
    <tr>
      <th>417</th>
      <td>RQT13</td>
    </tr>
    <tr>
      <th>418</th>
      <td>RBIV</td>
    </tr>
    <tr>
      <th>419</th>
      <td>RSMV</td>
    </tr>
    <tr>
      <th>420</th>
      <td>RDHZPS</td>
    </tr>
    <tr>
      <th>421</th>
      <td>PXMQNFA4WP</td>
    </tr>
    <tr>
      <th>422</th>
      <td>PVPUMP v.0</td>
    </tr>
    <tr>
      <th>423</th>
      <td>PXMCXBBWAP</td>
    </tr>
    <tr>
      <th>424</th>
      <td>LHC HALF-CELL</td>
    </tr>
    <tr>
      <th>425</th>
      <td>HCCFP__</td>
    </tr>
    <tr>
      <th>426</th>
      <td>GENERAL UNDG CIVIL WORK</td>
    </tr>
    <tr>
      <th>427</th>
      <td>PreChopperL4 v.4.5.7</td>
    </tr>
    <tr>
      <th>428</th>
      <td>PXMONDAFWP</td>
    </tr>
    <tr>
      <th>429</th>
      <td>RQIF</td>
    </tr>
    <tr>
      <th>430</th>
      <td>RSMH</td>
    </tr>
    <tr>
      <th>431</th>
      <td>PXMBHGC4WP</td>
    </tr>
    <tr>
      <th>432</th>
      <td>RQID</td>
    </tr>
    <tr>
      <th>433</th>
      <td>HCRPHGC</td>
    </tr>
    <tr>
      <th>434</th>
      <td>HCDQQDC</td>
    </tr>
    <tr>
      <th>435</th>
      <td>HCRPHGA</td>
    </tr>
    <tr>
      <th>436</th>
      <td>HCRPHGB</td>
    </tr>
  </tbody>
</table>
<p>437 rows × 1 columns</p>
</div>



9. `/api/public/v1/faulty-elements`

Retrieve all faulty elements in use in AFT. The results are paginated.


```python
params = {'label': 'QPS', 'page': 0, 'size': 10}
faulty_elements = session.get("https://aft.cern.ch/api/public/v1/faulty-elements?", params=params, verify=False)
pd.DataFrame(faulty_elements.json())
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>name</th>
      <th>source</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>164878</td>
      <td>Circuit breaker in a QPS element</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>1</th>
      <td>165063</td>
      <td>Comm lost with QPS controller RS[DF]2.A81B[12]</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>2</th>
      <td>161929</td>
      <td>600A QPS reset not working</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>3</th>
      <td>163583</td>
      <td>Cannot get QPS_OK</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>4</th>
      <td>163188</td>
      <td>IPQs QPS detection</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>5</th>
      <td>161874</td>
      <td>B27L5 nQPS</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>6</th>
      <td>161018</td>
      <td>B32R7 nQPS controller</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>7</th>
      <td>162457</td>
      <td>Circuit braker for QPS rack</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>8</th>
      <td>165620</td>
      <td>EE QPS RQD.A12</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>9</th>
      <td>162518</td>
      <td>Access in S78 for QPS disjuntor reset</td>
      <td>ELOGBOOK</td>
    </tr>
  </tbody>
</table>
</div>



10. `/api/public/v1/faulty-elements/{faultyElementId}/statistics`

Retrieve faulty element statistics from AFT.


```python
faulty_elements_statistics = session.get("https://aft.cern.ch/api/public/v1/faulty-elements/%d/statistics" % 163188, verify=False)
pd.DataFrame(faulty_elements_statistics.json(), index=[163188])
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>faultCount</th>
      <th>faultDuration</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>163188</th>
      <td>163188</td>
      <td>1</td>
      <td>2065000</td>
    </tr>
  </tbody>
</table>
</div>



11. `/api/public/v1/systems/{systemId}/properties`

Retrieve the properties which are specific to the given system.


```python
faulty_elements_statistics = session.get("https://aft.cern.ch/api/public/v1/systems/%d/properties" % 83823, verify=False)
pd.DataFrame(faulty_elements_statistics.json())
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>name</th>
      <th>pattern</th>
      <th>values</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>166765</td>
      <td>TI Fault Type</td>
      <td>None</td>
      <td>[Cable, Fuse, PLC Hardware, Relay, Wrong Actio...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>358702</td>
      <td>Cause</td>
      <td>None</td>
      <td>[Wind, Pollution, Snow, Unknown, Wildlife, Sho...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>302969</td>
      <td>Voltage Dip % (RTE)</td>
      <td>[-+]?[0-9]*\.?[0-9]*</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>3</th>
      <td>302967</td>
      <td>Duration ms (RTE)</td>
      <td>[-+]?[0-9]*\.?[0-9]*</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>4</th>
      <td>166766</td>
      <td>TI Major Event Id</td>
      <td>[1-9][0-9]*</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>5</th>
      <td>358703</td>
      <td>Origin Location</td>
      <td>None</td>
      <td>[Grande Ile - Le Cheylas, Albertville - Coche ...</td>
    </tr>
    <tr>
      <th>6</th>
      <td>302968</td>
      <td>Duration ms (CERN)</td>
      <td>[-+]?[0-9]*\.?[0-9]*</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>7</th>
      <td>302970</td>
      <td>Voltage Dip % (CERN)</td>
      <td>[-+]?[0-9]*\.?[0-9]*</td>
      <td>[]</td>
    </tr>
  </tbody>
</table>
</div>



## 3.1. DbRequest
### 3.1.1. PmDbRequest
**PmDbRequest** class encapsulates PM database queries in order to simplify the query creation as well as browsing of PM events for a given period.

- Expected Success Response


```python
from lhcsmapi.pyedsl.dbsignal.post_mortem.PmDbRequest import PmDbRequest
source = 'RPTE.UA47.RB.A45'
system = 'FGC'
className = '51_self_pmd'
t_start = '2015-03-13 05:20:59.4910002'
duration = [(100, 's'), (100, 's')]
PmDbRequest.find_events(source=source, system=system, className=className, t_start=t_start, duration=duration)
```




    [('RPTE.UA47.RB.A45', 1426220469520000000)]



Source can be also replaced with a wildcard '*' in order to retrieve all sources for a given system, className, and duration.


```python
from lhcsmapi.pyedsl.dbsignal.post_mortem.PmDbRequest import PmDbRequest
source = "*"
system = "QPS"
className = "DQAMCNMB_PMHSU"
ts_start = 1426201200000000000
event_duration_in_sec = 24*60*60 # 1 day is the maximum duration for a single query

PmDbRequest.find_events(source, system, className, t_start=ts_start, duration=[(event_duration_in_sec, 's')])
```




    [('B20L5', 1426220469491000000),
     ('C20L5', 1426220517100000000),
     ('A20L5', 1426220518112000000),
     ('A21L5', 1426220625990000000),
     ('B21L5', 1426220866112000000),
     ('C23L4', 1426236802332000000),
     ('B23L4', 1426236839404000000),
     ('A23L4', 1426236839832000000),
     ('C22L4', 1426236949841000000),
     ('C15R4', 1426251285711000000),
     ('B15R4', 1426251337747000000),
     ('A15R4', 1426251388741000000),
     ('B34L8', 1426258716281000000),
     ('C34L8', 1426258747672000000),
     ('A34L8', 1426258747370000000),
     ('C33L8', 1426258835955000000),
     ('C34R7', 1426258853947000000),
     ('A34R7', 1426258854113000000),
     ('A20R3', 1426267931956000000),
     ('B20R3', 1426267983579000000),
     ('C20R3', 1426268004144000000),
     ('B18L5', 1426277626360000000),
     ('A18L5', 1426277679838000000),
     ('C18L5', 1426277680496000000),
     ('A19L5', 1426277903449000000)]



- Expected Response with no data or wrong input


```python
from lhcsmapi.pyedsl.dbsignal.post_mortem.PmDbRequest import PmDbRequest
source = "*"
system = "QPS"
className = "DQAMCNMB_PMHSU"
ts_start = 1426201200000000000
event_duration_in_sec = 1

PmDbRequest.find_events(source, system, className, t_start=ts_start, duration=[(event_duration_in_sec, 's')])
```




    []



### 3.1.2. AftDbRequest


```python
from lhcsmapi.pyedsl.dbsignal.aft.AftDbRequest import AftDbRequest
```

1. `/api/public/v1/accelerators`

Retrieve all available accelerators.


```python
AftDbRequest(session).context_query('accelerators')
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>ISO_HRS</td>
      <td>ISOLDE HRS</td>
    </tr>
    <tr>
      <th>1</th>
      <td>LINAC4</td>
      <td>LINAC4</td>
    </tr>
    <tr>
      <th>2</th>
      <td>CLEAR</td>
      <td>CLEAR</td>
    </tr>
    <tr>
      <th>3</th>
      <td>LEIR</td>
      <td>LEIR</td>
    </tr>
    <tr>
      <th>4</th>
      <td>ISO_GPS</td>
      <td>ISOLDE GPS</td>
    </tr>
    <tr>
      <th>5</th>
      <td>SPS</td>
      <td>SPS</td>
    </tr>
    <tr>
      <th>6</th>
      <td>LINAC2</td>
      <td>LINAC2</td>
    </tr>
    <tr>
      <th>7</th>
      <td>AD</td>
      <td>AD</td>
    </tr>
    <tr>
      <th>8</th>
      <td>ELENA</td>
      <td>ELENA</td>
    </tr>
    <tr>
      <th>9</th>
      <td>LINAC3</td>
      <td>LINAC3</td>
    </tr>
    <tr>
      <th>10</th>
      <td>PS</td>
      <td>PS</td>
    </tr>
    <tr>
      <th>11</th>
      <td>PSB</td>
      <td>PSB</td>
    </tr>
    <tr>
      <th>12</th>
      <td>LHC</td>
      <td>LHC</td>
    </tr>
    <tr>
      <th>13</th>
      <td>ISO_REX_HIE</td>
      <td>ISOLDE REX-HIE</td>
    </tr>
  </tbody>
</table>
</div>



2. `/api/public/v1/accelerators/{acceleratorId}/properties`

Retrieve the properties which are specific to the given accelerator.


```python
AftDbRequest(session).context_query('accelerators/{acceleratorId}/properties', context_id='LHC')
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>group</th>
      <th>id</th>
      <th>name</th>
      <th>pattern</th>
      <th>values</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>LHC Fault Context</td>
      <td>303161</td>
      <td>Beam Mode</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>1</th>
      <td>LHC Fault Context</td>
      <td>303160</td>
      <td>Operational Mode</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Impact</td>
      <td>193273</td>
      <td>Prevents Injection</td>
      <td>None</td>
      <td>[No, Yes]</td>
    </tr>
    <tr>
      <th>3</th>
      <td>R2E Status</td>
      <td>266828</td>
      <td>R2E Status</td>
      <td>None</td>
      <td>[R2E_CANDIDATE, NOT_R2E_RELATED, R2E_REJECTED,...</td>
    </tr>
    <tr>
      <th>4</th>
      <td>LHC Fault Context</td>
      <td>303158</td>
      <td>Accelerator Mode</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>5</th>
      <td>LHC Fault Context</td>
      <td>303163</td>
      <td>Time in Fill (ms)</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>6</th>
      <td>LHC Fault Context</td>
      <td>303166</td>
      <td>Injection Scheme</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>7</th>
      <td>LHC Fault Context</td>
      <td>303164</td>
      <td>Time in Beam Mode</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>8</th>
      <td>Impact</td>
      <td>193272</td>
      <td>Precycle Needed</td>
      <td>None</td>
      <td>[No, Yes]</td>
    </tr>
    <tr>
      <th>9</th>
      <td>LHC Fault Context</td>
      <td>303159</td>
      <td>Fill No</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>10</th>
      <td>Impact</td>
      <td>193271</td>
      <td>RP Needed</td>
      <td>None</td>
      <td>[No, Yes]</td>
    </tr>
    <tr>
      <th>11</th>
      <td>LHC Fault Context</td>
      <td>303165</td>
      <td>Time in Beam Mode (ms)</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>12</th>
      <td>LHC Fault Context</td>
      <td>303162</td>
      <td>Time in Fill</td>
      <td>None</td>
      <td>[]</td>
    </tr>
  </tbody>
</table>
</div>



3. `/api/public/v1/accelerators/{acceleratorId}/systems`

Retrieve the systems of the given accelerator.


```python
AftDbRequest(session).context_query('accelerators/{acceleratorId}/systems', context_id='LHC')
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>children</th>
      <th>id</th>
      <th>name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>[{'id': 31395, 'name': 'Access Infrastructure ...</td>
      <td>83803</td>
      <td>Access Infrastructure</td>
    </tr>
    <tr>
      <th>1</th>
      <td>[{'id': 371383, 'name': 'Machine Interlock Sys...</td>
      <td>14</td>
      <td>Machine Interlock Systems</td>
    </tr>
    <tr>
      <th>2</th>
      <td>[]</td>
      <td>16739</td>
      <td>Beam-induced Quench</td>
    </tr>
    <tr>
      <th>3</th>
      <td>[{'id': 68, 'name': 'Cryogenics » Equipment', ...</td>
      <td>16</td>
      <td>Cryogenics</td>
    </tr>
    <tr>
      <th>4</th>
      <td>[{'id': 30655, 'name': 'LBDS » TCDQ', 'childre...</td>
      <td>10</td>
      <td>LBDS</td>
    </tr>
    <tr>
      <th>5</th>
      <td>[{'id': 55, 'name': 'Collimation » Interlocks ...</td>
      <td>13</td>
      <td>Collimation</td>
    </tr>
    <tr>
      <th>6</th>
      <td>[{'id': 89, 'name': 'Transverse Damper » Contr...</td>
      <td>21</td>
      <td>Transverse Damper</td>
    </tr>
    <tr>
      <th>7</th>
      <td>[{'id': 31171, 'name': 'Cooling and Ventilatio...</td>
      <td>83789</td>
      <td>Cooling and Ventilation</td>
    </tr>
    <tr>
      <th>8</th>
      <td>[{'id': 86, 'name': 'Radio Frequency » Hardwar...</td>
      <td>20</td>
      <td>Radio Frequency</td>
    </tr>
    <tr>
      <th>9</th>
      <td>[{'id': 106, 'name': 'SIS » Controls', 'childr...</td>
      <td>26</td>
      <td>SIS</td>
    </tr>
    <tr>
      <th>10</th>
      <td>[{'id': 107, 'name': 'QPS » Controller', 'chil...</td>
      <td>27</td>
      <td>QPS</td>
    </tr>
    <tr>
      <th>11</th>
      <td>[{'id': 79, 'name': 'Beam Injection » Oscillat...</td>
      <td>18</td>
      <td>Beam Injection</td>
    </tr>
    <tr>
      <th>12</th>
      <td>[{'id': 29456, 'name': 'Beam Exciters » Apertu...</td>
      <td>29453</td>
      <td>Beam Exciters</td>
    </tr>
    <tr>
      <th>13</th>
      <td>[{'id': 21232, 'name': 'Injection Systems » MK...</td>
      <td>21231</td>
      <td>Injection Systems</td>
    </tr>
    <tr>
      <th>14</th>
      <td>[{'id': 66, 'name': 'Accelerator Controls » CM...</td>
      <td>15</td>
      <td>Accelerator Controls</td>
    </tr>
    <tr>
      <th>15</th>
      <td>[{'id': 45, 'name': 'Beam Losses » Other', 'ch...</td>
      <td>11</td>
      <td>Beam Losses</td>
    </tr>
    <tr>
      <th>16</th>
      <td>[{'id': 47, 'name': 'Beam Instrumentation » BP...</td>
      <td>12</td>
      <td>Beam Instrumentation</td>
    </tr>
    <tr>
      <th>17</th>
      <td>[{'id': 31267, 'name': 'Electrical Network » B...</td>
      <td>83823</td>
      <td>Electrical Network</td>
    </tr>
    <tr>
      <th>18</th>
      <td>[{'id': 84, 'name': 'Operation » Operational e...</td>
      <td>19</td>
      <td>Operation</td>
    </tr>
    <tr>
      <th>19</th>
      <td>[{'id': 73, 'name': 'Experiments » ATLAS', 'ch...</td>
      <td>17</td>
      <td>Experiments</td>
    </tr>
    <tr>
      <th>20</th>
      <td>[]</td>
      <td>83801</td>
      <td>Ventilation Doors</td>
    </tr>
    <tr>
      <th>21</th>
      <td>[{'id': 101, 'name': 'Injector Complex » Beam ...</td>
      <td>24</td>
      <td>Injector Complex</td>
    </tr>
    <tr>
      <th>22</th>
      <td>[{'id': 21228, 'name': 'Magnet circuits » Eart...</td>
      <td>16738</td>
      <td>Magnet circuits</td>
    </tr>
    <tr>
      <th>23</th>
      <td>[{'id': 32132, 'name': 'IT Services » Network'...</td>
      <td>32124</td>
      <td>IT Services</td>
    </tr>
    <tr>
      <th>24</th>
      <td>[]</td>
      <td>29</td>
      <td>Other</td>
    </tr>
    <tr>
      <th>25</th>
      <td>[{'id': 31, 'name': 'Access System » Controls'...</td>
      <td>9</td>
      <td>Access System</td>
    </tr>
    <tr>
      <th>26</th>
      <td>[{'id': 95, 'name': 'Orbit » Reference', 'chil...</td>
      <td>22</td>
      <td>Orbit</td>
    </tr>
    <tr>
      <th>27</th>
      <td>[]</td>
      <td>28</td>
      <td>Power Converters</td>
    </tr>
    <tr>
      <th>28</th>
      <td>[{'id': 29452, 'name': 'Access Management » Ac...</td>
      <td>29449</td>
      <td>Access Management</td>
    </tr>
    <tr>
      <th>29</th>
      <td>[{'id': 103, 'name': 'Vacuum » Hardware', 'chi...</td>
      <td>25</td>
      <td>Vacuum</td>
    </tr>
  </tbody>
</table>
</div>



4. `/api/public/v1/faults`

Search for faults fulfilling the given criteria.


```python
AftDbRequest(session).fault_query(t_start='2016-01-13T00:00:00Z', t_end='2016-05-13T00:00:00Z', acceleratorId='LHC', accessNeeded=True)
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>acceleratorName</th>
      <th>acceleratorPropertyInstances</th>
      <th>accessNeeded</th>
      <th>description</th>
      <th>displayLabel</th>
      <th>duration</th>
      <th>effectiveDuration</th>
      <th>endTime</th>
      <th>faultyElementNames</th>
      <th>id</th>
      <th>labelNames</th>
      <th>parentId</th>
      <th>parentSystemName</th>
      <th>reviewedByAwg</th>
      <th>reviewedByExpert</th>
      <th>startTime</th>
      <th>stateChanges</th>
      <th>systemName</th>
      <th>systemPropertyInstances</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Fill', 'value': '02...</td>
      <td>False</td>
      <td>access sectors 4,5,6,7 and LHCb indicate "blue...</td>
      <td>None</td>
      <td>4890000</td>
      <td>4890000</td>
      <td>2016-05-12T16:06:12Z</td>
      <td>[patrols lost]</td>
      <td>26594</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-12T14:44:42Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-1...</td>
      <td>Access System » Hardware</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>1</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Injection Scheme', 'value':...</td>
      <td>False</td>
      <td>electrical problem on switchboard</td>
      <td>None</td>
      <td>10123000</td>
      <td>10123000</td>
      <td>2016-05-12T15:10:42Z</td>
      <td>[RQ9.L2B1]</td>
      <td>26599</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-12T12:21:59Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-1...</td>
      <td>Electrical Network » Distribution » 400 kV</td>
      <td>[{'propertyName': 'TI Fault Type', 'value': 'G...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Beam Mode (ms)', 'v...</td>
      <td>False</td>
      <td>Ventilation doors in IP7, they do not close, n...</td>
      <td>None</td>
      <td>3011000</td>
      <td>3011000</td>
      <td>2016-05-06T13:23:42Z</td>
      <td>[Ventilation doors in IP7]</td>
      <td>26462</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-06T12:33:31Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-0...</td>
      <td>Ventilation Doors</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>3</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Fill No', 'value': '4885'},...</td>
      <td>False</td>
      <td>RQT13.R7B1 water problem</td>
      <td>None</td>
      <td>18288000</td>
      <td>18288000</td>
      <td>2016-05-06T12:33:31Z</td>
      <td>[RQT13.R7B1, RPMBA.RR77.RQT13.R7B1]</td>
      <td>26459</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-06T07:28:43Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-0...</td>
      <td>Power Converters</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>4</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Accelerator Mode', 'value':...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>12347000</td>
      <td>12347000</td>
      <td>2016-05-06T07:28:43Z</td>
      <td>[ROF.A45B1]</td>
      <td>26456</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-06T04:02:56Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-0...</td>
      <td>QPS » Controller</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>5</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Beam Mode', 'value': 'INJPR...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>9298000</td>
      <td>9298000</td>
      <td>2016-05-05T20:15:30Z</td>
      <td>[RPMBB.UA63.RSF1.A56B1, RSF1.A56B1]</td>
      <td>26450</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-05T17:40:32Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-0...</td>
      <td>Power Converters</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>6</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Beam Mode', 'value': 'INJPR...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>3817000</td>
      <td>3817000</td>
      <td>2016-05-05T15:39:44Z</td>
      <td>[RPMBB.UA63.RSF1.A56B1, RSF1.A56B1]</td>
      <td>26444</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-05T14:36:07Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-0...</td>
      <td>Power Converters</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>7</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Fill', 'value': '05...</td>
      <td>False</td>
      <td>cannot close door R74</td>
      <td>None</td>
      <td>6114000</td>
      <td>6114000</td>
      <td>2016-05-05T09:52:50Z</td>
      <td>[IP7 enclosure door]</td>
      <td>26441</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-05T08:10:56Z</td>
      <td>[{'stateId': 'NON_BLOCKING_OP', 'time': '2016-...</td>
      <td>Ventilation Doors</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>8</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Beam Mode', 'value'...</td>
      <td>False</td>
      <td>opened while in beam ON</td>
      <td>None</td>
      <td>16471000</td>
      <td>16471000</td>
      <td>2016-04-26T13:39:48Z</td>
      <td>[UL44]</td>
      <td>26260</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-04-26T09:05:17Z</td>
      <td>[{'stateId': 'NON_BLOCKING_OP', 'time': '2016-...</td>
      <td>Ventilation Doors</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>9</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Beam Mode', 'value'...</td>
      <td>False</td>
      <td>The door opened because the closing mechanism ...</td>
      <td>None</td>
      <td>5433000</td>
      <td>5433000</td>
      <td>2016-04-12T07:57:09Z</td>
      <td>[door YCPZ01=PM25]</td>
      <td>25760</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-04-12T06:26:36Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-04-1...</td>
      <td>Access Management » Patrol Lost</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>10</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Beam Mode', 'value'...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>3091000</td>
      <td>3091000</td>
      <td>2016-04-08T09:07:23Z</td>
      <td>[TCL.5R1B1]</td>
      <td>25662</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-04-08T08:15:52Z</td>
      <td>[{'stateId': 'NON_BLOCKING_OP', 'time': '2016-...</td>
      <td>Collimation » Controls</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>11</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Operational Mode', 'value':...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>80000</td>
      <td>80000</td>
      <td>2016-04-07T16:04:39Z</td>
      <td>[RU.L4]</td>
      <td>25940</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-04-07T16:03:19Z</td>
      <td>[{'stateId': 'NON_BLOCKING_OP', 'time': '2016-...</td>
      <td>QPS » Controller</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>12</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Accelerator Mode', 'value':...</td>
      <td>False</td>
      <td>RCBYV4.R1B1: Vin DC over voltage\nRQ7.R1 (AC f...</td>
      <td>None</td>
      <td>14998000</td>
      <td>14998000</td>
      <td>2016-04-01T05:16:05Z</td>
      <td>[RPLB.RR17.RCBYV4.R1B1, RPHGA.RR17.RQ7.R1B2, R...</td>
      <td>25500</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-04-01T01:06:07Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-04-0...</td>
      <td>Power Converters</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>13</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Accelerator Mode', 'value':...</td>
      <td>False</td>
      <td>RB.A34 warm cable cooling (interlock on water ...</td>
      <td>None</td>
      <td>7654000</td>
      <td>7654000</td>
      <td>2016-03-30T22:44:40Z</td>
      <td>[RB.A34]</td>
      <td>25460</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-03-30T20:37:06Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-03-3...</td>
      <td>Cooling and Ventilation » Cooling » Deminerali...</td>
      <td>[{'propertyName': 'TI Major Event Id', 'value'...</td>
    </tr>
    <tr>
      <th>14</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Injection Scheme', 'value':...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>17483000</td>
      <td>17483000</td>
      <td>2016-03-28T08:00:42Z</td>
      <td>[RPHFC.UA83.RQX.L8, RQX.L8]</td>
      <td>25478</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-03-28T03:09:19Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-03-2...</td>
      <td>Power Converters</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>15</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Injection Scheme', 'value':...</td>
      <td>False</td>
      <td>broken cable on thermo switch</td>
      <td>None</td>
      <td>10820000</td>
      <td>10820000</td>
      <td>2016-03-25T19:00:15Z</td>
      <td>[RD34.LR3]</td>
      <td>25380</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-03-25T15:59:55Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-03-2...</td>
      <td>Magnet circuits » Other</td>
      <td>[]</td>
    </tr>
  </tbody>
</table>
</div>



5. `/api/public/v1/faults/{faultId}`

Retrieve one particular fault.


```python
AftDbRequest(session).context_query('faults/{faultId}', context_id=26594)
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





    {'id': 26594,
     'acceleratorName': 'LHC',
     'systemName': 'Access System » Hardware',
     'startTime': '2016-05-12T14:44:42Z',
     'endTime': '2016-05-12T16:06:12Z',
     'duration': 4890000,
     'effectiveDuration': 4890000,
     'description': 'access sectors 4,5,6,7 and LHCb indicate "blue" door statuses and have lost patrol',
     'displayLabel': None,
     'stateChanges': [{'stateId': 'BLOCKING_OP', 'time': '2016-05-12T14:44:42Z'},
      {'stateId': 'OP_ENDED', 'time': '2016-05-12T16:06:12Z'}],
     'accessNeeded': True,
     'reviewedByAwg': True,
     'reviewedByExpert': True,
     'labelNames': ['TIOC'],
     'parentId': None,
     'parentSystemName': None,
     'systemPropertyInstances': [],
     'acceleratorPropertyInstances': [{'propertyName': 'Time in Fill',
       'value': '02h 13min 44s'},
      {'propertyName': 'Injection Scheme',
       'value': '2nominals_10pilots_RomanPot_Alignment'},
      {'propertyName': 'Accelerator Mode', 'value': 'Proton Physics'},
      {'propertyName': 'R2E Status', 'value': 'NOT_R2E_RELATED'},
      {'propertyName': 'Fill No', 'value': '4917'},
      {'propertyName': 'Time in Beam Mode (ms)', 'value': '2392997'},
      {'propertyName': 'Operational Mode', 'value': 'No Beams'},
      {'propertyName': 'Precycle Needed', 'value': 'No'},
      {'propertyName': 'Prevents Injection', 'value': 'Yes'},
      {'propertyName': 'Time in Beam Mode', 'value': '39min 52s'},
      {'propertyName': 'Beam Mode', 'value': 'NOBEAM'},
      {'propertyName': 'RP Needed', 'value': 'No'},
      {'propertyName': 'Time in Fill (ms)', 'value': '8024318'}],
     'faultyElementNames': ['patrols lost']}



6. `/api/public/v1/faults/labels`

Retrieve all labels attached to any fault.


```python
AftDbRequest(session).context_query('faults/labels')
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>152937</td>
      <td>60A BPM Interaction</td>
    </tr>
    <tr>
      <th>1</th>
      <td>302613</td>
      <td>T4</td>
    </tr>
    <tr>
      <th>2</th>
      <td>83888</td>
      <td>TIOC</td>
    </tr>
    <tr>
      <th>3</th>
      <td>284995</td>
      <td>Access PS/PSB</td>
    </tr>
    <tr>
      <th>4</th>
      <td>372881</td>
      <td>LBE</td>
    </tr>
    <tr>
      <th>5</th>
      <td>262457</td>
      <td>PSU</td>
    </tr>
    <tr>
      <th>6</th>
      <td>370386</td>
      <td>klystron instability + modulator fault</td>
    </tr>
    <tr>
      <th>7</th>
      <td>255578</td>
      <td>AWG Notable Fault</td>
    </tr>
    <tr>
      <th>8</th>
      <td>372879</td>
      <td>Power supply</td>
    </tr>
    <tr>
      <th>9</th>
      <td>189853</td>
      <td>BLM Sanity Checks</td>
    </tr>
    <tr>
      <th>10</th>
      <td>302604</td>
      <td>LEIR</td>
    </tr>
    <tr>
      <th>11</th>
      <td>189865</td>
      <td>False Dump</td>
    </tr>
    <tr>
      <th>12</th>
      <td>114933</td>
      <td>TE-EPC</td>
    </tr>
    <tr>
      <th>13</th>
      <td>372379</td>
      <td>Major Event</td>
    </tr>
    <tr>
      <th>14</th>
      <td>260311</td>
      <td>SR7.C</td>
    </tr>
    <tr>
      <th>15</th>
      <td>248740</td>
      <td>PIC PLC</td>
    </tr>
    <tr>
      <th>16</th>
      <td>256924</td>
      <td>Ions</td>
    </tr>
    <tr>
      <th>17</th>
      <td>259150</td>
      <td>BLETC</td>
    </tr>
    <tr>
      <th>18</th>
      <td>281492</td>
      <td>pole-phase winding</td>
    </tr>
    <tr>
      <th>19</th>
      <td>302618</td>
      <td>frev</td>
    </tr>
    <tr>
      <th>20</th>
      <td>152921</td>
      <td>MPE-EPC Interface</td>
    </tr>
    <tr>
      <th>21</th>
      <td>260308</td>
      <td>SEM</td>
    </tr>
    <tr>
      <th>22</th>
      <td>249289</td>
      <td>human error</td>
    </tr>
    <tr>
      <th>23</th>
      <td>358893</td>
      <td>BLECF</td>
    </tr>
    <tr>
      <th>24</th>
      <td>87033</td>
      <td>Septum down</td>
    </tr>
    <tr>
      <th>25</th>
      <td>259163</td>
      <td>SR5.L.CD11.B</td>
    </tr>
    <tr>
      <th>26</th>
      <td>302607</td>
      <td>CPS RF</td>
    </tr>
    <tr>
      <th>27</th>
      <td>302570</td>
      <td>Flowmeter problem</td>
    </tr>
    <tr>
      <th>28</th>
      <td>302592</td>
      <td>LINAC3</td>
    </tr>
    <tr>
      <th>29</th>
      <td>302599</td>
      <td>source</td>
    </tr>
    <tr>
      <th>30</th>
      <td>372381</td>
      <td>Major Machine Protection Event</td>
    </tr>
    <tr>
      <th>31</th>
      <td>300628</td>
      <td>REX</td>
    </tr>
    <tr>
      <th>32</th>
      <td>289715</td>
      <td>RFQ RF protection</td>
    </tr>
    <tr>
      <th>33</th>
      <td>372877</td>
      <td>Ion pump</td>
    </tr>
    <tr>
      <th>34</th>
      <td>191632</td>
      <td>MENA-20 (BE-CO)</td>
    </tr>
    <tr>
      <th>35</th>
      <td>376535</td>
      <td>TAILCLIPPER TIMING</td>
    </tr>
  </tbody>
</table>
</div>



7. `/api/public/v1/faults/states`

Retrieve all possible states a fault can be in.


```python
AftDbRequest(session).context_query('faults/states')
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>NON_BLOCKING_OP</td>
      <td>Non-Blocking OP</td>
    </tr>
    <tr>
      <th>1</th>
      <td>CANCELLED</td>
      <td>Cancelled</td>
    </tr>
    <tr>
      <th>2</th>
      <td>UNDERSTOOD</td>
      <td>Understood</td>
    </tr>
    <tr>
      <th>3</th>
      <td>SUSPENDED</td>
      <td>Suspended</td>
    </tr>
    <tr>
      <th>4</th>
      <td>SYSTEM_EXPERT_ENDED</td>
      <td>System Expert Ended</td>
    </tr>
    <tr>
      <th>5</th>
      <td>OP_ENDED</td>
      <td>OP Ended</td>
    </tr>
    <tr>
      <th>6</th>
      <td>BLOCKING_OP</td>
      <td>Blocking OP</td>
    </tr>
  </tbody>
</table>
</div>



8. `/api/public/v1/faulty-element-types`

Retrieve all faulty element types in use in AFT.


```python
AftDbRequest(session).context_query('faulty-element-types')
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>PXACC40</td>
    </tr>
    <tr>
      <th>1</th>
      <td>PXRKDIO</td>
    </tr>
    <tr>
      <th>2</th>
      <td>HCTCSG_</td>
    </tr>
    <tr>
      <th>3</th>
      <td>L4LAINTLKSRC v.3.1.1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>PXBTVBM004</td>
    </tr>
    <tr>
      <th>5</th>
      <td>HCRPZES</td>
    </tr>
    <tr>
      <th>6</th>
      <td>HCRPZEQ</td>
    </tr>
    <tr>
      <th>7</th>
      <td>HCTCDIV</td>
    </tr>
    <tr>
      <th>8</th>
      <td>HCRPZEO</td>
    </tr>
    <tr>
      <th>9</th>
      <td>PXLMK__003</td>
    </tr>
    <tr>
      <th>10</th>
      <td>RCBV33</td>
    </tr>
    <tr>
      <th>11</th>
      <td>PXRK___</td>
    </tr>
    <tr>
      <th>12</th>
      <td>RNFH</td>
    </tr>
    <tr>
      <th>13</th>
      <td>HCCFI__</td>
    </tr>
    <tr>
      <th>14</th>
      <td>HCRPZEG</td>
    </tr>
    <tr>
      <th>15</th>
      <td>HCRPZEH</td>
    </tr>
    <tr>
      <th>16</th>
      <td>RCBV31</td>
    </tr>
    <tr>
      <th>17</th>
      <td>HCRPZEF</td>
    </tr>
    <tr>
      <th>18</th>
      <td>HCBVCRA001</td>
    </tr>
    <tr>
      <th>19</th>
      <td>HCRPZEB</td>
    </tr>
    <tr>
      <th>20</th>
      <td>PXACH__</td>
    </tr>
    <tr>
      <th>21</th>
      <td>RAC</td>
    </tr>
    <tr>
      <th>22</th>
      <td>PXDHZ__8AF</td>
    </tr>
    <tr>
      <th>23</th>
      <td>PXKHZ__</td>
    </tr>
    <tr>
      <th>24</th>
      <td>HCETH__</td>
    </tr>
    <tr>
      <th>25</th>
      <td>MKController_Virtual v.0.1.0</td>
    </tr>
    <tr>
      <th>26</th>
      <td>LEBT v.2.0.4</td>
    </tr>
    <tr>
      <th>27</th>
      <td>RCBV27</td>
    </tr>
    <tr>
      <th>28</th>
      <td>PXMCVEBHWC</td>
    </tr>
    <tr>
      <th>29</th>
      <td>RAR</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th>407</th>
      <td>HCTCL__</td>
    </tr>
    <tr>
      <th>408</th>
      <td>PXDVT__00T</td>
    </tr>
    <tr>
      <th>409</th>
      <td>HCLY___129</td>
    </tr>
    <tr>
      <th>410</th>
      <td>HCLY___005</td>
    </tr>
    <tr>
      <th>411</th>
      <td>LTIM v.4.1.13</td>
    </tr>
    <tr>
      <th>412</th>
      <td>HCRS___033</td>
    </tr>
    <tr>
      <th>413</th>
      <td>PXSTP__</td>
    </tr>
    <tr>
      <th>414</th>
      <td>RQT12</td>
    </tr>
    <tr>
      <th>415</th>
      <td>HCEAV__</td>
    </tr>
    <tr>
      <th>416</th>
      <td>PXCK___</td>
    </tr>
    <tr>
      <th>417</th>
      <td>RQT13</td>
    </tr>
    <tr>
      <th>418</th>
      <td>RBIV</td>
    </tr>
    <tr>
      <th>419</th>
      <td>RSMV</td>
    </tr>
    <tr>
      <th>420</th>
      <td>RDHZPS</td>
    </tr>
    <tr>
      <th>421</th>
      <td>PXMQNFA4WP</td>
    </tr>
    <tr>
      <th>422</th>
      <td>PVPUMP v.0</td>
    </tr>
    <tr>
      <th>423</th>
      <td>PXMCXBBWAP</td>
    </tr>
    <tr>
      <th>424</th>
      <td>LHC HALF-CELL</td>
    </tr>
    <tr>
      <th>425</th>
      <td>HCCFP__</td>
    </tr>
    <tr>
      <th>426</th>
      <td>GENERAL UNDG CIVIL WORK</td>
    </tr>
    <tr>
      <th>427</th>
      <td>PreChopperL4 v.4.5.7</td>
    </tr>
    <tr>
      <th>428</th>
      <td>PXMONDAFWP</td>
    </tr>
    <tr>
      <th>429</th>
      <td>RQIF</td>
    </tr>
    <tr>
      <th>430</th>
      <td>RSMH</td>
    </tr>
    <tr>
      <th>431</th>
      <td>PXMBHGC4WP</td>
    </tr>
    <tr>
      <th>432</th>
      <td>RQID</td>
    </tr>
    <tr>
      <th>433</th>
      <td>HCRPHGC</td>
    </tr>
    <tr>
      <th>434</th>
      <td>HCDQQDC</td>
    </tr>
    <tr>
      <th>435</th>
      <td>HCRPHGA</td>
    </tr>
    <tr>
      <th>436</th>
      <td>HCRPHGB</td>
    </tr>
  </tbody>
</table>
<p>437 rows × 1 columns</p>
</div>



9. `/api/public/v1/faulty-elements`

Retrieve all faulty elements in use in AFT. The results are paginated.


```python
AftDbRequest(session).context_query('faulty-elements', label='QPS', page=0, size=10)
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>name</th>
      <th>source</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>164878</td>
      <td>Circuit breaker in a QPS element</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>1</th>
      <td>165063</td>
      <td>Comm lost with QPS controller RS[DF]2.A81B[12]</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>2</th>
      <td>161929</td>
      <td>600A QPS reset not working</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>3</th>
      <td>163583</td>
      <td>Cannot get QPS_OK</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>4</th>
      <td>163188</td>
      <td>IPQs QPS detection</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>5</th>
      <td>161874</td>
      <td>B27L5 nQPS</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>6</th>
      <td>161018</td>
      <td>B32R7 nQPS controller</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>7</th>
      <td>162457</td>
      <td>Circuit braker for QPS rack</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>8</th>
      <td>165620</td>
      <td>EE QPS RQD.A12</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>9</th>
      <td>162518</td>
      <td>Access in S78 for QPS disjuntor reset</td>
      <td>ELOGBOOK</td>
    </tr>
  </tbody>
</table>
</div>



10. `/api/public/v1/faulty-elements/{faultyElementId}/statistics`

Retrieve faulty element statistics from AFT.


```python
AftDbRequest(session).context_query('faulty-elements/{faultyElementId}/statistics', context_id=163188)
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>faultCount</th>
      <th>faultDuration</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>163188</th>
      <td>163188</td>
      <td>1</td>
      <td>2065000</td>
    </tr>
  </tbody>
</table>
</div>



11. `/api/public/v1/systems/{systemId}/properties`

Retrieve the properties which are specific to the given system.


```python
AftDbRequest(session).context_query('systems/{systemId}/properties', context_id=83823)
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>name</th>
      <th>pattern</th>
      <th>values</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>166765</td>
      <td>TI Fault Type</td>
      <td>None</td>
      <td>[Cable, Fuse, PLC Hardware, Wrong Action, Rela...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>358702</td>
      <td>Cause</td>
      <td>None</td>
      <td>[Wind, Pollution, Snow, Unknown, Wildlife, Sho...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>302969</td>
      <td>Voltage Dip % (RTE)</td>
      <td>[-+]?[0-9]*\.?[0-9]*</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>3</th>
      <td>302967</td>
      <td>Duration ms (RTE)</td>
      <td>[-+]?[0-9]*\.?[0-9]*</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>4</th>
      <td>166766</td>
      <td>TI Major Event Id</td>
      <td>[1-9][0-9]*</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>5</th>
      <td>358703</td>
      <td>Origin Location</td>
      <td>None</td>
      <td>[Grande Ile - Le Cheylas, Albertville - Coche ...</td>
    </tr>
    <tr>
      <th>6</th>
      <td>302968</td>
      <td>Duration ms (CERN)</td>
      <td>[-+]?[0-9]*\.?[0-9]*</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>7</th>
      <td>302970</td>
      <td>Voltage Dip % (CERN)</td>
      <td>[-+]?[0-9]*\.?[0-9]*</td>
      <td>[]</td>
    </tr>
  </tbody>
</table>
</div>



## 3.2. DbSignal

**DbSignal** class encapsulates low-level APIs for working with signal databases. It provides read and write methods for accessing Post Mortem, NXCALS and AFT APIs.

Read method supports only scalar arguments and returns a pandas DataFrame.  
In case of errors, the read method returns a warning message with the raw API use to query in order to quickly reproduce the error. This way, one can exclude the error introduced by the lhcsmapi.

### 3.2.1. PmDbSignal

- PM Success Response


```python
from lhcsmapi.pyedsl.dbsignal.SignalDbQueryFactory import SignalDbQueryFactory
system = 'FGC'
source = 'RPTE.UA47.RB.A45'
className = '51_self_pmd'
signal = 'STATUS.I_MEAS'
eventTime = 1426220469520000000
i_meas_df = SignalDbQueryFactory().build('pm', system=system, source=source, className=className, signal=signal, eventTime=eventTime).read()[0]
i_meas_df.plot()
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fdcba166668>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_140_1.png)
    



```python
from lhcsmapi.pyedsl.dbsignal.SignalDbQueryFactory import SignalDbQueryFactory
system = "QPS"
className = "DQAMCNMB_PMHSU"
magnet = "B20L5"
signal = "I_HDS_4"
timestamp = 1426220469491000000
i_hds_4_df = SignalDbQueryFactory().build('pm', system=system, className=className, source=magnet, signal=signal, eventTime=timestamp).read()[0]
i_hds_4_df.plot()
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fdcc83a7828>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_141_1.png)
    


- PM Fail Response

In case of a query failure, an empty is returned. 


```python
from lhcsmapi.pyedsl.dbsignal.SignalDbQueryFactory import SignalDbQueryFactory
system = "QPS"
className = "DQAMCNMB_PMHSU"
magnet = "B20L5"
signal = "I_HDS_4"
timestamp = 142622046949100000 # last zero removed
i_hds_4_df = SignalDbQueryFactory().build('pm', system=system, className=className, source=magnet, signal=signal, eventTime=timestamp).read()[0]
i_hds_4_df
```

    /eos/project/l/lhcsm/venv/lhcsmapi/pyedsl/dbsignal/post_mortem/PmDbRequest.py:165: UserWarning: Querying Post Mortem failed using the following query: http://pm-api-pro/v2/pmdata/signal?system=QPS&className=DQAMCNMB_PMHSU&source=B20L5&timestampInNanos=142622046949100000&signal=I_HDS_4
      warnings.warn(warning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
</div>



### 3.2.2. NxcalsDbSignal

- NXCALS Success Response


```python
from lhcsmapi.pyedsl.dbsignal.SignalDbQueryFactory import SignalDbQueryFactory

nxcals_system = 'CMW'
nxcals_device = 'RPTE.UA47.RB.A45'
nxcals_property = 'SUB'
signal = 'I_MEAS'
t_start = 1426220469491000000
duration = [(10, 's'), (100, 's')]

i_meas_df = SignalDbQueryFactory().build('nxcals', nxcals_system=nxcals_system,  nxcals_device=[nxcals_device], nxcals_property=nxcals_property, 
                                         signals=[signal], t_start=t_start, duration=duration, spark=spark).read()[0]
i_meas_df.plot()
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fdcb9fb8978>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_146_1.png)
    



```python
from lhcsmapi.pyedsl.dbsignal.SignalDbQueryFactory import SignalDbQueryFactory
nxcals_system = 'CMW'

signal = 'DCBA.15R4.R:U_MAG'
t_start = 1426220469491000000
duration = [(100, 's')]

u_mag_df = SignalDbQueryFactory().build('nxcals', nxcals_system=nxcals_system,
                                        signals=[signal], t_start=t_start, duration=duration, spark=spark).read()[0]
if not u_mag_df.empty:
    u_mag_df.plot()
```


    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_147_0.png)
    



```python
from lhcsmapi.pyedsl.dbsignal.SignalDbQueryFactory import SignalDbQueryFactory

bmode_df = SignalDbQueryFactory().build('nxcals', nxcals_system='CMW',
                                        signals=['HX:BMODE'], t_start='2015-03-13 05:20:59.4910002', t_end='2015-04-13 05:20:59.4910002', spark=spark).read()[0]
bmode_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>HX:BMODE</th>
    </tr>
    <tr>
      <th>nxcals_timestamp</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1426513864882000000</th>
      <td>SETUP</td>
    </tr>
    <tr>
      <th>1426515551191000000</th>
      <td>NOBEAM</td>
    </tr>
    <tr>
      <th>1426597426610000000</th>
      <td>SETUP</td>
    </tr>
    <tr>
      <th>1426597576404000000</th>
      <td>STABLE</td>
    </tr>
    <tr>
      <th>1426598591040000000</th>
      <td>UNSTABLE</td>
    </tr>
  </tbody>
</table>
</div>



- NXCALS Fail Response


```python
from lhcsmapi.pyedsl.dbsignal.SignalDbQueryFactory import SignalDbQueryFactory
nxcals_system = 'WINCCOA'

signal = 'DCBA.15R4.R:U_MAG'
t_start = 1426220469491000000
duration = [(100, 's')]

u_mag_df = SignalDbQueryFactory().build('nxcals', nxcals_system=nxcals_system,
                                        signals=[signal], t_start=t_start, duration=duration, spark=spark).read()[0]
if not u_mag_df.empty:
    u_mag_df.plot()
```

    /eos/project/l/lhcsm/venv/lhcsmapi/pyedsl/dbsignal/nxcals/NxcalsVariableSignalDbQuery.py:110: UserWarning: 'Illegal call to remote service for method=EntityResourceClient#findBy(QueryDataCriteria), status=400: Illegal Argument: Variables: [DCBA.15R4.R:U_MAG] were not found in system WINCCOA\nStacktrace: java.lang.IllegalArgumentException: Variables: [DCBA.15R4.R:U_MAG] were not found in system WINCCOA\n\tat cern.nxcals.service.internal.EntityResourceService.getVariablesFor(EntityResourceService.java:186)\n\tat cern.nxcals.service.internal.EntityResourceService.getVariableConfigs(EntityResourceService.java:131)\n\tat cern.nxcals.service.internal.EntityResourceService.getSourceConfigs(EntityResourceService.java:105)\n\tat cern.nxcals.service.internal.EntityResourceService.findBy(EntityResourceService.java:97)\n\tat cern.nxcals.service.internal.EntityResourceService$$FastClassBySpringCGLIB$$29e04dfb.invoke(<generated>)\n\tat org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\n\tat org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:749)\n\tat org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\n\tat org.springframework.transaction.interceptor.TransactionAspectSupport.invokeWithinTransaction(TransactionAspectSupport.java:294)\n\tat org.springframework.transaction.interceptor.TransactionInterceptor.invoke(TransactionInterceptor.java:98)\n\tat org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186)\n\tat org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:688)\n\tat cern.nxcals.service.internal.EntityResourceService$$EnhancerBySpringCGLIB$$d851f3ca.findBy(<generated>)\n\tat cern.nxcals.service.rest.EntityResourceController.findBy(EntityResourceController.java:30)\n\tat sun.reflect.GeneratedMethodAccessor204.invoke(Unknown Source)\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.reflect.Method.invoke(Method.java:498)\n\tat org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:189)\n\tat org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:138)\n\tat org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:102)\n\tat org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:895)\n\tat org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:800)\n\tat org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\n\tat org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1038)\n\tat org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:942)\n\tat org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1005)\n\tat org.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:908)\n\tat javax.servlet.http.HttpServlet.service(HttpServlet.java:660)\n\tat org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:882)\n\tat javax.servlet.http.HttpServlet.service(HttpServlet.java:741)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:231)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:101)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.security.kerberos.web.authentication.SpnegoAuthenticationProcessingFilter.doFilter(SpnegoAuthenticationProcessingFilter.java:128)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.boot.actuate.web.trace.servlet.HttpTraceFilter.doFilterInternal(HttpTraceFilter.java:90)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:320)\n\tat org.springframework.security.web.access.intercept.FilterSecurityInterceptor.invoke(FilterSecurityInterceptor.java:127)\n\tat org.springframework.security.web.access.intercept.FilterSecurityInterceptor.doFilter(FilterSecurityInterceptor.java:91)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.access.ExceptionTranslationFilter.doFilter(ExceptionTranslationFilter.java:119)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.session.SessionManagementFilter.doFilter(SessionManagementFilter.java:137)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.authentication.AnonymousAuthenticationFilter.doFilter(AnonymousAuthenticationFilter.java:111)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter.doFilter(SecurityContextHolderAwareRequestFilter.java:170)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.savedrequest.RequestCacheAwareFilter.doFilter(RequestCacheAwareFilter.java:63)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.kerberos.web.authentication.SpnegoAuthenticationProcessingFilter.doFilter(SpnegoAuthenticationProcessingFilter.java:167)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat cern.nxcals.service.security.rbac.RbacAuthenticationFilter.doFilterInternal(RbacAuthenticationFilter.java:39)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.authentication.logout.LogoutFilter.doFilter(LogoutFilter.java:116)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.header.HeaderWriterFilter.doFilterInternal(HeaderWriterFilter.java:74)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.context.SecurityContextPersistenceFilter.doFilter(SecurityContextPersistenceFilter.java:105)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFilter.doFilterInternal(WebAsyncManagerIntegrationFilter.java:56)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.FilterChainProxy.doFilterInternal(FilterChainProxy.java:215)\n\tat org.springframework.security.web.FilterChainProxy.doFilter(FilterChainProxy.java:178)\n\tat org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:357)\n\tat org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:270)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:99)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.web.filter.FormContentFilter.doFilterInternal(FormContentFilter.java:92)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.web.filter.HiddenHttpMethodFilter.doFilterInternal(HiddenHttpMethodFilter.java:93)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.boot.actuate.metrics.web.servlet.WebMvcMetricsFilter.filterAndRecordMetrics(WebMvcMetricsFilter.java:117)\n\tat org.springframework.boot.actuate.metrics.web.servlet.WebMvcMetricsFilter.doFilterInternal(WebMvcMetricsFilter.java:106)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:200)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:200)\n\tat org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:96)\n\tat org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:490)\n\tat org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:139)\n\tat org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\n\tat org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:74)\n\tat org.apache.catalina.valves.AbstractAccessLogValve.invoke(AbstractAccessLogValve.java:668)\n\tat org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:343)\n\tat org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:408)\n\tat org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:66)\n\tat org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:834)\n\tat org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1415)\n\tat org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\n\tat java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)\n\tat java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)\n\tat org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\n\tat java.lang.Thread.run(Thread.java:748)\n'
      warnings.warn(str(e))
    /eos/project/l/lhcsm/venv/lhcsmapi/pyedsl/dbsignal/nxcals/NxcalsVariableSignalDbQuery.py:111: UserWarning: NXCALS query failed with the parameters, nxcals_system: WINCCOA, t_start: 2015-03-13T04:21:09.491000000, t_end: 2015-03-13T04:22:49.491000000, 
                            signals: ['DCBA.15R4.R:U_MAG']
    
                            List of empty signals returned.
                            ___________________________________________________________________________________
                            You can reproduce the error using the raw NXCALS API:
    
                            from cern.nxcals.pyquery.builders import *
                
                            variable_builder = VariableQuery.builder(spark) \
                                .system(WINCCOA) \
                                .startTime(2015-03-13T04:21:09.491000000).endTime(2015-03-13T04:22:49.491000000)
    
                            for signal in ['DCBA.15R4.R:U_MAG']:
                                variable_builder = variable_builder.variable(signal)
                
                            multisignal_df = variable_builder.buildDataset().select(
                                ['nxcals_timestamp', 'nxcals_value', 'nxcals_variable_name']).dropna().toPandas()
                
                            multisignal_df.set_index(multisignal_df['nxcals_timestamp'], inplace=True)
                            multisignal_df.drop(columns='nxcals_timestamp', inplace=True)
                            multisignal_df.dropna(inplace=True)
                            multisignal_df.sort_index(inplace=True)
                
                            multisignal_dct = dict(tuple(multisignal_df.groupby('nxcals_variable_name')))
                            signal_dfs = []
                            for signal in ['DCBA.15R4.R:U_MAG']:
                                signal_df = multisignal_dct.get(signal, pd.DataFrame(columns=[signal]))
                                signal_df.sort_index(inplace=True)
                                signal_df.drop(columns='nxcals_variable_name', inplace=True)
                                signal_df.rename(columns=dict('nxcals_value': signal), inplace=True)
                                signal_dfs.append(signal_df)
                    
      warnings.warn(warning)



```python
from lhcsmapi.pyedsl.dbsignal.SignalDbQueryFactory import SignalDbQueryFactory
nxcals_system = 'CMW'
nxcals_device = 'RPTE.UA47.RB.A4' # missing last 5
nxcals_property = 'SUB'
signal = 'I_MEAS'
t_start = 1426220469491000000
duration = [(10, 's'), (100, 's')]

i_meas_df = SignalDbQueryFactory().build('nxcals', nxcals_system=nxcals_system,  nxcals_device=[nxcals_device], nxcals_property=nxcals_property, 
                                         signals=[signal], t_start=t_start, duration=duration, spark=spark).read()[0]
if not i_meas_df.empty:
    i_meas_df.plot()
```

    /eos/project/l/lhcsm/venv/lhcsmapi/pyedsl/dbsignal/nxcals/NxcalsDevicePropertySignalDbQuery.py:119: UserWarning: 'Illegal call to remote service for method=EntityResourceClient#findBy(QueryDataCriteria), status=400: Illegal Argument: Entity with {property=SUB, device=RPTE.UA47.RB.A4} was not found in the system CMW\nStacktrace: java.lang.IllegalArgumentException: Entity with {property=SUB, device=RPTE.UA47.RB.A4} was not found in the system CMW\n\tat cern.nxcals.service.internal.EntityResourceService.getEntityConfigs(EntityResourceService.java:124)\n\tat cern.nxcals.service.internal.EntityResourceService.getSourceConfigs(EntityResourceService.java:107)\n\tat cern.nxcals.service.internal.EntityResourceService.findBy(EntityResourceService.java:97)\n\tat cern.nxcals.service.internal.EntityResourceService$$FastClassBySpringCGLIB$$29e04dfb.invoke(<generated>)\n\tat org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\n\tat org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:749)\n\tat org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\n\tat org.springframework.transaction.interceptor.TransactionAspectSupport.invokeWithinTransaction(TransactionAspectSupport.java:294)\n\tat org.springframework.transaction.interceptor.TransactionInterceptor.invoke(TransactionInterceptor.java:98)\n\tat org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186)\n\tat org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:688)\n\tat cern.nxcals.service.internal.EntityResourceService$$EnhancerBySpringCGLIB$$bf515992.findBy(<generated>)\n\tat cern.nxcals.service.rest.EntityResourceController.findBy(EntityResourceController.java:30)\n\tat sun.reflect.GeneratedMethodAccessor298.invoke(Unknown Source)\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.reflect.Method.invoke(Method.java:498)\n\tat org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:189)\n\tat org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:138)\n\tat org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:102)\n\tat org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:895)\n\tat org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:800)\n\tat org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\n\tat org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1038)\n\tat org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:942)\n\tat org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1005)\n\tat org.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:908)\n\tat javax.servlet.http.HttpServlet.service(HttpServlet.java:660)\n\tat org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:882)\n\tat javax.servlet.http.HttpServlet.service(HttpServlet.java:741)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:231)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:101)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.security.kerberos.web.authentication.SpnegoAuthenticationProcessingFilter.doFilter(SpnegoAuthenticationProcessingFilter.java:128)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.boot.actuate.web.trace.servlet.HttpTraceFilter.doFilterInternal(HttpTraceFilter.java:90)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:320)\n\tat org.springframework.security.web.access.intercept.FilterSecurityInterceptor.invoke(FilterSecurityInterceptor.java:127)\n\tat org.springframework.security.web.access.intercept.FilterSecurityInterceptor.doFilter(FilterSecurityInterceptor.java:91)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.access.ExceptionTranslationFilter.doFilter(ExceptionTranslationFilter.java:119)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.session.SessionManagementFilter.doFilter(SessionManagementFilter.java:137)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.authentication.AnonymousAuthenticationFilter.doFilter(AnonymousAuthenticationFilter.java:111)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter.doFilter(SecurityContextHolderAwareRequestFilter.java:170)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.savedrequest.RequestCacheAwareFilter.doFilter(RequestCacheAwareFilter.java:63)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.kerberos.web.authentication.SpnegoAuthenticationProcessingFilter.doFilter(SpnegoAuthenticationProcessingFilter.java:167)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat cern.nxcals.service.security.rbac.RbacAuthenticationFilter.doFilterInternal(RbacAuthenticationFilter.java:39)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.authentication.logout.LogoutFilter.doFilter(LogoutFilter.java:116)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.header.HeaderWriterFilter.doFilterInternal(HeaderWriterFilter.java:74)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.context.SecurityContextPersistenceFilter.doFilter(SecurityContextPersistenceFilter.java:105)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFilter.doFilterInternal(WebAsyncManagerIntegrationFilter.java:56)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\n\tat org.springframework.security.web.FilterChainProxy.doFilterInternal(FilterChainProxy.java:215)\n\tat org.springframework.security.web.FilterChainProxy.doFilter(FilterChainProxy.java:178)\n\tat org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:357)\n\tat org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:270)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:99)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.web.filter.FormContentFilter.doFilterInternal(FormContentFilter.java:92)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.web.filter.HiddenHttpMethodFilter.doFilterInternal(HiddenHttpMethodFilter.java:93)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.boot.actuate.metrics.web.servlet.WebMvcMetricsFilter.filterAndRecordMetrics(WebMvcMetricsFilter.java:117)\n\tat org.springframework.boot.actuate.metrics.web.servlet.WebMvcMetricsFilter.doFilterInternal(WebMvcMetricsFilter.java:106)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:200)\n\tat org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n\tat org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n\tat org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n\tat org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:200)\n\tat org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:96)\n\tat org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:490)\n\tat org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:139)\n\tat org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\n\tat org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:74)\n\tat org.apache.catalina.valves.AbstractAccessLogValve.invoke(AbstractAccessLogValve.java:668)\n\tat org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:343)\n\tat org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:408)\n\tat org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:66)\n\tat org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:834)\n\tat org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1415)\n\tat org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\n\tat java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)\n\tat java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)\n\tat org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\n\tat java.lang.Thread.run(Thread.java:748)\n'
      warnings.warn(str(e))
    /eos/project/l/lhcsm/venv/lhcsmapi/pyedsl/dbsignal/nxcals/NxcalsDevicePropertySignalDbQuery.py:120: UserWarning: NXCALS query failed with the parameters, nxcals_system: CMW, t_start: 2015-03-13T04:20:59.491000000, t_end: 2015-03-13T04:22:49.491000000, 
                            nxcals_devices: ['RPTE.UA47.RB.A4'], nxcals_property: SUB, signal: I_MEAS
    
                            List of empty signals returned.
                            ___________________________________________________________________________________
                            You can reproduce the error using the raw NXCALS API:
    
                            from cern.nxcals.pyquery.builders import *
                            
                            device_property_builder = DevicePropertyQuery.builder(spark) \
                                .system(CMW) \
                                .startTime(2015-03-13T04:20:59.491000000) \
                                .endTime(2015-03-13T04:22:49.491000000) \
    
                            # Populate builder
                            for nxcals_device in ['RPTE.UA47.RB.A4']:
                                device_property_builder = device_property_builder.entity() \
                                    .device(nxcals_device).property(SUB)
                
                            # Execute query
                            multisignal_df = device_property_builder.buildDataset().select(
                                ['acqStamp', I_MEAS, 'device']).dropna().toPandas()
                            multisignal_df.set_index(multisignal_df['acqStamp'], inplace=True)
                            multisignal_df.drop(columns='acqStamp', inplace=True)
                            
                            # Extract signals in the order of input devices
                            multisignal_dct = dict(tuple(multisignal_df.groupby('device')))
                            signal_dfs = []
                            for nxcals_device in ['RPTE.UA47.RB.A4']:
                                signal_df = multisignal_dct.get(nxcals_device, pd.DataFrame(columns=[I_MEAS]))
                                signal_df.sort_index(inplace=True)
                                signal_df.drop(columns='device')
                                signal_dfs.append(signal_df)
                            
      warnings.warn(warning)


## 3.3. Embedded Domain Specific Language in python (pyeDSL)

The Signal class serves it purpose to get a raw data for a single signal. However, it makes it difficult to generalize in order to acquire the same signal across several circuits. In addition, typical processing requires to use additional classes and introduce even more local variables

```python
circuit_type = 'RB'
circuit_name = 'RB.A12'
t_start = '2015-01-13 16:59:11+01:00'
t_end = '2015-01-13 17:15:46+01:00'
db = 'NXCALS'
system = 'PC'

metadata_pc = SignalMetadata.get_circuit_signal_database_metadata(circuit_type, circuit_name, system, db)
I_MEAS = SignalMetadata.get_signal_name(circuit_type, circuit_name, system, db, 'I_MEAS')

i_meas_nxcals_df = Signal().read(db, signal=I_MEAS, t_start=t_start, t_end=t_end, 
                                 nxcals_device=metadata_pc['device'], nxcals_property=metadata_pc['property'], nxcals_system=metadata_pc['system'], 
                                 spark=spark)

i_meas_nxcals_df = SignalUtilities.synchronize_df(i_meas_nxcals_df)
i_meas_nxcals_df = SignalUtilities.convert_indices_to_sec(i_meas_nxcals_df)

```

Several design flaws leading to inconsistency and code duplications:
- <span style="color: red;">use of multiple methods, multiple arguments (duplicated across methods)</span>
- <span style="color: red;">multiple local variables (naming consistency across analysis modules)</span>
- <span style="color: red;">order of methods and arguments (with duck typing) not fixed</span>

The API also does not answer in a generic way the following questions
- What if we want to get current for each circuit?
- What if we want to get several current signals?

Natural languages have certain structure [1]

|Language|Word order|Example|
|--------|---------------------------|--------------|
|English:| {Subject}.{Verb}.{Object}:| John ate cake|
|Japanese:| {Subject}.{Order}.{Verb}:| John-ga keiki-o tabeta|
|-|-| 			John cake ate |
                
One can enforce syntactical order in code:
 - Domain Specific Language – new language, requires parser
 - Embedded Domain Specific Language – extends existing language
 
Furthermore, an eDSL could be implemented following the Fluent interface approach [2]. The use of an eDSL for signal query and processing is not a new concept as there exists already an eDSL in Java used to automatically check signals during Hardware Commisionning campaigns of the LHC [3].

[1] K. Gulordava, Word order variation and dependency length minimisation: a cross-linguistic computational approach, PhD thesis, UniGe,  
[2] https://en.wikipedia.org/wiki/Fluent_interface  
[3] M. Audrain, et al. - Using a Java Embedded Domain-Specific Language for LHC Test Analysis, ICALEPCS2013, San Francisco, CA, USA  


### 3.3.1. QueryBuilder()

We propose a python embedded Domain Specific Language (pyeDSL) for building queries:
- General purpose query
<center>{DB}.{DURATION}.{QUERY_PARAMETERS}.{QUERY}</center>

e.g.
```python
df = QueryBuilder().{with_pm()/with_cals()/with_nxcals()}.with_duration().with_query_parameters()\
	.signal_query().dfs[0]
df = QueryBuilder().{with_pm()/with_cals()/with_nxcals()}.with_timestamp().with_query_parameters()\
	.signal_query().dfs[0]
```

- Circuit-oriented query to generalize query across and within circuit types
<center>{DB}.{DURATION}.{CIRCUIT_TYPE}.{METADATA}.{QUERY}</center>

e.g.  

```python
df = QueryBuilder().{with_pm()/with_cals()/with_nxcals()}.with_duration().with_circuit_type().with_metadata()\
	.signal_query().dfs[0]
df = QueryBuilder().{with_pm()/with_cals()/with_nxcals()}.with_timestamp().with_circuit_type().with_metadata()\
	.signal_query().dfs[0]
```

+ <span style="color: green;">each parameter defined once (validation of input at each stage)</span>
+ <span style="color: green;">single local variable</span>
+ <span style="color: green;">order of operation is fixed</span>
+ <span style="color: green;">support for vector inputs</span>
+ <span style="color: green;">time-dependent metadata</span>


The pyeDSL provides hints on the order of execution


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

QueryBuilder()
```




    Set database name using with_pm(), with_nxcals(spark), with_aft(session) methods.




```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

QueryBuilder().with_nxcals(spark)
```




    Database name properly set to NXCALS. Set time definition: for PM signal query, with_timestamp(),
     for PM event query or AFT, NXCALS signal query, with_duration()




```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

QueryBuilder().with_nxcals(spark).with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(100, 's'), (100, 's')])
```




    Query duration set to t_start=1426220359491000200, t_end=1426220559491000200. Set generic query parameter using with_query_parameters() method, or a circuit signal using with_circuit_type() method.



At the same time it prohibits unsupported operations throwing a meaningful exception


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

QueryBuilder().with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(100, 's'), (100, 's')])
```


    ---------------------------------------------------------------------------

    AttributeError                            Traceback (most recent call last)

    <ipython-input-53-17466de21b88> in <module>()
          1 from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder
          2 
    ----> 3 QueryBuilder().with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(100, 's'), (100, 's')])
    

    /eos/project/l/lhcsm/venv/lhcsmapi/pyedsl/QueryBuilder.py in __getattr__(self, name)
        189         """
        190         if name not in ['query', 'with_pm', 'with_aft', 'with_nxcals']:
    --> 191             raise AttributeError('{}() is not supported. {}'.format(name, self._msg))
        192 
        193     def __repr__(self):


    AttributeError: with_duration() is not supported. Set database name using with_pm(), with_nxcals(spark), with_aft(session) methods.


The following sections deal with: PM event and signal query, NXCALS signal and feature query, and AFT context and fault query. Each case is discussed with a general purpose query, where the user has to provide full data on signal name and its metadata as well as circuit-oriented queries which provide a generic way of querying LHC circuit variables. Both query types are polymorphic and complemented with a set of post-processing functions.


### 3.3.1a. General-Purpose Query - Examples
A sentence constructed this way maintains the differences of query types while providing a common structure

- PM event query


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

source_timestamp_df = QueryBuilder().with_pm() \
    .with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(100, 's'), (100, 's')]) \
    .with_query_parameters(system='FGC', className='51_self_pmd', source='RPTE.UA47.RB.A45') \
    .event_query().df

source_timestamp_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>source</th>
      <th>timestamp</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>RPTE.UA47.RB.A45</td>
      <td>1426220469520000000</td>
    </tr>
  </tbody>
</table>
</div>




```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

source_timestamp_df = QueryBuilder().with_pm() \
    .with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(24*60*60, 's')]) \
    .with_query_parameters(system='QPS', className='DQAMCNMB_PMHSU', source='*') \
    .event_query().df

source_timestamp_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>source</th>
      <th>timestamp</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>B20L5</td>
      <td>1426220469491000000</td>
    </tr>
    <tr>
      <th>1</th>
      <td>C20L5</td>
      <td>1426220517100000000</td>
    </tr>
    <tr>
      <th>2</th>
      <td>A20L5</td>
      <td>1426220518112000000</td>
    </tr>
    <tr>
      <th>3</th>
      <td>A21L5</td>
      <td>1426220625990000000</td>
    </tr>
    <tr>
      <th>4</th>
      <td>B21L5</td>
      <td>1426220866112000000</td>
    </tr>
    <tr>
      <th>5</th>
      <td>C23L4</td>
      <td>1426236802332000000</td>
    </tr>
    <tr>
      <th>6</th>
      <td>B23L4</td>
      <td>1426236839404000000</td>
    </tr>
    <tr>
      <th>7</th>
      <td>A23L4</td>
      <td>1426236839832000000</td>
    </tr>
    <tr>
      <th>8</th>
      <td>C22L4</td>
      <td>1426236949841000000</td>
    </tr>
    <tr>
      <th>9</th>
      <td>C15R4</td>
      <td>1426251285711000000</td>
    </tr>
    <tr>
      <th>10</th>
      <td>B15R4</td>
      <td>1426251337747000000</td>
    </tr>
    <tr>
      <th>11</th>
      <td>A15R4</td>
      <td>1426251388741000000</td>
    </tr>
    <tr>
      <th>12</th>
      <td>B34L8</td>
      <td>1426258716281000000</td>
    </tr>
    <tr>
      <th>13</th>
      <td>C34L8</td>
      <td>1426258747672000000</td>
    </tr>
    <tr>
      <th>14</th>
      <td>A34L8</td>
      <td>1426258747370000000</td>
    </tr>
    <tr>
      <th>15</th>
      <td>C33L8</td>
      <td>1426258835955000000</td>
    </tr>
    <tr>
      <th>16</th>
      <td>C34R7</td>
      <td>1426258853947000000</td>
    </tr>
    <tr>
      <th>17</th>
      <td>A34R7</td>
      <td>1426258854113000000</td>
    </tr>
    <tr>
      <th>18</th>
      <td>A20R3</td>
      <td>1426267931956000000</td>
    </tr>
    <tr>
      <th>19</th>
      <td>B20R3</td>
      <td>1426267983579000000</td>
    </tr>
    <tr>
      <th>20</th>
      <td>C20R3</td>
      <td>1426268004144000000</td>
    </tr>
    <tr>
      <th>21</th>
      <td>B18L5</td>
      <td>1426277626360000000</td>
    </tr>
    <tr>
      <th>22</th>
      <td>A18L5</td>
      <td>1426277679838000000</td>
    </tr>
    <tr>
      <th>23</th>
      <td>C18L5</td>
      <td>1426277680496000000</td>
    </tr>
    <tr>
      <th>24</th>
      <td>A19L5</td>
      <td>1426277903449000000</td>
    </tr>
  </tbody>
</table>
</div>



- PM signal query


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

i_meas_df = QueryBuilder().with_pm() \
    .with_timestamp(1426220469520000000) \
    .with_query_parameters(system='FGC', source='RPTE.UA47.RB.A45', className='51_self_pmd', signal='STATUS.I_MEAS') \
    .signal_query().dfs[0]

i_meas_df.plot()
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fdcafc43e48>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_167_1.png)
    


- NXCALS signal query - device, property


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

i_meas_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(100, 's'), (100, 's')]) \
    .with_query_parameters(nxcals_system='CMW', nxcals_device='RPTE.UA47.RB.A45', nxcals_property='SUB', signal='I_MEAS') \
    .signal_query().dfs[0]

i_meas_df.plot()
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fdcafb60470>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_169_1.png)
    


- NXCALS signal query - variable


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

u_mag_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(100, 's'), (100, 's')]) \
    .with_query_parameters(nxcals_system='CMW', signal='DCBA.15R4.R:U_MAG') \
    .signal_query().dfs[0]

u_mag_df.plot()
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fdcafb77160>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_171_1.png)
    



```python
bmode_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start='2015-03-13 05:20:59.4910002', t_end='2015-04-13 05:20:59.4910002') \
    .with_query_parameters(nxcals_system='CMW', signal='HX:BMODE') \
    .signal_query().dfs[0]

bmode_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>HX:BMODE</th>
    </tr>
    <tr>
      <th>nxcals_timestamp</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1426513864882000000</th>
      <td>SETUP</td>
    </tr>
    <tr>
      <th>1426515551191000000</th>
      <td>NOBEAM</td>
    </tr>
    <tr>
      <th>1426597426610000000</th>
      <td>SETUP</td>
    </tr>
    <tr>
      <th>1426597576404000000</th>
      <td>STABLE</td>
    </tr>
    <tr>
      <th>1426598591040000000</th>
      <td>UNSTABLE</td>
    </tr>
  </tbody>
</table>
</div>



- NXCALS feature query - device, property

I_MEAS from beam injection, through beam acceleration, to stable beams


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

i_meas_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start=1526898157236000000, t_end=1526903352338000000) \
    .with_query_parameters(nxcals_system='CMW', nxcals_device='RPTE.UA23.RB.A12', nxcals_property='SUB', signal='I_MEAS') \
    .feature_query(['mean', 'std', 'max', 'min', 'count']).df

i_meas_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>device</th>
      <th>count</th>
      <th>mean</th>
      <th>std</th>
      <th>min</th>
      <th>max</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>RPTE.UA23.RB.A12</td>
      <td>2498</td>
      <td>5374.768659</td>
      <td>3315.972856</td>
      <td>757.18</td>
      <td>10978.8</td>
    </tr>
  </tbody>
</table>
</div>



- NXCALS feature query - variable

U_MAG from beam injection, through beam acceleration, to stable beams


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

u_mag_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start=1526898157236000000, t_end=1526903352338000000) \
    .with_query_parameters(nxcals_system='CMW', signal='DCBB.8L2.R:U_MAG') \
    .feature_query(['mean', 'std', 'max', 'min', 'count']).df

u_mag_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>nxcals_variable_name</th>
      <th>count</th>
      <th>mean</th>
      <th>std</th>
      <th>min</th>
      <th>max</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>DCBB.8L2.R:U_MAG</td>
      <td>51951</td>
      <td>-0.196706</td>
      <td>0.375195</td>
      <td>-0.975332</td>
      <td>0.002113</td>
    </tr>
  </tbody>
</table>
</div>



### 3.3.1b. General-Purpose Query - Polymorphism
- multiple signal, mutliple sources, multiple systems, multiple className


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

i_meas_df = QueryBuilder().with_pm() \
    .with_timestamp(1426220469520000000) \
    .with_query_parameters(system=['FGC', 'FGC'], source=['RPTE.UA47.RB.A45', 'RPTE.UA47.RB.A45'], 
                           className=['51_self_pmd', '51_self_pmd'], signal=['STATUS.I_MEAS', 'STATUS.I_REF']) \
    .signal_query().dfs

ax=i_meas_df[0].plot()
i_meas_df[1].plot(ax=ax, grid=True)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fdcaf87ab70>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_178_1.png)
    


- multiple signal, single source, single system, single className


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

i_meas_df = QueryBuilder().with_pm() \
    .with_timestamp(1426220469520000000) \
    .with_query_parameters(system='FGC', source='RPTE.UA47.RB.A45', 
                           className='51_self_pmd', signal=['STATUS.I_MEAS', 'STATUS.I_REF']) \
    .signal_query().dfs

ax=i_meas_df[0].plot()
i_meas_df[1].plot(ax=ax, grid=True)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fdcaf7fbb70>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_180_1.png)
    


### 3.3.1c. Circuit-Oriented Query - Examples
- PM event query


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

source_timestamp_df = QueryBuilder().with_pm() \
    .with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(100, 's'), (100, 's')]) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A45', system='PC') \
    .event_query().df

source_timestamp_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>source</th>
      <th>timestamp</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>RPTE.UA47.RB.A45</td>
      <td>1426220469520000000</td>
    </tr>
  </tbody>
</table>
</div>




```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

source_timestamp_df = QueryBuilder().with_pm() \
    .with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(24*60*60, 's')]) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A45', system='QH', source='*') \
    .event_query().df

source_timestamp_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>source</th>
      <th>timestamp</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>B20L5</td>
      <td>1426220469491000000</td>
    </tr>
    <tr>
      <th>1</th>
      <td>C20L5</td>
      <td>1426220517100000000</td>
    </tr>
    <tr>
      <th>2</th>
      <td>A20L5</td>
      <td>1426220518112000000</td>
    </tr>
    <tr>
      <th>3</th>
      <td>A21L5</td>
      <td>1426220625990000000</td>
    </tr>
    <tr>
      <th>4</th>
      <td>B21L5</td>
      <td>1426220866112000000</td>
    </tr>
    <tr>
      <th>5</th>
      <td>C23L4</td>
      <td>1426236802332000000</td>
    </tr>
    <tr>
      <th>6</th>
      <td>B23L4</td>
      <td>1426236839404000000</td>
    </tr>
    <tr>
      <th>7</th>
      <td>A23L4</td>
      <td>1426236839832000000</td>
    </tr>
    <tr>
      <th>8</th>
      <td>C22L4</td>
      <td>1426236949841000000</td>
    </tr>
    <tr>
      <th>9</th>
      <td>C15R4</td>
      <td>1426251285711000000</td>
    </tr>
    <tr>
      <th>10</th>
      <td>B15R4</td>
      <td>1426251337747000000</td>
    </tr>
    <tr>
      <th>11</th>
      <td>A15R4</td>
      <td>1426251388741000000</td>
    </tr>
    <tr>
      <th>12</th>
      <td>B34L8</td>
      <td>1426258716281000000</td>
    </tr>
    <tr>
      <th>13</th>
      <td>C34L8</td>
      <td>1426258747672000000</td>
    </tr>
    <tr>
      <th>14</th>
      <td>A34L8</td>
      <td>1426258747370000000</td>
    </tr>
    <tr>
      <th>15</th>
      <td>C33L8</td>
      <td>1426258835955000000</td>
    </tr>
    <tr>
      <th>16</th>
      <td>C34R7</td>
      <td>1426258853947000000</td>
    </tr>
    <tr>
      <th>17</th>
      <td>A34R7</td>
      <td>1426258854113000000</td>
    </tr>
    <tr>
      <th>18</th>
      <td>A20R3</td>
      <td>1426267931956000000</td>
    </tr>
    <tr>
      <th>19</th>
      <td>B20R3</td>
      <td>1426267983579000000</td>
    </tr>
    <tr>
      <th>20</th>
      <td>C20R3</td>
      <td>1426268004144000000</td>
    </tr>
    <tr>
      <th>21</th>
      <td>B18L5</td>
      <td>1426277626360000000</td>
    </tr>
    <tr>
      <th>22</th>
      <td>A18L5</td>
      <td>1426277679838000000</td>
    </tr>
    <tr>
      <th>23</th>
      <td>C18L5</td>
      <td>1426277680496000000</td>
    </tr>
    <tr>
      <th>24</th>
      <td>A19L5</td>
      <td>1426277903449000000</td>
    </tr>
  </tbody>
</table>
</div>



- PM signal query


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

i_meas_df = QueryBuilder().with_pm() \
    .with_timestamp(1426220469520000000) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A45', system='PC', signal='I_MEAS') \
    .signal_query().dfs[0]

i_meas_df.plot(grid=True)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fdcaf87abe0>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_185_1.png)
    


- NXCALS signal query - device, property


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

i_meas_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(100, 's'), (100, 's')]) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A45', system='PC', signal='I_MEAS') \
    .signal_query().dfs[0]

i_meas_df.plot(grid=True)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fdcaf6b9710>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_187_1.png)
    


- NXCALS signal query - variable


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

u_mag_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(100, 's'), (100, 's')]) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A45', system='BUSBAR', signal='U_MAG', wildcard={'BUSBAR': 'DCBA.15R4.R'}) \
    .signal_query().dfs[0]

u_mag_df.plot(grid=True)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fdcaf6a1278>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_189_1.png)
    


- NXCALS feature query - device, property

I_MEAS from beam injection, through beam acceleration, to stable beams


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

i_meas_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start=1526898157236000000, t_end=1526903352338000000) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A12', system='PC', signal='I_MEAS') \
    .feature_query(['mean', 'std', 'max', 'min', 'count']).df

i_meas_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>device</th>
      <th>count</th>
      <th>mean</th>
      <th>std</th>
      <th>min</th>
      <th>max</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>RPTE.UA23.RB.A12</td>
      <td>2498</td>
      <td>5374.768659</td>
      <td>3315.972856</td>
      <td>757.18</td>
      <td>10978.8</td>
    </tr>
  </tbody>
</table>
</div>



- NXCALS feature query - variable

U_MAG from beam injection, through beam acceleration, to stable beams


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

u_mag_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start=1526898157236000000, t_end=1526903352338000000) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A12', system='BUSBAR', signal='U_MAG', wildcard={'BUSBAR': 'DCBB.8L2.R'}) \
    .feature_query(['mean', 'std', 'max', 'min', 'count']).df

u_mag_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>nxcals_variable_name</th>
      <th>count</th>
      <th>mean</th>
      <th>std</th>
      <th>min</th>
      <th>max</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>DCBB.8L2.R:U_MAG</td>
      <td>51951</td>
      <td>-0.196706</td>
      <td>0.375195</td>
      <td>-0.975332</td>
      <td>0.002113</td>
    </tr>
  </tbody>
</table>
</div>



### 3.3.1d. Circuit-Oriented Query - Polymorphism
- Multiple circuit names


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

i_meas_dfs = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(100, 's'), (100, 's')]) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name=['RB.A12', 'RB.A45'], system='PC', signal='I_MEAS')\
    .signal_query().dfs

ax = i_meas_dfs[0].plot()
i_meas_dfs[1].plot(ax=ax, grid=True)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fdcaf6230f0>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_195_1.png)
    


- Multiple system names


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

u_hts_dfs = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start=1544622149598000000, duration=[(50, 's'), (150, 's')]) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A12', system=['LEADS_EVEN', 'LEADS_ODD'], signal='U_HTS') \
    .signal_query().dfs

ax = u_hts_dfs[0].plot()
u_hts_dfs[1].plot(ax=ax)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fdcaf57e240>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_197_1.png)
    


- Multi-signal query

Polymorphic `signal_query()` call for NXCALS executes sequential queries of one signal at a time. This turns to be slow for multiple signals. Instead, `multi_signal_query()` queries multiple signals in parallel. This allows significantly reducing the query time (even 10x).


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder
import matplotlib.pyplot as plt

signal_dfs = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start='2018-12-03 05:20:00+01:00', t_end='2018-12-03 05:30:00+01:00') \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A12', system='DIODE_RB', signal='U_DIODE_RB', wildcard={'MAGNET': ['MB.A17L2', 'MB.A9L2']}) \
    .multi_signal_query() \
    .synchronize_time() \
    .convert_index_to_sec().dfs
    
fig, ax = plt.subplots(figsize=(13, 6.5))
for signal_df in signal_dfs:
    signal_df.plot(ax=ax, grid=True)
```


    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_199_0.png)
    



```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder
import matplotlib.pyplot as plt

signal_dfs = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start='2015-03-13 05:20:59.491000000', t_end='2015-03-13 05:22:49.491000000') \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name=['RB.A45', 'RB.A12'], system='PC', signal='I_MEAS') \
    .signal_query() \
    .synchronize_time() \
    .convert_index_to_sec().dfs

fig, ax = plt.subplots(figsize=(13, 6.5))
for signal_df in signal_dfs:
    signal_df.plot(ax=ax, grid=True)
```


    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_200_0.png)
    


- Multiple signal names


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

u_ext_dfs = QueryBuilder().with_pm() \
    .with_timestamp(1544622149598000000) \
    .with_circuit_type('RQ') \
    .with_metadata(circuit_name='RQD.A12', system='QDS', signal=['U_1_EXT', 'U_2_EXT'],
                        source='16L2', wildcard={'CELL': '16L2'}) \
    .signal_query().dfs

ax = u_ext_dfs[0].plot()
u_ext_dfs[1].plot(ax=ax)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fdcaf551320>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_202_1.png)
    


### 3.3.1e. Advanced Feature Query

NXCALS enables calculation of signal features such as min, max, mean, std, count directly on the cluster without the need for costly query of the signal and performing calculation locally. This approach enables parallel computing on the cluster. To this end, a query should contain an element enabling a group by operation. Each group by operation allows for executing computation in parallel. For the sake of compactness, we only show examples for circuit-oriented query, however, the same principle applies to the general-purpose queries.
- Feature query of multiple signals for the same period of time


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

i_meas_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start=1526898157236000000, t_end=1526903352338000000) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='*', system='PC', signal='I_MEAS') \
    .feature_query(['mean', 'std', 'max', 'min', 'count']).df

i_meas_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>device</th>
      <th>count</th>
      <th>mean</th>
      <th>std</th>
      <th>min</th>
      <th>max</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>RPTE.UA63.RB.A56</td>
      <td>2498</td>
      <td>5372.636369</td>
      <td>3314.646638</td>
      <td>756.90</td>
      <td>10974.36</td>
    </tr>
    <tr>
      <th>1</th>
      <td>RPTE.UA87.RB.A81</td>
      <td>2498</td>
      <td>5371.596990</td>
      <td>3314.230379</td>
      <td>756.82</td>
      <td>10973.21</td>
    </tr>
    <tr>
      <th>2</th>
      <td>RPTE.UA83.RB.A78</td>
      <td>2498</td>
      <td>5369.226934</td>
      <td>3312.676833</td>
      <td>756.47</td>
      <td>10967.94</td>
    </tr>
    <tr>
      <th>3</th>
      <td>RPTE.UA27.RB.A23</td>
      <td>2498</td>
      <td>5373.973151</td>
      <td>3315.324716</td>
      <td>757.04</td>
      <td>10976.46</td>
    </tr>
    <tr>
      <th>4</th>
      <td>RPTE.UA43.RB.A34</td>
      <td>2497</td>
      <td>5373.559131</td>
      <td>3314.074552</td>
      <td>756.95</td>
      <td>10975.16</td>
    </tr>
    <tr>
      <th>5</th>
      <td>RPTE.UA67.RB.A67</td>
      <td>2498</td>
      <td>5374.329179</td>
      <td>3315.855302</td>
      <td>757.18</td>
      <td>10978.56</td>
    </tr>
    <tr>
      <th>6</th>
      <td>RPTE.UA47.RB.A45</td>
      <td>2497</td>
      <td>5370.954557</td>
      <td>3312.837184</td>
      <td>756.71</td>
      <td>10971.44</td>
    </tr>
    <tr>
      <th>7</th>
      <td>RPTE.UA23.RB.A12</td>
      <td>2498</td>
      <td>5374.768659</td>
      <td>3315.972856</td>
      <td>757.18</td>
      <td>10978.80</td>
    </tr>
  </tbody>
</table>
</div>



- Feature query of multiple signals with the same period of time subdivided into three intervals - group by signal name and interval


```python
t_start_injs = 1526898157236000000
t_end_injs = 1526899957236000000
t_start_sbs = 1526901552338000000
t_end_sbs = 1526903352338000000
```


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

i_meas_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start=t_start_injs, t_end=t_end_sbs) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A12', system='PC', signal='I_MEAS')\
    .signal_query().dfs[0]

ax = i_meas_df.plot(figsize=(10,5), linewidth=5)
ax.axvspan(xmin=t_start_injs, xmax=t_end_injs, facecolor='xkcd:goldenrod')
ax.axvspan(xmin=t_end_injs, xmax=t_start_sbs, facecolor='xkcd:grey')
ax.axvspan(xmin=t_start_sbs, xmax=t_end_sbs, facecolor='xkcd:green')
```




    <matplotlib.patches.Polygon at 0x7fdcaf39df60>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_207_1.png)
    


Function translate introduces a mapping based on the time column. Here, we consider three subintervals for beam injection, beam acceleration, and stable beams.

As a result, the time column forms a partition and can be executed in parallel.


```python
from pyspark.sql.functions import udf
from pyspark.sql.types import IntegerType

def translate(timestamp):
    if(timestamp >= t_start_injs and timestamp < t_end_injs):
        return 1
    
    if(timestamp >=  t_end_injs and timestamp < t_start_sbs):
        return 2
    
    if(timestamp >= t_start_sbs and timestamp <= t_end_sbs):
        return 3
    
    return -1

translate_udf = udf(translate, IntegerType())
```

The translate function should be passed as a function argument


```python
i_meas_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start=t_start_injs, t_end=t_end_sbs) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='*', system='PC', signal='I_MEAS') \
    .feature_query(['mean', 'std', 'max', 'min', 'count'], function=translate_udf).df

i_meas_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>device</th>
      <th>class</th>
      <th>count</th>
      <th>mean</th>
      <th>std</th>
      <th>min</th>
      <th>max</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>RPTE.UA43.RB.A34</td>
      <td>1</td>
      <td>60</td>
      <td>756.959833</td>
      <td>0.001291</td>
      <td>756.95</td>
      <td>756.96</td>
    </tr>
    <tr>
      <th>1</th>
      <td>RPTE.UA63.RB.A56</td>
      <td>3</td>
      <td>60</td>
      <td>10974.350167</td>
      <td>0.001291</td>
      <td>10974.35</td>
      <td>10974.36</td>
    </tr>
    <tr>
      <th>2</th>
      <td>RPTE.UA47.RB.A45</td>
      <td>3</td>
      <td>60</td>
      <td>10971.439667</td>
      <td>0.001810</td>
      <td>10971.43</td>
      <td>10971.44</td>
    </tr>
    <tr>
      <th>3</th>
      <td>RPTE.UA47.RB.A45</td>
      <td>2</td>
      <td>2377</td>
      <td>5346.059971</td>
      <td>3193.562789</td>
      <td>756.71</td>
      <td>10970.89</td>
    </tr>
    <tr>
      <th>4</th>
      <td>RPTE.UA23.RB.A12</td>
      <td>3</td>
      <td>60</td>
      <td>10978.781500</td>
      <td>0.007552</td>
      <td>10978.77</td>
      <td>10978.80</td>
    </tr>
    <tr>
      <th>5</th>
      <td>RPTE.UA83.RB.A78</td>
      <td>2</td>
      <td>2378</td>
      <td>5344.349941</td>
      <td>3193.572017</td>
      <td>756.47</td>
      <td>10967.54</td>
    </tr>
    <tr>
      <th>6</th>
      <td>RPTE.UA43.RB.A34</td>
      <td>2</td>
      <td>2377</td>
      <td>5348.695949</td>
      <td>3194.776339</td>
      <td>756.95</td>
      <td>10974.68</td>
    </tr>
    <tr>
      <th>7</th>
      <td>RPTE.UA67.RB.A67</td>
      <td>1</td>
      <td>60</td>
      <td>757.188833</td>
      <td>0.003237</td>
      <td>757.18</td>
      <td>757.19</td>
    </tr>
    <tr>
      <th>8</th>
      <td>RPTE.UA87.RB.A81</td>
      <td>3</td>
      <td>60</td>
      <td>10973.209167</td>
      <td>0.002787</td>
      <td>10973.20</td>
      <td>10973.21</td>
    </tr>
    <tr>
      <th>9</th>
      <td>RPTE.UA27.RB.A23</td>
      <td>2</td>
      <td>2378</td>
      <td>5349.106434</td>
      <td>3196.138736</td>
      <td>757.04</td>
      <td>10976.10</td>
    </tr>
    <tr>
      <th>10</th>
      <td>RPTE.UA83.RB.A78</td>
      <td>3</td>
      <td>60</td>
      <td>10967.940000</td>
      <td>0.000000</td>
      <td>10967.94</td>
      <td>10967.94</td>
    </tr>
    <tr>
      <th>11</th>
      <td>RPTE.UA47.RB.A45</td>
      <td>1</td>
      <td>60</td>
      <td>756.710000</td>
      <td>0.000000</td>
      <td>756.71</td>
      <td>756.71</td>
    </tr>
    <tr>
      <th>12</th>
      <td>RPTE.UA83.RB.A78</td>
      <td>1</td>
      <td>60</td>
      <td>756.472000</td>
      <td>0.004034</td>
      <td>756.47</td>
      <td>756.48</td>
    </tr>
    <tr>
      <th>13</th>
      <td>RPTE.UA27.RB.A23</td>
      <td>1</td>
      <td>60</td>
      <td>757.046833</td>
      <td>0.004691</td>
      <td>757.04</td>
      <td>757.05</td>
    </tr>
    <tr>
      <th>14</th>
      <td>RPTE.UA87.RB.A81</td>
      <td>2</td>
      <td>2378</td>
      <td>5346.697637</td>
      <td>3195.063002</td>
      <td>756.83</td>
      <td>10972.79</td>
    </tr>
    <tr>
      <th>15</th>
      <td>RPTE.UA87.RB.A81</td>
      <td>1</td>
      <td>60</td>
      <td>756.829167</td>
      <td>0.002787</td>
      <td>756.82</td>
      <td>756.83</td>
    </tr>
    <tr>
      <th>16</th>
      <td>RPTE.UA23.RB.A12</td>
      <td>2</td>
      <td>2378</td>
      <td>5349.879361</td>
      <td>3196.753501</td>
      <td>757.19</td>
      <td>10978.41</td>
    </tr>
    <tr>
      <th>17</th>
      <td>RPTE.UA67.RB.A67</td>
      <td>2</td>
      <td>2378</td>
      <td>5349.423865</td>
      <td>3196.631627</td>
      <td>757.18</td>
      <td>10978.15</td>
    </tr>
    <tr>
      <th>18</th>
      <td>RPTE.UA63.RB.A56</td>
      <td>2</td>
      <td>2378</td>
      <td>5347.758797</td>
      <td>3195.477077</td>
      <td>756.90</td>
      <td>10973.97</td>
    </tr>
    <tr>
      <th>19</th>
      <td>RPTE.UA43.RB.A34</td>
      <td>3</td>
      <td>60</td>
      <td>10975.154833</td>
      <td>0.005039</td>
      <td>10975.15</td>
      <td>10975.16</td>
    </tr>
    <tr>
      <th>20</th>
      <td>RPTE.UA23.RB.A12</td>
      <td>1</td>
      <td>60</td>
      <td>757.201667</td>
      <td>0.007847</td>
      <td>757.18</td>
      <td>757.22</td>
    </tr>
    <tr>
      <th>21</th>
      <td>RPTE.UA27.RB.A23</td>
      <td>3</td>
      <td>60</td>
      <td>10976.450333</td>
      <td>0.001810</td>
      <td>10976.45</td>
      <td>10976.46</td>
    </tr>
    <tr>
      <th>22</th>
      <td>RPTE.UA63.RB.A56</td>
      <td>1</td>
      <td>60</td>
      <td>756.903667</td>
      <td>0.004860</td>
      <td>756.90</td>
      <td>756.91</td>
    </tr>
    <tr>
      <th>23</th>
      <td>RPTE.UA67.RB.A67</td>
      <td>3</td>
      <td>60</td>
      <td>10978.550167</td>
      <td>0.001291</td>
      <td>10978.55</td>
      <td>10978.56</td>
    </tr>
  </tbody>
</table>
</div>



The same method applied to NXCALS signals based on variable


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

u_mag_ab_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start=t_start_injs, t_end=t_end_sbs) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A12', system='BUSBAR', signal='U_MAG', wildcard={'BUSBAR': 'DCBB.8L2.R'}) \
    .feature_query(['mean', 'std', 'max', 'min', 'count'], function=translate_udf).df

u_mag_ab_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>nxcals_variable_name</th>
      <th>class</th>
      <th>count</th>
      <th>mean</th>
      <th>std</th>
      <th>min</th>
      <th>max</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>DCBB.8L2.R:U_MAG</td>
      <td>3</td>
      <td>18000</td>
      <td>-0.001983</td>
      <td>0.002882</td>
      <td>-0.034035</td>
      <td>0.002003</td>
    </tr>
    <tr>
      <th>1</th>
      <td>DCBB.8L2.R:U_MAG</td>
      <td>1</td>
      <td>18000</td>
      <td>-0.001861</td>
      <td>0.002844</td>
      <td>-0.005992</td>
      <td>0.002113</td>
    </tr>
    <tr>
      <th>2</th>
      <td>DCBB.8L2.R:U_MAG</td>
      <td>2</td>
      <td>15951</td>
      <td>-0.636315</td>
      <td>0.423767</td>
      <td>-0.975332</td>
      <td>0.002041</td>
    </tr>
  </tbody>
</table>
</div>



This method can be used together with signal decimation, i.e., taking every nth sample.

For example this can be useful to query QPS board A and B which share the same channel and samples are shifted by 5 so that 
- every 10-th sample belongs to board A (or B), decimation=10


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

u_mag_a_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start=t_start_injs, t_end=t_end_sbs) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A12', system='BUSBAR', signal='U_MAG', wildcard={'BUSBAR': 'DCBB.8L2.R'}) \
    .feature_query(['mean', 'std', 'max', 'min', 'count'], function=translate_udf, decimation=10).df

u_mag_a_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>nxcals_variable_name</th>
      <th>class</th>
      <th>count</th>
      <th>mean</th>
      <th>std</th>
      <th>min</th>
      <th>max</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>DCBB.8L2.R:U_MAG</td>
      <td>3</td>
      <td>1800</td>
      <td>0.000812</td>
      <td>0.000529</td>
      <td>-0.012705</td>
      <td>0.002003</td>
    </tr>
    <tr>
      <th>1</th>
      <td>DCBB.8L2.R:U_MAG</td>
      <td>1</td>
      <td>1800</td>
      <td>0.000965</td>
      <td>0.000434</td>
      <td>-0.000463</td>
      <td>0.002113</td>
    </tr>
    <tr>
      <th>2</th>
      <td>DCBB.8L2.R:U_MAG</td>
      <td>2</td>
      <td>1595</td>
      <td>-0.633601</td>
      <td>0.423847</td>
      <td>-0.969405</td>
      <td>0.002041</td>
    </tr>
  </tbody>
</table>
</div>



- every 5+10-th sample belongs to board B (or A), decimation=10, shift=5


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

u_mag_b_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start=t_start_injs, t_end=t_end_sbs) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A12', system='BUSBAR', signal='U_MAG', wildcard={'BUSBAR': 'DCBB.8L2.R'}) \
    .feature_query(['mean', 'std', 'max', 'min', 'count'], function=translate_udf, decimation=10, shift=5).df

u_mag_b_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>nxcals_variable_name</th>
      <th>class</th>
      <th>count</th>
      <th>mean</th>
      <th>std</th>
      <th>min</th>
      <th>max</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>DCBB.8L2.R:U_MAG</td>
      <td>3</td>
      <td>1800</td>
      <td>-0.004754</td>
      <td>0.000877</td>
      <td>-0.034035</td>
      <td>-0.003425</td>
    </tr>
    <tr>
      <th>1</th>
      <td>DCBB.8L2.R:U_MAG</td>
      <td>1</td>
      <td>1800</td>
      <td>-0.004656</td>
      <td>0.000441</td>
      <td>-0.005992</td>
      <td>-0.003361</td>
    </tr>
    <tr>
      <th>2</th>
      <td>DCBB.8L2.R:U_MAG</td>
      <td>2</td>
      <td>1595</td>
      <td>-0.639233</td>
      <td>0.423941</td>
      <td>-0.975332</td>
      <td>-0.003052</td>
    </tr>
  </tbody>
</table>
</div>



- with polymorphism one can query 1248 busbar at once (in two batches of 624 due to the limit of 1000 signal per query)


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

u_mag_ab_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start=t_start_injs, t_end=t_end_sbs) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='*', system='BUSBAR', signal='U_MAG', wildcard={'BUSBAR': '*'}) \
    .feature_query(['mean', 'std', 'max', 'min', 'count'], function=translate_udf).df

u_mag_ab_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>nxcals_variable_name</th>
      <th>class</th>
      <th>count</th>
      <th>mean</th>
      <th>std</th>
      <th>min</th>
      <th>max</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>DCBA.14L4.R:U_MAG</td>
      <td>1</td>
      <td>36000</td>
      <td>0.000171</td>
      <td>0.000603</td>
      <td>-0.000796</td>
      <td>0.001179</td>
    </tr>
    <tr>
      <th>1</th>
      <td>DCBB.B22L2.R:U_MAG</td>
      <td>1</td>
      <td>18000</td>
      <td>-0.001283</td>
      <td>0.000557</td>
      <td>-0.003088</td>
      <td>0.000341</td>
    </tr>
    <tr>
      <th>2</th>
      <td>DCBB.11L2.R:U_MAG</td>
      <td>3</td>
      <td>18000</td>
      <td>0.001457</td>
      <td>0.002587</td>
      <td>-0.002121</td>
      <td>0.048651</td>
    </tr>
    <tr>
      <th>3</th>
      <td>DCBA.15R4.R:U_MAG</td>
      <td>2</td>
      <td>15951</td>
      <td>0.632161</td>
      <td>0.423091</td>
      <td>-0.001566</td>
      <td>0.967469</td>
    </tr>
    <tr>
      <th>4</th>
      <td>DCBB.A23R1.L:U_MAG</td>
      <td>1</td>
      <td>18000</td>
      <td>-0.002505</td>
      <td>0.000457</td>
      <td>-0.004140</td>
      <td>-0.000787</td>
    </tr>
  </tbody>
</table>
</div>



### 3.3.1f. AFT Query

1. `/api/public/v1/accelerators`

  Retrieve all available accelerators.


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

QueryBuilder().with_aft(session) \
    .context_query('accelerators')
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>ISO_HRS</td>
      <td>ISOLDE HRS</td>
    </tr>
    <tr>
      <th>1</th>
      <td>LINAC4</td>
      <td>LINAC4</td>
    </tr>
    <tr>
      <th>2</th>
      <td>CLEAR</td>
      <td>CLEAR</td>
    </tr>
    <tr>
      <th>3</th>
      <td>LEIR</td>
      <td>LEIR</td>
    </tr>
    <tr>
      <th>4</th>
      <td>ISO_GPS</td>
      <td>ISOLDE GPS</td>
    </tr>
    <tr>
      <th>5</th>
      <td>SPS</td>
      <td>SPS</td>
    </tr>
    <tr>
      <th>6</th>
      <td>LINAC2</td>
      <td>LINAC2</td>
    </tr>
    <tr>
      <th>7</th>
      <td>AD</td>
      <td>AD</td>
    </tr>
    <tr>
      <th>8</th>
      <td>ELENA</td>
      <td>ELENA</td>
    </tr>
    <tr>
      <th>9</th>
      <td>LINAC3</td>
      <td>LINAC3</td>
    </tr>
    <tr>
      <th>10</th>
      <td>PS</td>
      <td>PS</td>
    </tr>
    <tr>
      <th>11</th>
      <td>PSB</td>
      <td>PSB</td>
    </tr>
    <tr>
      <th>12</th>
      <td>LHC</td>
      <td>LHC</td>
    </tr>
    <tr>
      <th>13</th>
      <td>ISO_REX_HIE</td>
      <td>ISOLDE REX-HIE</td>
    </tr>
  </tbody>
</table>
</div>



2. `/api/public/v1/accelerators/{acceleratorId}/properties`

  Retrieve the properties which are specific to the given accelerator.


```python
QueryBuilder().with_aft(session) \
    .context_query('accelerators/{acceleratorId}/properties', context_id='LHC')
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>group</th>
      <th>id</th>
      <th>name</th>
      <th>pattern</th>
      <th>values</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>LHC Fault Context</td>
      <td>303161</td>
      <td>Beam Mode</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>1</th>
      <td>LHC Fault Context</td>
      <td>303160</td>
      <td>Operational Mode</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Impact</td>
      <td>193273</td>
      <td>Prevents Injection</td>
      <td>None</td>
      <td>[No, Yes]</td>
    </tr>
    <tr>
      <th>3</th>
      <td>R2E Status</td>
      <td>266828</td>
      <td>R2E Status</td>
      <td>None</td>
      <td>[R2E_CANDIDATE, NOT_R2E_RELATED, R2E_REJECTED,...</td>
    </tr>
    <tr>
      <th>4</th>
      <td>LHC Fault Context</td>
      <td>303158</td>
      <td>Accelerator Mode</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>5</th>
      <td>LHC Fault Context</td>
      <td>303163</td>
      <td>Time in Fill (ms)</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>6</th>
      <td>LHC Fault Context</td>
      <td>303166</td>
      <td>Injection Scheme</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>7</th>
      <td>LHC Fault Context</td>
      <td>303164</td>
      <td>Time in Beam Mode</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>8</th>
      <td>Impact</td>
      <td>193272</td>
      <td>Precycle Needed</td>
      <td>None</td>
      <td>[No, Yes]</td>
    </tr>
    <tr>
      <th>9</th>
      <td>LHC Fault Context</td>
      <td>303159</td>
      <td>Fill No</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>10</th>
      <td>Impact</td>
      <td>193271</td>
      <td>RP Needed</td>
      <td>None</td>
      <td>[No, Yes]</td>
    </tr>
    <tr>
      <th>11</th>
      <td>LHC Fault Context</td>
      <td>303165</td>
      <td>Time in Beam Mode (ms)</td>
      <td>None</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>12</th>
      <td>LHC Fault Context</td>
      <td>303162</td>
      <td>Time in Fill</td>
      <td>None</td>
      <td>[]</td>
    </tr>
  </tbody>
</table>
</div>



3. `/api/public/v1/accelerators/{acceleratorId}/systems`

  Retrieve the systems of the given accelerator.


```python
QueryBuilder().with_aft(session) \
    .context_query('accelerators/{acceleratorId}/systems', context_id='LHC')
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>children</th>
      <th>id</th>
      <th>name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>[{'id': 31395, 'name': 'Access Infrastructure ...</td>
      <td>83803</td>
      <td>Access Infrastructure</td>
    </tr>
    <tr>
      <th>1</th>
      <td>[{'id': 371383, 'name': 'Machine Interlock Sys...</td>
      <td>14</td>
      <td>Machine Interlock Systems</td>
    </tr>
    <tr>
      <th>2</th>
      <td>[]</td>
      <td>16739</td>
      <td>Beam-induced Quench</td>
    </tr>
    <tr>
      <th>3</th>
      <td>[{'id': 68, 'name': 'Cryogenics » Equipment', ...</td>
      <td>16</td>
      <td>Cryogenics</td>
    </tr>
    <tr>
      <th>4</th>
      <td>[{'id': 30655, 'name': 'LBDS » TCDQ', 'childre...</td>
      <td>10</td>
      <td>LBDS</td>
    </tr>
    <tr>
      <th>5</th>
      <td>[{'id': 55, 'name': 'Collimation » Interlocks ...</td>
      <td>13</td>
      <td>Collimation</td>
    </tr>
    <tr>
      <th>6</th>
      <td>[{'id': 89, 'name': 'Transverse Damper » Contr...</td>
      <td>21</td>
      <td>Transverse Damper</td>
    </tr>
    <tr>
      <th>7</th>
      <td>[{'id': 31171, 'name': 'Cooling and Ventilatio...</td>
      <td>83789</td>
      <td>Cooling and Ventilation</td>
    </tr>
    <tr>
      <th>8</th>
      <td>[{'id': 86, 'name': 'Radio Frequency » Hardwar...</td>
      <td>20</td>
      <td>Radio Frequency</td>
    </tr>
    <tr>
      <th>9</th>
      <td>[{'id': 106, 'name': 'SIS » Controls', 'childr...</td>
      <td>26</td>
      <td>SIS</td>
    </tr>
    <tr>
      <th>10</th>
      <td>[{'id': 107, 'name': 'QPS » Controller', 'chil...</td>
      <td>27</td>
      <td>QPS</td>
    </tr>
    <tr>
      <th>11</th>
      <td>[{'id': 79, 'name': 'Beam Injection » Oscillat...</td>
      <td>18</td>
      <td>Beam Injection</td>
    </tr>
    <tr>
      <th>12</th>
      <td>[{'id': 29456, 'name': 'Beam Exciters » Apertu...</td>
      <td>29453</td>
      <td>Beam Exciters</td>
    </tr>
    <tr>
      <th>13</th>
      <td>[{'id': 21232, 'name': 'Injection Systems » MK...</td>
      <td>21231</td>
      <td>Injection Systems</td>
    </tr>
    <tr>
      <th>14</th>
      <td>[{'id': 66, 'name': 'Accelerator Controls » CM...</td>
      <td>15</td>
      <td>Accelerator Controls</td>
    </tr>
    <tr>
      <th>15</th>
      <td>[{'id': 45, 'name': 'Beam Losses » Other', 'ch...</td>
      <td>11</td>
      <td>Beam Losses</td>
    </tr>
    <tr>
      <th>16</th>
      <td>[{'id': 47, 'name': 'Beam Instrumentation » BP...</td>
      <td>12</td>
      <td>Beam Instrumentation</td>
    </tr>
    <tr>
      <th>17</th>
      <td>[{'id': 31267, 'name': 'Electrical Network » B...</td>
      <td>83823</td>
      <td>Electrical Network</td>
    </tr>
    <tr>
      <th>18</th>
      <td>[{'id': 84, 'name': 'Operation » Operational e...</td>
      <td>19</td>
      <td>Operation</td>
    </tr>
    <tr>
      <th>19</th>
      <td>[{'id': 73, 'name': 'Experiments » ATLAS', 'ch...</td>
      <td>17</td>
      <td>Experiments</td>
    </tr>
    <tr>
      <th>20</th>
      <td>[]</td>
      <td>83801</td>
      <td>Ventilation Doors</td>
    </tr>
    <tr>
      <th>21</th>
      <td>[{'id': 101, 'name': 'Injector Complex » Beam ...</td>
      <td>24</td>
      <td>Injector Complex</td>
    </tr>
    <tr>
      <th>22</th>
      <td>[{'id': 21228, 'name': 'Magnet circuits » Eart...</td>
      <td>16738</td>
      <td>Magnet circuits</td>
    </tr>
    <tr>
      <th>23</th>
      <td>[{'id': 32132, 'name': 'IT Services » Network'...</td>
      <td>32124</td>
      <td>IT Services</td>
    </tr>
    <tr>
      <th>24</th>
      <td>[]</td>
      <td>29</td>
      <td>Other</td>
    </tr>
    <tr>
      <th>25</th>
      <td>[{'id': 31, 'name': 'Access System » Controls'...</td>
      <td>9</td>
      <td>Access System</td>
    </tr>
    <tr>
      <th>26</th>
      <td>[{'id': 95, 'name': 'Orbit » Reference', 'chil...</td>
      <td>22</td>
      <td>Orbit</td>
    </tr>
    <tr>
      <th>27</th>
      <td>[]</td>
      <td>28</td>
      <td>Power Converters</td>
    </tr>
    <tr>
      <th>28</th>
      <td>[{'id': 29452, 'name': 'Access Management » Ac...</td>
      <td>29449</td>
      <td>Access Management</td>
    </tr>
    <tr>
      <th>29</th>
      <td>[{'id': 103, 'name': 'Vacuum » Hardware', 'chi...</td>
      <td>25</td>
      <td>Vacuum</td>
    </tr>
  </tbody>
</table>
</div>



4. `/api/public/v1/faults`

  Search for faults fulfilling the given criteria.


```python
QueryBuilder().with_aft(session) \
    .with_duration(t_start='2016-01-13T00:00:00Z', t_end='2016-05-13T00:00:00Z') \
    .fault_query(acceleratorId='LHC', accessNeeded=True)
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>acceleratorName</th>
      <th>acceleratorPropertyInstances</th>
      <th>accessNeeded</th>
      <th>description</th>
      <th>displayLabel</th>
      <th>duration</th>
      <th>effectiveDuration</th>
      <th>endTime</th>
      <th>faultyElementNames</th>
      <th>id</th>
      <th>labelNames</th>
      <th>parentId</th>
      <th>parentSystemName</th>
      <th>reviewedByAwg</th>
      <th>reviewedByExpert</th>
      <th>startTime</th>
      <th>stateChanges</th>
      <th>systemName</th>
      <th>systemPropertyInstances</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Fill', 'value': '02...</td>
      <td>False</td>
      <td>access sectors 4,5,6,7 and LHCb indicate "blue...</td>
      <td>None</td>
      <td>4890000</td>
      <td>4890000</td>
      <td>2016-05-12T16:06:12Z</td>
      <td>[patrols lost]</td>
      <td>26594</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-12T14:44:42Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-1...</td>
      <td>Access System » Hardware</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>1</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Injection Scheme', 'value':...</td>
      <td>False</td>
      <td>electrical problem on switchboard</td>
      <td>None</td>
      <td>10123000</td>
      <td>10123000</td>
      <td>2016-05-12T15:10:42Z</td>
      <td>[RQ9.L2B1]</td>
      <td>26599</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-12T12:21:59Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-1...</td>
      <td>Electrical Network » Distribution » 400 kV</td>
      <td>[{'propertyName': 'TI Fault Type', 'value': 'G...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Beam Mode (ms)', 'v...</td>
      <td>False</td>
      <td>Ventilation doors in IP7, they do not close, n...</td>
      <td>None</td>
      <td>3011000</td>
      <td>3011000</td>
      <td>2016-05-06T13:23:42Z</td>
      <td>[Ventilation doors in IP7]</td>
      <td>26462</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-06T12:33:31Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-0...</td>
      <td>Ventilation Doors</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>3</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Accelerator Mode', 'value':...</td>
      <td>False</td>
      <td>RQT13.R7B1 water problem</td>
      <td>None</td>
      <td>18288000</td>
      <td>18288000</td>
      <td>2016-05-06T12:33:31Z</td>
      <td>[RQT13.R7B1, RPMBA.RR77.RQT13.R7B1]</td>
      <td>26459</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-06T07:28:43Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-0...</td>
      <td>Power Converters</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>4</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Fill No', 'value': '4885'},...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>12347000</td>
      <td>12347000</td>
      <td>2016-05-06T07:28:43Z</td>
      <td>[ROF.A45B1]</td>
      <td>26456</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-06T04:02:56Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-0...</td>
      <td>QPS » Controller</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>5</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Beam Mode', 'value': 'INJPR...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>9298000</td>
      <td>9298000</td>
      <td>2016-05-05T20:15:30Z</td>
      <td>[RPMBB.UA63.RSF1.A56B1, RSF1.A56B1]</td>
      <td>26450</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-05T17:40:32Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-0...</td>
      <td>Power Converters</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>6</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Beam Mode', 'value': 'INJPR...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>3817000</td>
      <td>3817000</td>
      <td>2016-05-05T15:39:44Z</td>
      <td>[RPMBB.UA63.RSF1.A56B1, RSF1.A56B1]</td>
      <td>26444</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-05T14:36:07Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-05-0...</td>
      <td>Power Converters</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>7</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Fill', 'value': '05...</td>
      <td>False</td>
      <td>cannot close door R74</td>
      <td>None</td>
      <td>6114000</td>
      <td>6114000</td>
      <td>2016-05-05T09:52:50Z</td>
      <td>[IP7 enclosure door]</td>
      <td>26441</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-05-05T08:10:56Z</td>
      <td>[{'stateId': 'NON_BLOCKING_OP', 'time': '2016-...</td>
      <td>Ventilation Doors</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>8</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Beam Mode', 'value'...</td>
      <td>False</td>
      <td>opened while in beam ON</td>
      <td>None</td>
      <td>16471000</td>
      <td>16471000</td>
      <td>2016-04-26T13:39:48Z</td>
      <td>[UL44]</td>
      <td>26260</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-04-26T09:05:17Z</td>
      <td>[{'stateId': 'NON_BLOCKING_OP', 'time': '2016-...</td>
      <td>Ventilation Doors</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>9</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Beam Mode', 'value'...</td>
      <td>False</td>
      <td>The door opened because the closing mechanism ...</td>
      <td>None</td>
      <td>5433000</td>
      <td>5433000</td>
      <td>2016-04-12T07:57:09Z</td>
      <td>[door YCPZ01=PM25]</td>
      <td>25760</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-04-12T06:26:36Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-04-1...</td>
      <td>Access Management » Patrol Lost</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>10</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Time in Beam Mode', 'value'...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>3091000</td>
      <td>3091000</td>
      <td>2016-04-08T09:07:23Z</td>
      <td>[TCL.5R1B1]</td>
      <td>25662</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-04-08T08:15:52Z</td>
      <td>[{'stateId': 'NON_BLOCKING_OP', 'time': '2016-...</td>
      <td>Collimation » Controls</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>11</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Operational Mode', 'value':...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>80000</td>
      <td>80000</td>
      <td>2016-04-07T16:04:39Z</td>
      <td>[RU.L4]</td>
      <td>25940</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-04-07T16:03:19Z</td>
      <td>[{'stateId': 'NON_BLOCKING_OP', 'time': '2016-...</td>
      <td>QPS » Controller</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>12</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Accelerator Mode', 'value':...</td>
      <td>False</td>
      <td>RCBYV4.R1B1: Vin DC over voltage\nRQ7.R1 (AC f...</td>
      <td>None</td>
      <td>14998000</td>
      <td>14998000</td>
      <td>2016-04-01T05:16:05Z</td>
      <td>[RPLB.RR17.RCBYV4.R1B1, RPHGA.RR17.RQ7.R1B2, R...</td>
      <td>25500</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-04-01T01:06:07Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-04-0...</td>
      <td>Power Converters</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>13</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Accelerator Mode', 'value':...</td>
      <td>False</td>
      <td>RB.A34 warm cable cooling (interlock on water ...</td>
      <td>None</td>
      <td>7654000</td>
      <td>7654000</td>
      <td>2016-03-30T22:44:40Z</td>
      <td>[RB.A34]</td>
      <td>25460</td>
      <td>[TIOC]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-03-30T20:37:06Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-03-3...</td>
      <td>Cooling and Ventilation » Cooling » Deminerali...</td>
      <td>[{'propertyName': 'TI Major Event Id', 'value'...</td>
    </tr>
    <tr>
      <th>14</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Injection Scheme', 'value':...</td>
      <td>False</td>
      <td>None</td>
      <td>None</td>
      <td>17483000</td>
      <td>17483000</td>
      <td>2016-03-28T08:00:42Z</td>
      <td>[RPHFC.UA83.RQX.L8, RQX.L8]</td>
      <td>25478</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-03-28T03:09:19Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-03-2...</td>
      <td>Power Converters</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>15</th>
      <td>LHC</td>
      <td>[{'propertyName': 'Injection Scheme', 'value':...</td>
      <td>False</td>
      <td>broken cable on thermo switch</td>
      <td>None</td>
      <td>10820000</td>
      <td>10820000</td>
      <td>2016-03-25T19:00:15Z</td>
      <td>[RD34.LR3]</td>
      <td>25380</td>
      <td>[]</td>
      <td>None</td>
      <td>None</td>
      <td>True</td>
      <td>False</td>
      <td>2016-03-25T15:59:55Z</td>
      <td>[{'stateId': 'BLOCKING_OP', 'time': '2016-03-2...</td>
      <td>Magnet circuits » Other</td>
      <td>[]</td>
    </tr>
  </tbody>
</table>
</div>



5. `/api/public/v1/faults/{faultId}`

  Retrieve one particular fault.


```python
QueryBuilder().with_aft(session) \
    .context_query('faults/{faultId}', context_id=26594)
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





    {'id': 26594,
     'acceleratorName': 'LHC',
     'systemName': 'Access System » Hardware',
     'startTime': '2016-05-12T14:44:42Z',
     'endTime': '2016-05-12T16:06:12Z',
     'duration': 4890000,
     'effectiveDuration': 4890000,
     'description': 'access sectors 4,5,6,7 and LHCb indicate "blue" door statuses and have lost patrol',
     'displayLabel': None,
     'stateChanges': [{'stateId': 'BLOCKING_OP', 'time': '2016-05-12T14:44:42Z'},
      {'stateId': 'OP_ENDED', 'time': '2016-05-12T16:06:12Z'}],
     'accessNeeded': True,
     'reviewedByAwg': True,
     'reviewedByExpert': True,
     'labelNames': ['TIOC'],
     'parentId': None,
     'parentSystemName': None,
     'systemPropertyInstances': [],
     'acceleratorPropertyInstances': [{'propertyName': 'Time in Fill',
       'value': '02h 13min 44s'},
      {'propertyName': 'Injection Scheme',
       'value': '2nominals_10pilots_RomanPot_Alignment'},
      {'propertyName': 'Accelerator Mode', 'value': 'Proton Physics'},
      {'propertyName': 'R2E Status', 'value': 'NOT_R2E_RELATED'},
      {'propertyName': 'Fill No', 'value': '4917'},
      {'propertyName': 'Time in Beam Mode (ms)', 'value': '2392997'},
      {'propertyName': 'Operational Mode', 'value': 'No Beams'},
      {'propertyName': 'Precycle Needed', 'value': 'No'},
      {'propertyName': 'Prevents Injection', 'value': 'Yes'},
      {'propertyName': 'Time in Beam Mode', 'value': '39min 52s'},
      {'propertyName': 'Beam Mode', 'value': 'NOBEAM'},
      {'propertyName': 'RP Needed', 'value': 'No'},
      {'propertyName': 'Time in Fill (ms)', 'value': '8024318'}],
     'faultyElementNames': ['patrols lost']}



6. `/api/public/v1/faults/labels`

  Retrieve all labels attached to any fault.


```python
QueryBuilder().with_aft(session) \
    .context_query('faults/labels')
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>152937</td>
      <td>60A BPM Interaction</td>
    </tr>
    <tr>
      <th>1</th>
      <td>302613</td>
      <td>T4</td>
    </tr>
    <tr>
      <th>2</th>
      <td>83888</td>
      <td>TIOC</td>
    </tr>
    <tr>
      <th>3</th>
      <td>284995</td>
      <td>Access PS/PSB</td>
    </tr>
    <tr>
      <th>4</th>
      <td>372881</td>
      <td>LBE</td>
    </tr>
    <tr>
      <th>5</th>
      <td>262457</td>
      <td>PSU</td>
    </tr>
    <tr>
      <th>6</th>
      <td>370386</td>
      <td>klystron instability + modulator fault</td>
    </tr>
    <tr>
      <th>7</th>
      <td>255578</td>
      <td>AWG Notable Fault</td>
    </tr>
    <tr>
      <th>8</th>
      <td>372879</td>
      <td>Power supply</td>
    </tr>
    <tr>
      <th>9</th>
      <td>189853</td>
      <td>BLM Sanity Checks</td>
    </tr>
    <tr>
      <th>10</th>
      <td>302604</td>
      <td>LEIR</td>
    </tr>
    <tr>
      <th>11</th>
      <td>189865</td>
      <td>False Dump</td>
    </tr>
    <tr>
      <th>12</th>
      <td>114933</td>
      <td>TE-EPC</td>
    </tr>
    <tr>
      <th>13</th>
      <td>372379</td>
      <td>Major Event</td>
    </tr>
    <tr>
      <th>14</th>
      <td>260311</td>
      <td>SR7.C</td>
    </tr>
    <tr>
      <th>15</th>
      <td>248740</td>
      <td>PIC PLC</td>
    </tr>
    <tr>
      <th>16</th>
      <td>256924</td>
      <td>Ions</td>
    </tr>
    <tr>
      <th>17</th>
      <td>259150</td>
      <td>BLETC</td>
    </tr>
    <tr>
      <th>18</th>
      <td>281492</td>
      <td>pole-phase winding</td>
    </tr>
    <tr>
      <th>19</th>
      <td>302618</td>
      <td>frev</td>
    </tr>
    <tr>
      <th>20</th>
      <td>152921</td>
      <td>MPE-EPC Interface</td>
    </tr>
    <tr>
      <th>21</th>
      <td>260308</td>
      <td>SEM</td>
    </tr>
    <tr>
      <th>22</th>
      <td>249289</td>
      <td>human error</td>
    </tr>
    <tr>
      <th>23</th>
      <td>358893</td>
      <td>BLECF</td>
    </tr>
    <tr>
      <th>24</th>
      <td>87033</td>
      <td>Septum down</td>
    </tr>
    <tr>
      <th>25</th>
      <td>259163</td>
      <td>SR5.L.CD11.B</td>
    </tr>
    <tr>
      <th>26</th>
      <td>302607</td>
      <td>CPS RF</td>
    </tr>
    <tr>
      <th>27</th>
      <td>302570</td>
      <td>Flowmeter problem</td>
    </tr>
    <tr>
      <th>28</th>
      <td>302592</td>
      <td>LINAC3</td>
    </tr>
    <tr>
      <th>29</th>
      <td>302599</td>
      <td>source</td>
    </tr>
    <tr>
      <th>30</th>
      <td>372381</td>
      <td>Major Machine Protection Event</td>
    </tr>
    <tr>
      <th>31</th>
      <td>300628</td>
      <td>REX</td>
    </tr>
    <tr>
      <th>32</th>
      <td>289715</td>
      <td>RFQ RF protection</td>
    </tr>
    <tr>
      <th>33</th>
      <td>372877</td>
      <td>Ion pump</td>
    </tr>
    <tr>
      <th>34</th>
      <td>191632</td>
      <td>MENA-20 (BE-CO)</td>
    </tr>
    <tr>
      <th>35</th>
      <td>376535</td>
      <td>TAILCLIPPER TIMING</td>
    </tr>
  </tbody>
</table>
</div>



7. `/api/public/v1/faults/states`

  Retrieve all possible states a fault can be in.


```python
QueryBuilder().with_aft(session) \
    .context_query('faults/states')
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>NON_BLOCKING_OP</td>
      <td>Non-Blocking OP</td>
    </tr>
    <tr>
      <th>1</th>
      <td>CANCELLED</td>
      <td>Cancelled</td>
    </tr>
    <tr>
      <th>2</th>
      <td>UNDERSTOOD</td>
      <td>Understood</td>
    </tr>
    <tr>
      <th>3</th>
      <td>SUSPENDED</td>
      <td>Suspended</td>
    </tr>
    <tr>
      <th>4</th>
      <td>SYSTEM_EXPERT_ENDED</td>
      <td>System Expert Ended</td>
    </tr>
    <tr>
      <th>5</th>
      <td>OP_ENDED</td>
      <td>OP Ended</td>
    </tr>
    <tr>
      <th>6</th>
      <td>BLOCKING_OP</td>
      <td>Blocking OP</td>
    </tr>
  </tbody>
</table>
</div>



8. `/api/public/v1/faulty-element-types`

  Retrieve all faulty element types in use in AFT.


```python
QueryBuilder().with_aft(session) \
    .context_query('faulty-element-types')
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>PXACC40</td>
    </tr>
    <tr>
      <th>1</th>
      <td>PXRKDIO</td>
    </tr>
    <tr>
      <th>2</th>
      <td>HCTCSG_</td>
    </tr>
    <tr>
      <th>3</th>
      <td>L4LAINTLKSRC v.3.1.1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>PXBTVBM004</td>
    </tr>
    <tr>
      <th>5</th>
      <td>HCRPZES</td>
    </tr>
    <tr>
      <th>6</th>
      <td>HCRPZEQ</td>
    </tr>
    <tr>
      <th>7</th>
      <td>HCTCDIV</td>
    </tr>
    <tr>
      <th>8</th>
      <td>HCRPZEO</td>
    </tr>
    <tr>
      <th>9</th>
      <td>PXLMK__003</td>
    </tr>
    <tr>
      <th>10</th>
      <td>RCBV33</td>
    </tr>
    <tr>
      <th>11</th>
      <td>PXRK___</td>
    </tr>
    <tr>
      <th>12</th>
      <td>RNFH</td>
    </tr>
    <tr>
      <th>13</th>
      <td>HCCFI__</td>
    </tr>
    <tr>
      <th>14</th>
      <td>HCRPZEG</td>
    </tr>
    <tr>
      <th>15</th>
      <td>HCRPZEH</td>
    </tr>
    <tr>
      <th>16</th>
      <td>RCBV31</td>
    </tr>
    <tr>
      <th>17</th>
      <td>HCRPZEF</td>
    </tr>
    <tr>
      <th>18</th>
      <td>HCBVCRA001</td>
    </tr>
    <tr>
      <th>19</th>
      <td>HCRPZEB</td>
    </tr>
    <tr>
      <th>20</th>
      <td>PXACH__</td>
    </tr>
    <tr>
      <th>21</th>
      <td>RAC</td>
    </tr>
    <tr>
      <th>22</th>
      <td>PXDHZ__8AF</td>
    </tr>
    <tr>
      <th>23</th>
      <td>PXKHZ__</td>
    </tr>
    <tr>
      <th>24</th>
      <td>HCETH__</td>
    </tr>
    <tr>
      <th>25</th>
      <td>MKController_Virtual v.0.1.0</td>
    </tr>
    <tr>
      <th>26</th>
      <td>LEBT v.2.0.4</td>
    </tr>
    <tr>
      <th>27</th>
      <td>RCBV27</td>
    </tr>
    <tr>
      <th>28</th>
      <td>PXMCVEBHWC</td>
    </tr>
    <tr>
      <th>29</th>
      <td>RAR</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th>407</th>
      <td>HCTCL__</td>
    </tr>
    <tr>
      <th>408</th>
      <td>PXDVT__00T</td>
    </tr>
    <tr>
      <th>409</th>
      <td>HCLY___129</td>
    </tr>
    <tr>
      <th>410</th>
      <td>HCLY___005</td>
    </tr>
    <tr>
      <th>411</th>
      <td>LTIM v.4.1.13</td>
    </tr>
    <tr>
      <th>412</th>
      <td>HCRS___033</td>
    </tr>
    <tr>
      <th>413</th>
      <td>PXSTP__</td>
    </tr>
    <tr>
      <th>414</th>
      <td>RQT12</td>
    </tr>
    <tr>
      <th>415</th>
      <td>HCEAV__</td>
    </tr>
    <tr>
      <th>416</th>
      <td>PXCK___</td>
    </tr>
    <tr>
      <th>417</th>
      <td>RQT13</td>
    </tr>
    <tr>
      <th>418</th>
      <td>RBIV</td>
    </tr>
    <tr>
      <th>419</th>
      <td>RSMV</td>
    </tr>
    <tr>
      <th>420</th>
      <td>RDHZPS</td>
    </tr>
    <tr>
      <th>421</th>
      <td>PXMQNFA4WP</td>
    </tr>
    <tr>
      <th>422</th>
      <td>PVPUMP v.0</td>
    </tr>
    <tr>
      <th>423</th>
      <td>PXMCXBBWAP</td>
    </tr>
    <tr>
      <th>424</th>
      <td>LHC HALF-CELL</td>
    </tr>
    <tr>
      <th>425</th>
      <td>HCCFP__</td>
    </tr>
    <tr>
      <th>426</th>
      <td>GENERAL UNDG CIVIL WORK</td>
    </tr>
    <tr>
      <th>427</th>
      <td>PreChopperL4 v.4.5.7</td>
    </tr>
    <tr>
      <th>428</th>
      <td>PXMONDAFWP</td>
    </tr>
    <tr>
      <th>429</th>
      <td>RQIF</td>
    </tr>
    <tr>
      <th>430</th>
      <td>RSMH</td>
    </tr>
    <tr>
      <th>431</th>
      <td>PXMBHGC4WP</td>
    </tr>
    <tr>
      <th>432</th>
      <td>RQID</td>
    </tr>
    <tr>
      <th>433</th>
      <td>HCRPHGC</td>
    </tr>
    <tr>
      <th>434</th>
      <td>HCDQQDC</td>
    </tr>
    <tr>
      <th>435</th>
      <td>HCRPHGA</td>
    </tr>
    <tr>
      <th>436</th>
      <td>HCRPHGB</td>
    </tr>
  </tbody>
</table>
<p>437 rows × 1 columns</p>
</div>



9. `/api/public/v1/faulty-elements`

  Retrieve all faulty elements in use in AFT. The results are paginated.


```python
QueryBuilder().with_aft(session) \
    .context_query('faulty-elements')
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>name</th>
      <th>source</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>165220</td>
      <td>12 bunches train setting-up</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>1</th>
      <td>160394</td>
      <td>***</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>2</th>
      <td>160177</td>
      <td>12L6</td>
      <td>LAYOUT</td>
    </tr>
    <tr>
      <th>3</th>
      <td>173454</td>
      <td>!</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>4</th>
      <td>174929</td>
      <td>118</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>5</th>
      <td>163767</td>
      <td>Thermoswitch</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>6</th>
      <td>159257</td>
      <td>12 b SPS-Ring 1 off by 1 bucket</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>7</th>
      <td>165507</td>
      <td>ER.KFH32</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>8</th>
      <td>165197</td>
      <td>12b train lost at start SPS ramp</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>9</th>
      <td>163273</td>
      <td>11L1</td>
      <td>LAYOUT</td>
    </tr>
    <tr>
      <th>10</th>
      <td>164445</td>
      <td>10Mhz C76</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>11</th>
      <td>159703</td>
      <td>1 injection missing</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>12</th>
      <td>161267</td>
      <td>10MHz Cavity</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>13</th>
      <td>161644</td>
      <td>CCC/CCR router</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>14</th>
      <td>162274</td>
      <td>12 bunches</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>15</th>
      <td>165546</td>
      <td>BI4.DIS</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>16</th>
      <td>161016</td>
      <td>12 circuits in Sector 67</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>17</th>
      <td>163671</td>
      <td>10MHz C66</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>18</th>
      <td>160100</td>
      <td>10 MHz cavity replacement</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>19</th>
      <td>163584</td>
      <td>10L3</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>20</th>
      <td>282777</td>
      <td>10M</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>21</th>
      <td>162701</td>
      <td>12 bunches on TDI B1</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>22</th>
      <td>164662</td>
      <td>12b train</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>23</th>
      <td>193253</td>
      <td>10MHz.Cavity</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>24</th>
      <td>161067</td>
      <td>144 bunches unstable</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>25</th>
      <td>160213</td>
      <td>ROD.A34B2</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>26</th>
      <td>160310</td>
      <td>10 Mhz cavity</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>27</th>
      <td>159551</td>
      <td>dump intensity</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>28</th>
      <td>159882</td>
      <td>XLL2.CAV4</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>29</th>
      <td>160036</td>
      <td>10 MHz cavities</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>30</th>
      <td>163118</td>
      <td>10MHz casvity</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>31</th>
      <td>165387</td>
      <td>playback</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>32</th>
      <td>163731</td>
      <td>BTY.QDE182</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>33</th>
      <td>187170</td>
      <td>12btrain_intensity_too_low</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>34</th>
      <td>165364</td>
      <td>10MHz cavities</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>35</th>
      <td>164040</td>
      <td>Smoke detected Firebrigade intervention</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>36</th>
      <td>162079</td>
      <td>10MHz 56 66 76 81 86 91 96</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>37</th>
      <td>160678</td>
      <td>XLL2.CAV2</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>38</th>
      <td>165302</td>
      <td>11/51/66/76/86</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>39</th>
      <td>162864</td>
      <td>BTY.QFO210</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>40</th>
      <td>161130</td>
      <td>BETS SPS</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>41</th>
      <td>162024</td>
      <td>PFW</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>42</th>
      <td>163520</td>
      <td>11L5</td>
      <td>LAYOUT</td>
    </tr>
    <tr>
      <th>43</th>
      <td>165641</td>
      <td></td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>44</th>
      <td>160435</td>
      <td>10MHz cavities fault</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>45</th>
      <td>162967</td>
      <td>bunches measurement</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>46</th>
      <td>164509</td>
      <td>10MHz</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>47</th>
      <td>249312</td>
      <td>06X.BA6.SPARE2</td>
      <td>LAYOUT</td>
    </tr>
    <tr>
      <th>48</th>
      <td>178185</td>
      <td>-</td>
      <td>ELOGBOOK</td>
    </tr>
    <tr>
      <th>49</th>
      <td>164412</td>
      <td>10MHz cavity</td>
      <td>ELOGBOOK</td>
    </tr>
  </tbody>
</table>
</div>



10. `/api/public/v1/faulty-elements/{faultyElementId}/statistics`

  Retrieve faulty element statistics from AFT


```python
QueryBuilder().with_aft(session) \
    .context_query('faulty-elements/{faultyElementId}/statistics', context_id=163188)
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>faultCount</th>
      <th>faultDuration</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>163188</th>
      <td>163188</td>
      <td>1</td>
      <td>2065000</td>
    </tr>
  </tbody>
</table>
</div>



11. `/api/public/v1/systems/{systemId}/properties`

  Retrieve the properties which are specific to the given system.


```python
QueryBuilder().with_aft(session) \
    .context_query('systems/{systemId}/properties', context_id=83823)
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/urllib3/connectionpool.py:857: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
      InsecureRequestWarning)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>name</th>
      <th>pattern</th>
      <th>values</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>166765</td>
      <td>TI Fault Type</td>
      <td>None</td>
      <td>[Cable, Fuse, PLC Hardware, Wrong Action, Rela...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>358702</td>
      <td>Cause</td>
      <td>None</td>
      <td>[Wind, Pollution, Snow, Unknown, Wildlife, Sho...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>302969</td>
      <td>Voltage Dip % (RTE)</td>
      <td>[-+]?[0-9]*\.?[0-9]*</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>3</th>
      <td>302967</td>
      <td>Duration ms (RTE)</td>
      <td>[-+]?[0-9]*\.?[0-9]*</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>4</th>
      <td>166766</td>
      <td>TI Major Event Id</td>
      <td>[1-9][0-9]*</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>5</th>
      <td>358703</td>
      <td>Origin Location</td>
      <td>None</td>
      <td>[Grande Ile - Le Cheylas, Albertville - Coche ...</td>
    </tr>
    <tr>
      <th>6</th>
      <td>302968</td>
      <td>Duration ms (CERN)</td>
      <td>[-+]?[0-9]*\.?[0-9]*</td>
      <td>[]</td>
    </tr>
    <tr>
      <th>7</th>
      <td>302970</td>
      <td>Voltage Dip % (CERN)</td>
      <td>[-+]?[0-9]*\.?[0-9]*</td>
      <td>[]</td>
    </tr>
  </tbody>
</table>
</div>



## 7.1.6. Processing Raw Signals
Once a signal is queried, one can perform some operations on each of them.  
In this case, the order of operations does not matter (but can be checked).  

|Signal query|Signal processing|
| ---------- | :-------------- |
|{DB}.{DURATION}.{QUERY_PARAMETERS}.{QUERY}| |
|{DB}.{DURATION}.{CIRCUIT_TYPE}.{METADATA}.{QUERY}| |
|&nbsp;|.synchronize_time()|
|&nbsp;|.convert_index_to_sec()|
|&nbsp;|.create_col_from_index()|
|&nbsp;|.filter_median()|
|&nbsp;|.remove_values_for_time_less_than()|
|&nbsp;|.remove_initial_offset()|


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

i_meas_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start='2015-01-13 16:59:11+01:00', t_end='2015-01-13 17:15:46+01:00') \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A12', system='PC', signal='I_MEAS') \
    .signal_query() \
    .synchronize_time() \
    .convert_index_to_sec().dfs[0]

i_meas_df.plot()
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7fdcaf441cc0>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_244_1.png)
    


### 3.3.4. Processing Raw Events
For PM event queries one can perform several operations on source, timestamp dataframe. 

|Event query|Event processing|
| ---------- | :-------------- |
|{DB}.{DURATION}.{QUERY_PARAMETERS}.{QUERY}| |
|{DB}.{DURATION}.{CIRCUIT_TYPE}.{METADATA}.{QUERY}| |
|&nbsp;|.filter_source()|
|&nbsp;|.drop_duplicate_source()|
|&nbsp;|.sort_values()|

The processing methods are dedicated to performing repeated operations on PM events. In case of searching a given system and className with wildcard '\*' as a source, the event query can return events from different sectors. In this case, one can filter events to contain to a given sector. Some PM systems return duplicate events from different types of boards. In this case one can drop duplicate sources. Eventually, the events can be sorted by either source or timestamp.


- Filter source


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

source_timestamp_df = QueryBuilder().with_pm() \
    .with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(24*60*60, 's')]) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A45', system='QH', source='*') \
    .event_query() \
    .df

source_timestamp_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>source</th>
      <th>timestamp</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>B20L5</td>
      <td>1426220469491000000</td>
    </tr>
    <tr>
      <th>1</th>
      <td>C20L5</td>
      <td>1426220517100000000</td>
    </tr>
    <tr>
      <th>2</th>
      <td>A20L5</td>
      <td>1426220518112000000</td>
    </tr>
    <tr>
      <th>3</th>
      <td>A21L5</td>
      <td>1426220625990000000</td>
    </tr>
    <tr>
      <th>4</th>
      <td>B21L5</td>
      <td>1426220866112000000</td>
    </tr>
    <tr>
      <th>5</th>
      <td>C23L4</td>
      <td>1426236802332000000</td>
    </tr>
    <tr>
      <th>6</th>
      <td>B23L4</td>
      <td>1426236839404000000</td>
    </tr>
    <tr>
      <th>7</th>
      <td>A23L4</td>
      <td>1426236839832000000</td>
    </tr>
    <tr>
      <th>8</th>
      <td>C22L4</td>
      <td>1426236949841000000</td>
    </tr>
    <tr>
      <th>9</th>
      <td>C15R4</td>
      <td>1426251285711000000</td>
    </tr>
    <tr>
      <th>10</th>
      <td>B15R4</td>
      <td>1426251337747000000</td>
    </tr>
    <tr>
      <th>11</th>
      <td>A15R4</td>
      <td>1426251388741000000</td>
    </tr>
    <tr>
      <th>12</th>
      <td>B34L8</td>
      <td>1426258716281000000</td>
    </tr>
    <tr>
      <th>13</th>
      <td>C34L8</td>
      <td>1426258747672000000</td>
    </tr>
    <tr>
      <th>14</th>
      <td>A34L8</td>
      <td>1426258747370000000</td>
    </tr>
    <tr>
      <th>15</th>
      <td>C33L8</td>
      <td>1426258835955000000</td>
    </tr>
    <tr>
      <th>16</th>
      <td>C34R7</td>
      <td>1426258853947000000</td>
    </tr>
    <tr>
      <th>17</th>
      <td>A34R7</td>
      <td>1426258854113000000</td>
    </tr>
    <tr>
      <th>18</th>
      <td>A20R3</td>
      <td>1426267931956000000</td>
    </tr>
    <tr>
      <th>19</th>
      <td>B20R3</td>
      <td>1426267983579000000</td>
    </tr>
    <tr>
      <th>20</th>
      <td>C20R3</td>
      <td>1426268004144000000</td>
    </tr>
    <tr>
      <th>21</th>
      <td>B18L5</td>
      <td>1426277626360000000</td>
    </tr>
    <tr>
      <th>22</th>
      <td>A18L5</td>
      <td>1426277679838000000</td>
    </tr>
    <tr>
      <th>23</th>
      <td>C18L5</td>
      <td>1426277680496000000</td>
    </tr>
    <tr>
      <th>24</th>
      <td>A19L5</td>
      <td>1426277903449000000</td>
    </tr>
  </tbody>
</table>
</div>



Executing filter_source() with circuit name and system type would filter out events not belonging to a given circuit name


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

source_timestamp_df = QueryBuilder().with_pm() \
    .with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(24*60*60, 's')]) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A45', system='QH', source='*') \
    .event_query() \
    .filter_source('RB.A45', 'QH') \
    .df

source_timestamp_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>source</th>
      <th>timestamp</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>B20L5</td>
      <td>1426220469491000000</td>
    </tr>
    <tr>
      <th>1</th>
      <td>C20L5</td>
      <td>1426220517100000000</td>
    </tr>
    <tr>
      <th>2</th>
      <td>A20L5</td>
      <td>1426220518112000000</td>
    </tr>
    <tr>
      <th>3</th>
      <td>A21L5</td>
      <td>1426220625990000000</td>
    </tr>
    <tr>
      <th>4</th>
      <td>B21L5</td>
      <td>1426220866112000000</td>
    </tr>
    <tr>
      <th>5</th>
      <td>C15R4</td>
      <td>1426251285711000000</td>
    </tr>
    <tr>
      <th>6</th>
      <td>B15R4</td>
      <td>1426251337747000000</td>
    </tr>
    <tr>
      <th>7</th>
      <td>A15R4</td>
      <td>1426251388741000000</td>
    </tr>
    <tr>
      <th>8</th>
      <td>B18L5</td>
      <td>1426277626360000000</td>
    </tr>
    <tr>
      <th>9</th>
      <td>A18L5</td>
      <td>1426277679838000000</td>
    </tr>
    <tr>
      <th>10</th>
      <td>C18L5</td>
      <td>1426277680496000000</td>
    </tr>
    <tr>
      <th>11</th>
      <td>A19L5</td>
      <td>1426277903449000000</td>
    </tr>
  </tbody>
</table>
</div>



- Drop duplicates


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

source_timestamp_df = QueryBuilder().with_pm() \
    .with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(24*60*60, 's')]) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A45', system='QDS', source='*') \
    .event_query() \
    .drop_duplicate_source() \
    .df

source_timestamp_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>source</th>
      <th>timestamp</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>B20L5</td>
      <td>1426220469490000000</td>
    </tr>
    <tr>
      <th>1</th>
      <td>C20L5</td>
      <td>1426220517099000000</td>
    </tr>
    <tr>
      <th>2</th>
      <td>A20L5</td>
      <td>1426220518111000000</td>
    </tr>
    <tr>
      <th>3</th>
      <td>A21L5</td>
      <td>1426220625989000000</td>
    </tr>
    <tr>
      <th>4</th>
      <td>B21L5</td>
      <td>1426220866111000000</td>
    </tr>
    <tr>
      <th>5</th>
      <td>C23L4</td>
      <td>1426236802331000000</td>
    </tr>
    <tr>
      <th>6</th>
      <td>B23L4</td>
      <td>1426236839403000000</td>
    </tr>
    <tr>
      <th>7</th>
      <td>A23L4</td>
      <td>1426236839831000000</td>
    </tr>
    <tr>
      <th>8</th>
      <td>C22L4</td>
      <td>1426236949840000000</td>
    </tr>
    <tr>
      <th>9</th>
      <td>C15R4</td>
      <td>1426251285710000000</td>
    </tr>
    <tr>
      <th>10</th>
      <td>B15R4</td>
      <td>1426251337746000000</td>
    </tr>
    <tr>
      <th>11</th>
      <td>A15R4</td>
      <td>1426251388740000000</td>
    </tr>
    <tr>
      <th>12</th>
      <td>B34L8</td>
      <td>1426258716280000000</td>
    </tr>
    <tr>
      <th>13</th>
      <td>A34L8</td>
      <td>1426258747369000000</td>
    </tr>
    <tr>
      <th>14</th>
      <td>C34L8</td>
      <td>1426258747671000000</td>
    </tr>
    <tr>
      <th>15</th>
      <td>C33L8</td>
      <td>1426258835954000000</td>
    </tr>
    <tr>
      <th>16</th>
      <td>C34R7</td>
      <td>1426258853946000000</td>
    </tr>
    <tr>
      <th>17</th>
      <td>A34R7</td>
      <td>1426258854112000000</td>
    </tr>
    <tr>
      <th>18</th>
      <td>A20R3</td>
      <td>1426267931955000000</td>
    </tr>
    <tr>
      <th>19</th>
      <td>B20R3</td>
      <td>1426267983578000000</td>
    </tr>
    <tr>
      <th>20</th>
      <td>C20R3</td>
      <td>1426268004143000000</td>
    </tr>
    <tr>
      <th>21</th>
      <td>B18L5</td>
      <td>1426277626359000000</td>
    </tr>
    <tr>
      <th>22</th>
      <td>A18L5</td>
      <td>1426277679837000000</td>
    </tr>
    <tr>
      <th>23</th>
      <td>C18L5</td>
      <td>1426277680495000000</td>
    </tr>
    <tr>
      <th>24</th>
      <td>A19L5</td>
      <td>1426277903448000000</td>
    </tr>
  </tbody>
</table>
</div>



Executing filter_source() with circuit name and system type would filter out events not belonging to a given circuit name


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

source_timestamp_df = QueryBuilder().with_pm() \
    .with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(24*60*60, 's')]) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A45', system='QDS', source='*') \
    .event_query() \
    .filter_source('RB.A45', 'QDS') \
    .df

source_timestamp_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>source</th>
      <th>timestamp</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>B20L5</td>
      <td>1426220469490000000</td>
    </tr>
    <tr>
      <th>1</th>
      <td>C20L5</td>
      <td>1426220517099000000</td>
    </tr>
    <tr>
      <th>2</th>
      <td>A20L5</td>
      <td>1426220518111000000</td>
    </tr>
    <tr>
      <th>3</th>
      <td>A21L5</td>
      <td>1426220625989000000</td>
    </tr>
    <tr>
      <th>4</th>
      <td>B21L5</td>
      <td>1426220866111000000</td>
    </tr>
    <tr>
      <th>5</th>
      <td>B20L5</td>
      <td>1426220469492000000</td>
    </tr>
    <tr>
      <th>6</th>
      <td>C20L5</td>
      <td>1426220517101000000</td>
    </tr>
    <tr>
      <th>7</th>
      <td>A20L5</td>
      <td>1426220518113000000</td>
    </tr>
    <tr>
      <th>8</th>
      <td>A21L5</td>
      <td>1426220625991000000</td>
    </tr>
    <tr>
      <th>9</th>
      <td>B21L5</td>
      <td>1426220866113000000</td>
    </tr>
    <tr>
      <th>10</th>
      <td>C15R4</td>
      <td>1426251285710000000</td>
    </tr>
    <tr>
      <th>11</th>
      <td>B15R4</td>
      <td>1426251337746000000</td>
    </tr>
    <tr>
      <th>12</th>
      <td>A15R4</td>
      <td>1426251388740000000</td>
    </tr>
    <tr>
      <th>13</th>
      <td>C15R4</td>
      <td>1426251285712000000</td>
    </tr>
    <tr>
      <th>14</th>
      <td>B15R4</td>
      <td>1426251337748000000</td>
    </tr>
    <tr>
      <th>15</th>
      <td>A15R4</td>
      <td>1426251388742000000</td>
    </tr>
    <tr>
      <th>16</th>
      <td>B18L5</td>
      <td>1426277626359000000</td>
    </tr>
    <tr>
      <th>17</th>
      <td>A18L5</td>
      <td>1426277679837000000</td>
    </tr>
    <tr>
      <th>18</th>
      <td>C18L5</td>
      <td>1426277680495000000</td>
    </tr>
    <tr>
      <th>19</th>
      <td>A19L5</td>
      <td>1426277903448000000</td>
    </tr>
    <tr>
      <th>20</th>
      <td>B18L5</td>
      <td>1426277626361000000</td>
    </tr>
    <tr>
      <th>21</th>
      <td>A18L5</td>
      <td>1426277679839000000</td>
    </tr>
    <tr>
      <th>22</th>
      <td>C18L5</td>
      <td>1426277680497000000</td>
    </tr>
    <tr>
      <th>23</th>
      <td>A19L5</td>
      <td>1426277903450000000</td>
    </tr>
  </tbody>
</table>
</div>



- drop_duplicate_source()

Some PM systems return duplicate events from different types of boards. In this case one can drop duplicate sources. 


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

source_timestamp_df = QueryBuilder().with_pm() \
    .with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(24*60*60, 's')]) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A45', system='QDS', source='*') \
    .event_query() \
    .filter_source('RB.A45', 'QDS') \
    .drop_duplicate_source() \
    .df

source_timestamp_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>source</th>
      <th>timestamp</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>B20L5</td>
      <td>1426220469490000000</td>
    </tr>
    <tr>
      <th>1</th>
      <td>C20L5</td>
      <td>1426220517099000000</td>
    </tr>
    <tr>
      <th>2</th>
      <td>A20L5</td>
      <td>1426220518111000000</td>
    </tr>
    <tr>
      <th>3</th>
      <td>A21L5</td>
      <td>1426220625989000000</td>
    </tr>
    <tr>
      <th>4</th>
      <td>B21L5</td>
      <td>1426220866111000000</td>
    </tr>
    <tr>
      <th>5</th>
      <td>C15R4</td>
      <td>1426251285710000000</td>
    </tr>
    <tr>
      <th>6</th>
      <td>B15R4</td>
      <td>1426251337746000000</td>
    </tr>
    <tr>
      <th>7</th>
      <td>A15R4</td>
      <td>1426251388740000000</td>
    </tr>
    <tr>
      <th>8</th>
      <td>B18L5</td>
      <td>1426277626359000000</td>
    </tr>
    <tr>
      <th>9</th>
      <td>A18L5</td>
      <td>1426277679837000000</td>
    </tr>
    <tr>
      <th>10</th>
      <td>C18L5</td>
      <td>1426277680495000000</td>
    </tr>
    <tr>
      <th>11</th>
      <td>A19L5</td>
      <td>1426277903448000000</td>
    </tr>
  </tbody>
</table>
</div>



- sort_values()

The events can be sorted by either source or timestamp.


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder

source_timestamp_df = QueryBuilder().with_pm() \
    .with_duration(t_start='2015-03-13 05:20:59.4910002', duration=[(24*60*60, 's')]) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A45', system='QDS', source='*') \
    .event_query() \
    .filter_source('RB.A45', 'QDS') \
    .drop_duplicate_source() \
    .sort_values(by='timestamp') \
    .df

source_timestamp_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>source</th>
      <th>timestamp</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>B20L5</td>
      <td>1426220469490000000</td>
    </tr>
    <tr>
      <th>1</th>
      <td>C20L5</td>
      <td>1426220517099000000</td>
    </tr>
    <tr>
      <th>2</th>
      <td>A20L5</td>
      <td>1426220518111000000</td>
    </tr>
    <tr>
      <th>3</th>
      <td>A21L5</td>
      <td>1426220625989000000</td>
    </tr>
    <tr>
      <th>4</th>
      <td>B21L5</td>
      <td>1426220866111000000</td>
    </tr>
    <tr>
      <th>5</th>
      <td>C15R4</td>
      <td>1426251285710000000</td>
    </tr>
    <tr>
      <th>6</th>
      <td>B15R4</td>
      <td>1426251337746000000</td>
    </tr>
    <tr>
      <th>7</th>
      <td>A15R4</td>
      <td>1426251388740000000</td>
    </tr>
    <tr>
      <th>8</th>
      <td>B18L5</td>
      <td>1426277626359000000</td>
    </tr>
    <tr>
      <th>9</th>
      <td>A18L5</td>
      <td>1426277679837000000</td>
    </tr>
    <tr>
      <th>10</th>
      <td>C18L5</td>
      <td>1426277680495000000</td>
    </tr>
    <tr>
      <th>11</th>
      <td>A19L5</td>
      <td>1426277903448000000</td>
    </tr>
  </tbody>
</table>
</div>



## 3.4. AssertionBuilder()

<center>{SIGNALS}.(TIME_RANGE).{ASSERTION}</center>

|Signal input|Time range definition (optional) / Signal assertion|Signal assertions (if time range defined)|
| ---------- | :------------------ |-----------------|
|.with_signal()|&nbsp;|&nbsp;|
|&nbsp;|.has_min_max_value()|&nbsp;|
|&nbsp;|.compare_to_reference()|&nbsp;|
|&nbsp;|.with_time_range()|.has_min_max_variation()|
|&nbsp;|.with_time_range()|.has_min_max_slope()|

- has_min_max_value()

<img src="https://gitlab.cern.ch/LHCData/lhc-sm-api/raw/master/figures/MP3.png" width=75%>


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder
from lhcsmapi.pyedsl.AssertionBuilder import AssertionBuilder

tt891a_dfs = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start='2014-12-13 09:12:41+01:00', t_end='2014-12-13 12:27:11+01:00') \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A12', system=['LEADS_EVEN_WINCCOA', 'LEADS_ODD_WINCCOA'], signal='TT891A') \
    .signal_query() \
    .synchronize_time() \
    .convert_index_to_sec() \
    .filter_median().dfs


AssertionBuilder().with_signal(tt891a_dfs) \
    .has_min_max_value(value_min=46, value_max=54)
```




    <lhcsmapi.pyedsl.AssertionBuilder.AssertionBuilderSignalPlot at 0x7fdca93da668>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_260_1.png)
    


In the case below, the assertion should fail and raise a warning.


```python
AssertionBuilder().with_signal(tt891a_dfs) \
    .has_min_max_value(value_min=50, value_max=54)
```

    DACA05_07L2_TT891A.TEMPERATURECALC outside of the [50, 54] threshold
    DACA06_07L2_TT891A.TEMPERATURECALC outside of the [50, 54] threshold
    DABA01_07R1_TT891A.TEMPERATURECALC outside of the [50, 54] threshold
    DABA02_07R1_TT891A.TEMPERATURECALC outside of the [50, 54] threshold





    <lhcsmapi.pyedsl.AssertionBuilder.AssertionBuilderSignalPlot at 0x7fdca9347a58>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_262_2.png)
    


- compare_to_reference()


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder
from lhcsmapi.pyedsl.AssertionBuilder import AssertionBuilder
from lhcsmapi.reference.Reference import Reference
from lhcsmapi.Time import Time

timestamp_ee_rqd = 1544622149701000000
timestamp_fgc_rqd = 1544622149620000000
signal_names = 'T_RES'


t_res_df = QueryBuilder().with_pm() \
    .with_timestamp(timestamp_ee_rqd) \
    .with_circuit_type('RQ') \
    .with_metadata(circuit_name='RQD.A12', system='EE', signal=signal_names).signal_query() \
    .remove_values_for_time_less_than(timestamp_ee_rqd) \
    .synchronize_time(timestamp_fgc_rqd) \
    .convert_index_to_sec().dfs[0]


timestamp_ee_ref_rqd = Reference.get_power_converter_reference_fpa('RQ', 'RQD.A12', 'eePm')
timestamp_ee_ref_rqd = Time.to_unix_timestamp(timestamp_ee_ref_rqd)

timestamp_fgc_ref_rqd = Reference.get_power_converter_reference_fpa('RQ', 'RQD.A12', 'fgcPm')
timestamp_fgc_ref_rqd = Time.to_unix_timestamp(timestamp_fgc_ref_rqd)


t_res_ref_df = QueryBuilder().with_pm() \
    .with_timestamp(timestamp_ee_ref_rqd) \
    .with_circuit_type('RQ') \
    .with_metadata(circuit_name='RQD.A12', system='EE', signal=signal_names).signal_query() \
    .remove_values_for_time_less_than(timestamp_ee_ref_rqd) \
    .synchronize_time(timestamp_fgc_ref_rqd) \
    .convert_index_to_sec().dfs[0]

AssertionBuilder().with_signal([t_res_df])\
    .compare_to_reference(signal_ref_dfs=[t_res_ref_df], abs_margin=25, scaling=1)
```




    <lhcsmapi.pyedsl.AssertionBuilder.AssertionBuilderSignalPlot at 0x7fdca6801080>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_264_1.png)
    


- has_min_max_variation()


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder
from lhcsmapi.pyedsl.AssertionBuilder import AssertionBuilder
from lhcsmapi.analysis.busbar.BusbarResistanceAnalysis import BusbarResistanceAnalysis

t_start = '2014-12-13 09:12:41+01:00'
t_end = '2014-12-13 12:27:11+01:00'

cv891_dfs = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start=t_start, t_end=t_end) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A12', system=['LEADS_EVEN_WINCCOA', 'LEADS_ODD_WINCCOA'], signal='CV891') \
    .signal_query() \
    .synchronize_time() \
    .convert_index_to_sec() \
    .filter_median() \
    .dfs

i_meas_raw_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start=t_start, t_end=t_end) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A12', system='PC', signal='I_MEAS') \
    .signal_query() \
    .dfs[0]

plateau_start, plateau_end = BusbarResistanceAnalysis.calculate_current_plateau_start_end([i_meas_raw_df], i_meas_threshold=500)

AssertionBuilder().with_signal(cv891_dfs) \
    .with_time_range(t_start=(plateau_start-i_meas_raw_df.index[0])/1e9, t_end=(plateau_end-i_meas_raw_df.index[0])/1e9) \
    .has_min_max_variation(variation_min_max=8)
```




    <lhcsmapi.pyedsl.AssertionBuilder.AssertionBuilderSignalPlot at 0x7fdca51075f8>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_266_1.png)
    


In the case below, the variation is too tight and the assertion fails 


```python
AssertionBuilder().with_signal(cv891_dfs) \
    .with_time_range(t_start=(plateau_start-i_meas_raw_df.index[0])/1e9, t_end=(plateau_end-i_meas_raw_df.index[0])/1e9) \
    .has_min_max_variation(variation_min_max=1)
```

    The variation of DACA05_07L2_CV891.POSST (1.0 %) exceeds 1.8000000000000007 % for constant current from 747.096686592 to 5882.761325312 s
    The variation of DACA05_07L2_CV891.POSST (1.0 %) exceeds 1.3000000000000007 % for constant current from 6140.794719232 to 9740.260548864 s
    The variation of DACA06_07L2_CV891.POSST (1.0 %) exceeds 2.1000000000000014 % for constant current from 747.096686592 to 5882.761325312 s
    The variation of DACA06_07L2_CV891.POSST (1.0 %) exceeds 1.1999999999999993 % for constant current from 6140.794719232 to 9740.260548864 s
    The variation of DABA01_07R1_CV891.POSST (1.0 %) exceeds 2.1999999999999993 % for constant current from 747.096686592 to 5882.761325312 s
    The variation of DABA01_07R1_CV891.POSST (1.0 %) exceeds 1.9000000000000057 % for constant current from 6140.794719232 to 9740.260548864 s
    The variation of DABA02_07R1_CV891.POSST (1.0 %) exceeds 2.099999999999998 % for constant current from 747.096686592 to 5882.761325312 s
    The variation of DABA02_07R1_CV891.POSST (1.0 %) exceeds 1.5 % for constant current from 6140.794719232 to 9740.260548864 s





    <lhcsmapi.pyedsl.AssertionBuilder.AssertionBuilderSignalPlot at 0x7fdca38ab358>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_268_2.png)
    


- has_min_max_slope()


```python
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder
from lhcsmapi.pyedsl.AssertionBuilder import AssertionBuilder
from lhcsmapi.analysis.busbar.BusbarResistanceAnalysis import BusbarResistanceAnalysis

t_start = '2014-12-13 09:12:41+01:00'
t_end = '2014-12-13 12:27:11+01:00'

u_res_dfs = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start=t_start, t_end=t_end) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A12', system=['LEADS_EVEN', 'LEADS_ODD'], signal='U_RES') \
    .signal_query() \
    .synchronize_time() \
    .convert_index_to_sec() \
    .filter_median() \
    .dfs

i_meas_raw_df = QueryBuilder().with_nxcals(spark) \
    .with_duration(t_start=t_start, t_end=t_end) \
    .with_circuit_type('RB') \
    .with_metadata(circuit_name='RB.A12', system='PC', signal='I_MEAS') \
    .signal_query() \
    .dfs[0]

plateau_start, plateau_end = BusbarResistanceAnalysis.calculate_current_plateau_start_end([i_meas_raw_df], i_meas_threshold=500)

AssertionBuilder().with_signal(u_res_dfs) \
    .with_time_range(t_start=(plateau_start-i_meas_raw_df.index[0])/1e9, t_end=(plateau_end-i_meas_raw_df.index[0])/1e9) \
    .has_min_max_slope(slope_min=-2, slope_max=2)
```




    <lhcsmapi.pyedsl.AssertionBuilder.AssertionBuilderSignalPlot at 0x7fdca383e240>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_270_1.png)
    


In the case below, the allowed slope is too tight and the assertion fails 


```python
AssertionBuilder().with_signal(u_res_dfs) \
    .with_time_range(t_start=(plateau_start-i_meas_raw_df.index[0])/1e9, t_end=(plateau_end-i_meas_raw_df.index[0])/1e9) \
    .has_min_max_slope(slope_min=-2e-3, slope_max=2e-3)
```

    The drift of DFLAS.7L2.RB.A12.LD1:U_RES is -0.155 mV/h for constant current from 747.096686592 to 5882.761325312 s
    The drift of DFLAS.7L2.RB.A12.LD1:U_RES is -0.786 mV/h for constant current from 6140.794719232 to 9740.260548864 s
    The drift of DFLAS.7L2.RB.A12.LD2:U_RES is 0.197 mV/h for constant current from 747.096686592 to 5882.761325312 s
    The drift of DFLAS.7L2.RB.A12.LD2:U_RES is 0.760 mV/h for constant current from 6140.794719232 to 9740.260548864 s
    The drift of DFLAS.7R1.RB.A12.LD3:U_RES is 0.238 mV/h for constant current from 747.096686592 to 5882.761325312 s
    The drift of DFLAS.7R1.RB.A12.LD3:U_RES is 0.541 mV/h for constant current from 6140.794719232 to 9740.260548864 s
    The drift of DFLAS.7R1.RB.A12.LD4:U_RES is -0.230 mV/h for constant current from 747.096686592 to 5882.761325312 s
    The drift of DFLAS.7R1.RB.A12.LD4:U_RES is -0.615 mV/h for constant current from 6140.794719232 to 9740.260548864 s





    <lhcsmapi.pyedsl.AssertionBuilder.AssertionBuilderSignalPlot at 0x7fdca1beec18>




    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_272_2.png)
    


## 3.5. FeatureBuilder()

<center>{SIGNALS}.(FEATURE_CALCULATION).{ASSERTION}</center>

e.g.  
```python
FeatureBuilder().with_signal(u_hds_dfs) \
                .calculate_features(features=['first', 'last20mean', 'tau_charge'], index=1544622149599000000)
```
Supported functions are: 

```python
['first', 'first20mean', 'last', 'last20mean', 'max', 'min', 'median', 'std', 'mean', 'tau_charge', 'tau_energy', 'tau_lin_reg', 'tau_exp_fit']
```
For example, to calculate initial voltage, final mean voltage based on the last 20 points, and the characteristic time of the pseudo-exponential decay for a quench heater voltage.
<img src = "https://gitlab.cern.ch/LHCData/lhc-sm-api/raw/master/figures/QH.png">


```python
from lhcsmapi.pyedsl.PlotBuilder import PlotBuilder
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder
from lhcsmapi.pyedsl.FeatureBuilder import FeatureBuilder
import matplotlib.pyplot as plt

timestamp = 1544622149599000000

u_hds_dfs = QueryBuilder().with_pm() \
    .with_timestamp(timestamp) \
    .with_circuit_type('RQ') \
    .with_metadata(circuit_name='RQD.A12', system='QH', signal='U_HDS', source='16L2', wildcard={'CELL': '16L2'}) \
    .signal_query()\
    .synchronize_time(timestamp)\
    .convert_index_to_sec().dfs

PlotBuilder().with_signal(u_hds_dfs, title='QH Discharge in RQ', grid=True) \
    .with_ylabel(ylabel='U, [V]') \
    .with_xlabel(xlabel='time, [s]') \
    .plot()

FeatureBuilder().with_signal(u_hds_dfs) \
                .calculate_features(features=['first', 'last20mean', 'tau_charge'], index=1544622149599000000)
```


    
![png](https://gitlab.cern.ch/LHCData/lhc-sm-docs/-/raw/master/docs/api-user-guide/output_274_0.png)
    





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>16L2:U_HDS_1:first</th>
      <th>16L2:U_HDS_1:last20mean</th>
      <th>16L2:U_HDS_1:tau_charge</th>
      <th>16L2:U_HDS_2:first</th>
      <th>16L2:U_HDS_2:last20mean</th>
      <th>16L2:U_HDS_2:tau_charge</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1544622149599000000</th>
      <td>880.4621</td>
      <td>5.575086</td>
      <td>0.07779</td>
      <td>872.8354</td>
      <td>6.734703</td>
      <td>0.076704</td>
    </tr>
  </tbody>
</table>
</div>



## 4. Analysis
An analysis of a signal consists of two steps: signal query and processing. We follow a circuit-oriented approach; as a contrary to system-oriented approach. Following this approach we provide classes capable of querying (CircuitQuery class hierarchy) and processing (CircuitAnalysis class hierarchy) for each circuit. In case a system is the same across all circuit types (e.g., power converter, current leads), the common methods are stored in super classes. Custom processing of a system (e.g., PIC) or systems present only in some circuits (e.g., energy extraction) are handled in classes specific to each concerned circuit. In other words, common systems are treated in the same way and differences are treated for each circuit separately. This structure reflects the actual circuit arrangement. So far, the main dipole and quadrupole circuits were treated this way. This template will be applied to the remaining circuits.
<img src="https://sigmon.web.cern.ch/sites/sigmon.web.cern.ch/files/inline-images/analysis-query-class-diagram.png" width=75%>

All queries rely on the pyeDSL sub-package while the processing methods use the pyeDSL for repetitive tasks supported by dedicated methods for custom analyses. We apply a factory design pattern to instantiate an object matching the circuit type. The Analysis sub-package is used for both HWC and operation notebooks as well as signal monitoring applications.

## 5. GUI

- Browser for FPA
<img src="https://sigmon.web.cern.ch/sites/sigmon.web.cern.ch/files/inline-images/swan-rb-fpa-analysis-fgc-pm-browser.png">

- Browser for HWC
<img src="https://sigmon.web.cern.ch/sites/sigmon.web.cern.ch/files/inline-images/swan-hwc-browser.png">

