# Help and Feedback
Despite thorough testing, while using LHC-SM quench analysis notebooks two types of issues can occur, related to:
 
- analysis (e.g., wrong analysis results, corrupted plots, etc.); 
- SWAN (e.g., package installation problems, connection errors, service unavailability, etc.).

## 1. LHC Signal Monitoring
In order to provide feedback and ask for help regarding the analysis modules, you are cordially invited to contact the LHC Signal Monitoring team ([lhc-signal-monitoring@cern.ch](mailto:lhc-signal-monitoring@cern.ch)).

## 2. SWAN
There are three ways to contact SWAN support for help related to the service:

- Asking SWAN Community through a dedicated user forum: https://swan-community.web.cern.ch
- Creating a support SNOW ticket: https://cern.service-now.com/service-portal/function.do?name=swan
- Reporting a bug on dedicated JIRA platform: https://its.cern.ch/jira/projects/UCA/issues/UCA-359?filter=allopenissues

All three links are also available in the footer of SWAN website as shown below.

<center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/raw/master/figures/swan-help.png" width=75%></center>
