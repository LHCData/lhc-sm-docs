# Supported Circuits
## 1. RB - Main Dipole Circuit

<center><img src = "https://gitlab.cern.ch/LHCData/lhc-sm-hwc/raw/master/figures/rb/RB.png"></center>

<p>source: Powering Procedure and Acceptance Criteria for the 13 kA Dipole Circuits, MP3 Procedure, <a href="https://edms.cern.ch/document/874713">https://edms.cern.ch/document/874713</a></p>

|Type|Test|Current|Description|Notebook|Example report|
|----|----|-------|-----------|--------|--------------|
|HWC|QH IST|0|Quench heater IST|[HWC\_RB\_QHDA](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/qh/HWC_RB_QHDA.ipynb)|[HWC\_RB\_QHDA](#)|
|HWC|PIC2|I\_MIN\_OP|Interlock tests with PC connected to the leads|[AN\_RB\_PIC2](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rb/AN_RB_PIC2.ipynb)|[AN\_RB\_PIC2](https://edms.cern.ch/ui/file/2464883/1/AN_RB_PIC2.pdf)|
|HWC|PLI1.a2|I\_INJECTION|Current cycle to I\_INJECTION|[AN\_RB\_PLI1.a2](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rb/AN_RB_PLI1.a2.ipynb)|[AN\_RB\_PLI1.a2](https://edms.cern.ch/ui/file/2464883/1/AN_RB_PLI1.a2.pdf)|
|HWC|PLI1.b2|I\_INJECTION|Energy Extraction from QPS|[AN\_RB\_PLI1.b2](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rb/AN_RB_PLI1.b2.ipynb)|[AN\_RB\_PLI1.b2](https://edms.cern.ch/ui/file/2464883/1/AN_RB_PLI2.b2.pdf)|
|HWC|PLI1.d2|I\_INJECTION|Unipolar Powering Failure|[AN\_RB\_PLI1.d2](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rb/AN_RB_PLI1.d2.ipynb)|[AN\_RB\_PLI1.d2](https://edms.cern.ch/ui/file/2464883/1/AN_RB_PLI1.d2.pdf)|
|HWC|PLI2.s1|I\_INTERM\_1|Splice Mapping|[AN\_RB\_PLI2.s1](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rb/AN_RB_PLI2.s1.ipynb)|[AN\_RB\_PLI2.s1](https://edms.cern.ch/ui/file/2464883/1/AN_RB_PLI2.s1.pdf)|
|HWC|PLI2.b2|I\_INTERM\_1|Energy Extraction from PIC during the ramp|[AN\_RB\_PLI2.b2](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rb/AN_RB_PLI2.b2.ipynb)|[AN\_RB\_PLI2.b2](https://edms.cern.ch/ui/file/2464883/1/AN_RB_PLI2.b2.pdf)|
|HWC|PLI2.f1|I\_INTERM\_1|Quench heater provoked|[AN\_RB\_PLI2.f1](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rb/AN_RB_PLI2.f1.ipynb)|[AN\_RB\_PLI2.f1](https://edms.cern.ch/ui/file/2464883/1/AN_RB_PLI2.f1.pdf)|
|HWC|PLIM.b2|I\_SM\_INT\_4|Energy Extraction from QPS|[AN\_RB\_PLIM.b2](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rb/AN_RB_PLIM.b2.ipynb)|[AN\_RB\_PLIM.b2](https://edms.cern.ch/ui/file/2464883/1/AN_RB_PLIM.b2.pdf)|
|HWC|PLIS.s2|I\_SM|Splice Mapping|[AN\_RB\_PLIS.s2](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rb/AN_RB_PLIS.s2.ipynb)|[AN\_RB\_PLIS.s2](https://edms.cern.ch/ui/file/2464883/1/AN_RB_PLIS.s2.pdf)|
|HWC|PLI3.a5|I\_INTERM\_2|Current cycle to I\_INTERM\_2|[AN\_RB\_PLI3.a5](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rb/AN_RB_PLI3.a5.ipynb)|[AN\_RB\_PLI3.a5](https://edms.cern.ch/ui/file/2464883/1/AN_RB_PLI3.a5.pdf)|
|HWC|PLI3.d2|I\_INTERM\_2|Unipolar Powering Failure|[AN\_RB\_PLI3.d2](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rb/AN_RB_PLI3.d2.ipynb)|[AN\_RB\_PLI3.d2](https://edms.cern.ch/ui/file/2464883/1/AN_RB_PLI3.d2.pdf)|
|HWC|PNO.b2|I\_PNO+I\_DELTA|Energy Extraction from QPS|[AN\_RB\_PNO.b2](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rb/AN_RB_PNO.b2.ipynb)|[AN\_RB\_PNO.b2](https://edms.cern.ch/ui/file/2464883/1/AN_RB_PNO.b2.pdf)|
|HWC|PNO.a6|I\_PNO|Energy Extraction from QPS|[AN\_RB\_PNO.a6](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rb/AN_RB_PNO.a6.ipynb)|[AN\_RB\_PNO.a6](https://edms.cern.ch/ui/file/2464883/1/AN_RB_PNO.a6.pdf)|
|Operation|FPA|I\_PNO|FPA during operation with magnets quenching|[AN\_RB\_FPA](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rb/AN_RB_FPA.ipynb)|[AN\_RB\_FPA](https://edms.cern.ch/ui/file/2464883/1/AN_RB_FPA.pdf)|


## 2. RQ - Main Quadrupole Circuit
<center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/raw/master/figures/rq/RQ.png"></center>

<p>source: Test Procedure and Acceptance Criteria for the 13 kA Quadrupole (RQD-RQF) Circuits, MP3 Procedure, <a href="https://edms.cern.ch/document/874714">https://edms.cern.ch/document/874714</a></p>

|Type|Test|Current|Description|Notebook|Example report|
|----|----|-------|-----------|--------|--------------|
|HWC|QH IST|0|Quench heater IST|[HWC\_RQ\_QHDA](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/qh/HWC_RQ_QHDA.ipynb)|[HWC\_RQ\_QHDA](#)|
|HWC|PIC2|I\_MIN\_OP|Powering Interlock Controller|[AN\_RQ\_PIC2](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rq/AN_RQ_PIC2.ipynb)|[AN\_RQ\_PIC2](https://edms.cern.ch/ui/file/2464883/1/AN_RQ_PIC2.pdf)|
|HWC|PLI1.b3|I\_INJECTION|Energy Extraction from QPS|[AN\_RQ\_PLI1.b3](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rq/AN_RQ_PLI1.b3.ipynb)|[AN\_RQ\_PLI1.b3](https://edms.cern.ch/ui/file/2464883/1/AN_RQ_PLI1.b3.pdf)|
|HWC|PLI1.d2|I\_INJECTION|Unipolar Powering Failure|[AN\_RQ\_PLI1.d2](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rq/AN_RQ_PLI1.d2.ipynb)|[AN\_RQ\_PLI1.d2](https://edms.cern.ch/ui/file/2464883/1/AN_RQ_PLI1.d2.pdf)|
|HWC|PLI2.s1|I\_INTERM\_1|Splice Mapping|[AN\_RQ\_PLI2.s1](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rq/AN_RQ_PLI2.s1.ipynb)|[AN\_RQ\_PLI2.s1](https://edms.cern.ch/ui/file/2464883/1/AN_RQ_PLI2.s1.pdf)|
|HWC|PLI2.b3|I\_INTERM\_1|Energy Extraction from QPS|[AN\_RQ\_PLI2.b3](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rq/AN_RQ_PLI2.b3.ipynb)|[AN\_RQ\_PLI2.b3](https://edms.cern.ch/ui/file/2464883/1/AN_RQ_PLI2.b3.pdf)|
|HWC|PLI2.f1|I\_INTERM\_1|Heater provoked quench|[AN\_RQ\_PLI2.f1](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rq/AN_RQ_PLI2.f1.ipynb)|[AN\_RQ\_PLI2.f1](https://edms.cern.ch/ui/file/2464883/1/AN_RQ_PLI2.f1.pdf)|
|HWC|PLIM.b3|I\_SM\_INT\_4|Energy Extraction from QPS|[AN\_RQ\_PLIM.b3](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rq/AN_RQ_PLIM.b3.ipynb)|[AN\_RQ\_PLIM.b3](https://edms.cern.ch/ui/file/2464883/1/AN_RQ_PLIM.b3.pdf)|
|HWC|PLIS.s2|I\_SM|Splice Mapping at I_SM|[AN\_RQ\_PLIS.s2](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rq/AN_RQ_PLIS.s2.ipynb)|[AN\_RQ\_PLIS.s2](https://edms.cern.ch/ui/file/2464883/1/AN_RQ_PLIS.s2.pdf)|
|HWC|PLI3.a5|I\_SM, I\_INTERM_2|Current cycle to I\_INTERM_2|[AN\_RQ\_PLI3.a5](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rq/AN_RQ_PLI3.a5.ipynb)|[AN\_RQ\_PLI3.a5](https://edms.cern.ch/ui/file/2464883/1/AN_RQ_PLI3.a5.pdf)|
|HWC|PLI3.b3|I\_INTERM\_2|Energy Extraction from QPS|[AN\_RQ\_PLI3.b3](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rq/AN_RQ_PLI3.b3.ipynb)|[AN\_RQ\_PLI3.b3](https://edms.cern.ch/ui/file/2464883/1/AN_RQ_PLI3.b3.pdf)|
|HWC|PNO.b3|I\_PNO+I\_DELTA|Energy Extraction from QPS|[AN\_RQ\_PNO.b3](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rq/AN_RQ_PNO.b3.ipynb)|[AN\_RQ\_PNO.b3](https://edms.cern.ch/ui/file/2464883/1/AN_RQ_PNO.b3.pdf)|
|HWC|PNO.a6|I\_PNO|Current cycle to I\_PNO|[AN\_RQ\_PNO.a6](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rq/AN_RQ_PNO.a6.ipynb)|[AN\_RQ\_PNO.a6](https://edms.cern.ch/ui/file/2464883/1/AN_RQ_PNO.a6.pdf)|
|Operation|FPA|I\_PNO|FPA during operation with magnets quenching|[AN\_RQ\_FPA](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/rq/AN_RQ_FPA.ipynb)|[AN\_RQ\_FPA](https://edms.cern.ch/ui/file/2464883/1/AN_RQ_FPA.pdf)|

## 3. IT - Inner Triplet Circuits

The main quadrupole magnet circuits of the 8 Inner Triplet (IT) systems in the LHC are composed of four single aperture quadrupole magnets in series and have a particular powering configuration, consisting of three nested power converters (PC), see Figure below.

<center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/it/IT.png"></center>

Main quadrupole magnet circuit of the Inner Triplet system for IT’s at points 1 and 5 (left) and IT’s at points 2 and 8 (right).

Note that the configuration for the IT’s in points 1 and 5 is different from the configuration in points 2 and 8. An earth detection system is present at the minus of the RTQX2 converter. Detailed information concerning the converters is given in EDMS 1054483. 

The two magnets Q1 and Q3 are type MQXA and the two combined magnets Q2a and Q2b are type MQXB. Q1 is located towards the interaction point.

Note that the IT’s at points 2 and 8 have a slightly higher nominal operating current than the IT’s at points 1 and 5, see Table 1.


|Circuit|I\_PNO RQX|I\_PNO RTQX2|I\_PNO RTQX1|
|-------|----------|------------|------------|
|RQX.L2, RQX.R2, RQX.L8, RQX.R8|7180 A| 4780 A|550 A|
|RQX.L1, RQX.R1, RQX.L5, RQX.R5|6800 A| 4600 A|550 A|


Nominal operating currents for 7 TeV of the three PC’s as given in the LHC design report volume I. For the nominal current during HWC see EDMS 1375861.

source: Test Procedure and Acceptance Criteria for the Inner Triplet Circuits in the LHC, MP3 Procedure, <a href="https://edms.cern.ch/document/874886">https://edms.cern.ch/document/874886</a>

|Type|Test|Current|Description|Notebook|Example report|
|----|----|-------|-----------|--------|--------------|
|HWC|QH IST|0|Quench heater IST|[HWC\_IT\_QHDA](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/qh/HWC_IT_QHDA.ipynb)|[HWC\_IT\_QHDA](#)|
|HWC|PCC.t4|~|Power Converter Configuration part 2|AN\_IT\_PCC.t4|-|
|HWC|PIC2|~|Powering Interlock Controller check with standby current|[AN\_IT\_PIC2](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/it/AN_IT_PIC2.ipynb)|[AN\_IT\_PIC2](https://edms.cern.ch/ui/file/2464883/1/AN_IT_PIC2.pdf)|
|HWC|PNO.d12|10% of I\_PNO|Powering Failure at +10% of nominal current|AN\_IT\_PNO.D12|-|
|HWC|PNO.d13|10% of I\_PNO|Powering Failure at -10% of nominal current|AN\_IT\_PNO.D13|-|
|HWC|PLI3.f6|I_PLI3|Heater Discharge Request at 2nd intermediate current (Note that I\_RTQX1=0A|[AN\_IT\_PLI3.f6](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/it/AN_IT_PLI3.f6.ipynb)|[AN_IT_PLI3.f6](https://edms.cern.ch/ui/file/2464883/1/AN_IT_PLI3.f6.pdf)|
|HWC|PNO.d14|50% of I\_PNO|Powering Failure at +50% of nominal current during a SPA|[AN\_IT\_PNO.d14](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/it/AN_IT_PNO.d14.ipynb)|[AN_IT_PNO.d14](https://edms.cern.ch/ui/file/2464883/1/AN_IT_PNO.d14.pdf)|
|HWC|PNO.d15|50% of I\_PNO|Powering Failure at -50% of nominal current|[AN\_IT\_PNO.d15](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/it/AN_IT_PNO.d15.ipynb)|[AN_IT_PNO.d15](https://edms.cern.ch/ui/file/2464883/1/AN_IT_PNO.d15.pdf)|
|HWC|PNO.a9|I\_PNO+I\_DELTA|Training and plateau at nominal current|[AN\_IT\_PNO.a9](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/it/AN_IT_PNO.a9.ipynb)|[AN_IT_PNO.a9](https://edms.cern.ch/ui/file/2464883/1/AN_IT_PNO.a9.pdf)|
|HWC|PNO.d16|90% of I\_PNO|Powering Failure at +90% of nominal current|[AN\_IT\_PNO.d16](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/it/AN_IT_PNO.d16.ipynb)|[AN_IT_PNO.d16](https://edms.cern.ch/ui/file/2464883/1/AN_IT_PNO.d16.pdf)|
|HWC|PNO.d17|90% of I\_PNO|Powering Failure at -90% of nominal current|[AN\_IT\_PNO.d17](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/it/AN_IT_PNO.d17.ipynb)|[AN_IT_PNO.d17](https://edms.cern.ch/ui/file/2464883/1/AN_IT_PNO.d17.pdf)|
|Operation|FPA|I\_PNO|FPA during operation with magnets quenching|[AN\_IT\_FPA](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/it/AN_IT_FPA.ipynb)|[AN\_IT\_FPA](https://edms.cern.ch/ui/file/2464883/1/AN_IT_FPA.pdf)|
    

## 4. IPQ - Individually Powered 4-6 kA Quadrupole Circuits in the LHC Insertions

This section is a copy of a document created by Alexandre Erokhin (https://twiki.cern.ch/twiki/pub/MP3/Analysis_Manual_IPQ/MQM.pdf).
    
The Individually Powered Quadrupole magnets (IPQs) in the LHC are located on both sides of the Interaction Regions (IR), in the matching sector and in the dispersion suppressor. The IPQ circuits RQ4 to RQ7 are part of the matching sector, and the IPQ circuits RQ8 to RQ10 are part of the dispersion suppressor. The magnets Q4 to Q6 are operated at 
4.5 K, whereas the magnets Q7 to Q10 are operated at 1.9 K. 

|Magnets in the Circuit|Temperature|Position|General information|
|----------------------|-----------|--------|-------------------|
|2MQM|1.9 K| RQ7.L4, RQ7.R4|I Nominal: 5390A, I_Ultimate: 6820A|
| | | |L tot: 2x15 mH, L per aperture: 15 mH|
| | | |max(di/dt): 12.917 A/s|
|2x2MQM|1.9\4.5\* K| RQ7.L1, RQ7.R1 RQ7.L2, RQ7.R2 RQ7.L5, RQ7.R5 RQ7.L8, RQ7.R8 RQ5.L8\*, RQ5.R2\*|I Nominal: 5390A\4310A\*, I_Ultimate: 5820A\4650A\*|
| | | |L tot: 2x2x15 mH, L per aperture: 15 mH|
| | | |max(di/dt): 12.917 A/s|
|2MQML|1.9\4.5\* K| RQ5.L1\*, RQ5.R1\* RQ5.L5\*, RQ5.R5\* RQ6.L1\*, RQ6.R1\* RQ6.L5\*, RQ6.R5* RQ8.L1, RQ8.R1 RQ8.L2, RQ8.R2 RQ8.L4, RQ8.R4 RQ8.L5, RQ8.R5 RQ8.L6, RQ8.R6 RQ8.L8, RQ8.R8 RQ10.L1, RQ10.R1 RQ10.L2, RQ10.R2 RQ10.L4, RQ10.R4 RQ10.L5, RQ10.R5 RQ10.L6, RQ10.R6 RQ10.L8, RQ10.R8|I Nominal: 5390A \ 4310A\*, I_Ultimate: 5820A \ 4650A\*|
| | | |L tot: 2x21 mH, L per aperture: 21 mH|
| | | |max(di/dt): 12.917 A/s|
|2MQMC|1.9 K\4.5K\*| Does not exist as an individual power circuit (exists only in combination with 2MQM)|I Nominal: 5390A\4310A\*, I_Ultimate: 5820A\4650A\*|
| | | |L tot: 2x11 mH, L per aperture: 11 mH|
| | | |max(di/dt): 12.917 A/s|
|2MQM+2MQML|4.5 K| RQ6.L2, RQ6.R2 RQ6.L8, RQ6.R8|I Nominal: 4310A, I_Ultimate: 4650A|
| | | |L tot: 2x15 mH + 2x21 mH|
| | | |max(di/dt): 12.917 A/s|
|2MQM+2MQMC|1.9 K| RQ9.L1, RQ9.R1 RQ9.L2, RQ9.R2 RQ9.L4, RQ9.R4 RQ9.L5, RQ9.R5 RQ9.L6, RQ9.R6 RQ9.L8, RQ9.R8|I Nominal: 5390A, I_Ultimate: 5820A|
| | | |L tot: 2x15 mH + 2x21 mH|
| | | |max(di/dt): 12.917 A/s|

    
The MQM quadrupole consists of two individually powered apertures assembled in a common yoke structure. Depending on a subsector there are few kinds of power circuits:

- 2MQM

    <center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/ipq/2MQM_QPS.png" width=75%></center>

    Apertures B1 of 2 magnets are powered in series with one power supply Apertures B2 of 2 magnets are powered in series with second power supply. The return bus is common for both power circuits.

- 2x2MQM

    <center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/ipq/2x2MQM_QPS.png" width=75%></center>

    Apertures B1 of 4 magnets are powered in series with one power supply Apertures B2 of 4 magnets are powered in series with second power supply. The return bus is common for both power circuits.

- 2MQML – long version

    <center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/ipq/2MQML_QPS.png" width=75%></center>

    Apertures B1 of 2 magnets are powered in series with one power supply Apertures B2 of 2 magnets are powered in series with second power supply. The return bus is common for both power circuits.

- 2MQM + 2MQML

    <center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/ipq/2MQM_2MQML_QPS.png" width=75%></center>
  
    Apertures B1 of 2 MQM and 2 MQML are powered in series with one power supply Apertures B2 of 2 MQM and 2 MQML are powered in series with second power supply. The return bus is common for both power circuits.

- 2MQM + 2MQMC

    <center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/ipq/2MQM_2MQMC_QPS.png" width=75%></center>

    Apertures B1 of 2 MQM and 2 MQMC are powered in series with one power supply Apertures B2 of 2 MQM and 2 MQMC are powered in series with second power supply. The return bus is common for both power circuits.

Another subclass of IPQ magnets are MQYs.
This section is a copy of a document created by Alexandre Erokhin (https://twiki.cern.ch/twiki/pub/MP3/General_Info_IPQ/MQY.pdf).

|Magnets in the Circuit|Temperature|Position|General information|
|----------------------|-----------|--------|-------------------|
|2x2MQY|4.5 K| RQ4.L2, RQ5.L2 RQ4.R2, RQ4.L8, RQ4.R8, RQ5.R8|I Nominal: 3610A, I_Ultimate: 3900A|
| | | |L tot: 2x2x74 mH, L per aperture: 74 mH|
| | | |max(di/dt): 10.8 A/s|
|2MQY|4.5 K| RQ4.L1, RQ4.R1, RQ4.L2, RQ5.L2 RQ4.R2, RQ5.L4, RQ6.L4 RQ5.R4, RQ6.R4 RQ4.L5, RQ4.R5, RQ4.L6, RQ5.L6 RQ4.R6, RQ5.R6|I Nominal: 3610A, I_Ultimate: 3900A|
| | | |L tot: 2x74 mH, L per aperture: 74 mH|
| | | |max(di/dt): 10.8 A/s|

The MQY wide-aperture quadrupole consists of two individually powered apertures assembled in a common yoke structure. Depending from a Subsector there are two kinds of power circuits:

- 2MQY*
  
    <img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/ipq/2MQY_QPS.png" width=75%>
  
    Apertures B1 of 2 magnets are powered in series with one power supply.
    Apertures B2 of 2 magnets are powered in series with second power supply. 
    The return bus is common for both power circuits.
  
    Note: * in accordance with layout database 2 MQY means apertures B1 and B2 of the one magnet MQY type.

- 2x2MQY
  
    <img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/ipq/2x2MQY_QPS.png" width=75%>
  
    Apertures B1 of 4 magnets are powered in series with one power supply.
    Apertures B2 of 4 magnets are powered in series with second power supply. 
    The return bus is common for both power circuits.
    
**Quench Detector Type**

DQQDC – current leads quench detector

DQAMG – controller attached to global protection

**Current Leads:**
- Typical resistance for U_RES: 7 uOhm
- Threshold for U_HTS: 3 mV, 1 s
- Polarity convention: if I_B1 = I_B2 > 0, LD1:U_RES < 0, LD3:U_RES > 0
- PM file:
  - Buffer range: 0 to 250, event at point 50
  - Time range: -10 to 40 s
  - Frequency: 5 Hz (dt = 200 ms)

**Magnet:**
- See polarity convention here above
- U_RES_B1 = U_1_B1 + U_2_B1
- U_RES_B2 = U_1_B2 + U_2_B2
- Threshold on U_RES: 100 mV, 10 ms
- Attention: B1 signals & B2 signals can be shifted by 4ms from each other
- If pure inductive signal:
  - If dI/dt < 0:
    - U_1_Qx = Ldi / dt < 0
    - U_2_Qx = -Ldi / dt > 0
- PM file:
  - Buffer range: 501 to 1500, event at point 1000
  - Time range: -2 to 2s
  - Frequency: 250Hz (dt = 4ms)
  
The protection of the MQM quadrupole during a quench is assured by eight strip quench heaters placed on the outer layer of each coil octant. 
<img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/ipq/QH_topology.png" width=75%>
For redundancy, the heaters are connected in two circuits, such that each circuit covers all four poles and powered by independent power supplies.
<img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/ipq/QH_powering.png" width=75%>

The protection of the MQY quadrupole during a quench is assured by sixteen strip quench heaters of two different widths. Eight wide quench heaters are mounted between the second and third layers and the narrow heaters on the outer surface of the fourth layer.

<center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/ipq/QH_MQY_topology.png" width=75%></center>

This number of heaters is required to limit the voltage during quench in case of failure of some of the heaters. Both the inner and outer heaters are connected in two circuits, each circuit covering all four poles and powered by independent power supplies.

<center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/ipq/QH_MQY_powering.png" width=75%></center>

|Type|Test|Current|Description|Notebook|Example report|
|----|----|-------|-----------|--------|--------------|
|HWC|QH IST|0|Quench heater IST|[HWC\_IPQ\_QHDA](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/qh/HWC_IPQ_QHDA.ipynb)|[HWC\_IPQ\_QHDA](#)|
|HWC|PCC.3|I\_PCC|Power Converter Configuration|-|-|
|HWC|PIC2|I\_MIN\_OP|Powering Interlock Controller check with standby current in the circuits.|[AN\_IPQ\_PIC2](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/ipq/AN_IPQ_PIC2.ipynb)|[AN\_IPQ\_PIC2](https://edms.cern.ch/ui/file/2464883/1/AN_IPQ_PIC2.pdf)|
|HWC|PLI1.c3|I\_INJECTION|Fast Power Abort at injection current.|[AN\_IPQ\_PLI1.c3](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/ipq/AN_IPQ_PLI1.c3.ipynb)|[AN\_IPQ\_PLI1.c3](https://edms.cern.ch/ui/file/2464883/1/AN_IPQ_PLI1.c3.pdf)|
|HWC|PLI2.f3|I\_INTERM\_1|Unbalanced ramp and Quench Heater Firing|[AN\_IPQ\_PLI2.f3](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/ipq/AN_IPQ_PLI2.f3.ipynb)|[AN\_IPQ\_PLI2.f3](https://edms.cern.ch/ui/file/2464883/1/AN_IPQ_PLI2.f3.pdf)|
|HWC|PLI2.e3|I\_INTERM\_1|Unbalanced slow power abort|[AN\_IPQ\_PLI2.e3](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/ipq/AN_IPQ_PLI2.e3.ipynb)|[AN\_IPQ\_PLI2.e3](https://edms.cern.ch/ui/file/2464883/1/AN_IPQ_PLI2.e3.pdf)|
|HWC|PNO.f4|I\_PNO|Symmetric Ramp and Symmetric Quench Heater Firing|-|-|
|HWC|PNO.a7|I\_PNO+I\_DELTA|Powering to I\_PNO + I\_DELTA and unbalanced SPA|[AN\_IPQ\_PNO.a7](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/ipq/AN_IPQ_PNO.a7.ipynb)|[AN\_IPQ\_PNO.a7](https://edms.cern.ch/ui/file/2464883/1/AN_IPQ_PNO.a7.pdf)|
|HWC|PNO.c4|I\_PNO|Current Lead test and FPA from I\_PNO|[AN\_IPQ\_PNO.c4](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/ipq/AN_IPQ_PNO.c4.ipynb)|[AN\_IPQ\_PNO.c4](https://edms.cern.ch/ui/file/2464883/1/AN_IPQ_PNO.c4.pdf)|
|Operation|FPA|I\_PNO|FPA during operation with magnets quenching|[AN\_IPQ\_FPA](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/ipq/AN_IPQ_FPA.ipynb)|[AN\_IPQ\_FPA](https://edms.cern.ch/ui/file/2464883/1/AN_IPQ_FPA.pdf)|

source: Test Procedure and Acceptance Criteria for the Individually Powered 4-6 kA Quadrupole-Circuits in the LHC Insertions, MP3 Procedure, <a href="https://edms.cern.ch/document/874884">https://edms.cern.ch/document/874884</a>

## 5. IPD - Beam Separation Dipoles D1-D4
This section is a copy of a document created by Alexandre Erokhin [https://twiki.cern.ch/twiki/pub/MP3/General_Info_IPD/separation_dipole.pdf](https://twiki.cern.ch/twiki/pub/MP3/General_Info_IPD/separation_dipole.pdf)


|Magnets in the Circuit|Temperature|Position|General information|
|----------------------|-----------|--------|-------------------|
|MBX (D1)|1.9 K| RD1.L2, RD1.R2, RD1.L8, RD1.R8|I Nominal: 5800A, I_Ultimate: 6100A|
| | | |L tot: 26 mH, L per aperture: 26 mH|
| | | |max(di/dt): 17.453 A/s|
|MBRC (D2)|4.5 K| RD2.L1, RD2.R1, RD2.L5, RD2.R5|I Nominal: 4400A, I_Ultimate: 4670A|
| | | RD2.L2, RD2.R2, RD2.L8, RD2.R8|I Nominal: 6000A, I_Ultimate: 6500A|
| | | |L tot: 52 mH, L per aperture: 26 mH|
| | | |max(di/dt): 18.147 A/s|
|MBRS (D3)|4.5 K| RD3.L4, RD3.R4|I Nominal: 5520A, I_Ultimate: 6000A|
| | | |L tot: 26 mH, L per aperture: 26 mH|
| | | |max(di/dt): 18.147 A/s|
|MBRB (D4)|4.5 K| RD4.L4, RD4.R4|I Nominal: 5520A, I_Ultimate: 6000A|
| | | |L tot: 26 mH, L per aperture: 26 mH|
| | | |max(di/dt): 18.147 A/s|

Superconducting beam separation dipoles of four different types are required in the Experimental Insertions (IR 1, 2, 5 and 8) and the RF insertion (IR 4). Single aperture dipoles D1 (MBX) and twin aperture dipoles D2 (MBRC) are utilized in the Experimental Insertions. They bring the two beams of the LHC into collision at four separate points then separate the beams again beyond the collision point. In the RF Insertions two types of twin aperture dipoles, each type with two different aperture spacings are used: D3 (MBRS) and D4 (MBRB). The D3 and D4 magnets increase the separation of the beams in IR 4 from the nominal spacing 194 mm to 420 mm. D2 and D4 are the twin apertures magnets with common iron core for both apertures. D3 is a twin apertures magnet with independent iron cores for each aperture.


The MBRC dipole consists of two individually powered apertures assembled in a common yoke structure.

- MBX – D1  

    Single aperture of the magnet powered with one power supply.

    <center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/ipd/IPD_MBX_D1.png" width=75%></center>

- MBRC – D2  
- MBRB – D4  

    Apertures B1 and B2 of the magnet are powered in series with one power supply.

    <center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/ipd/IPD_MBRC_D2_MBRB_D4.png" width=75%></center>

- MBRS - D3  

    Apertures B1 and B2 of the magnet are powered in series with one power supply but series connection done in the DFBA.

    <center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/ipd/IPD_MBRS_D3.png" width=75%></center>

**Quench Detector Type**  
DQQDC - current leads quench detector  
DQAMG - controller attached to global protection  

**Current Leads:**
- Typical resistance for U_RES: 7 uOhm
- Threshold for U_HTS: 3 mV, 1s
- Polarity convention: Arrows show how signals are measured. If I > 0, LD1: U_RES > 0, LD2: U_RES < 0
- PM file
  - Buffer range 0 to 250, event at point 50
  - Time range: -10 to 40 s
  - Frequency: 5 Hz (dt = 200 ms)
  
**Magnet:**
- See polarity convention in the circuit schematics
- U_RES_B1 = U_1_B1 + U_2_B1
- Threshold on U_RES_B1: 100 mV, 10 ms
- U_RES_B2, U_1_B2, U_2_B2 and U_INDUCT_B2 are given for diagnostics only
- Signals are measured with -2.5 V offset and with the gain factor = 0.0012
- *Attention: B1 signals and B2 signals can be shifted by 4 ms from each other*
- If pure inductive signal and di/dt < 0:
  - U_1_B1 = L di/dt < 0
  - U_2_B1 = -L di/dt < 0
  
- PM file
  - Buffer range 501 to 1500, event at point 1000
  - Time range: -2 to 2 s
  - Frequency: 250 Hz (dt = 4 ms)

|Type|Test|Current|Description|Notebook|Example report|
|----|----|-------|-----------|--------|--------------|
|HWC|QH IST|0|Quench heater IST|[HWC\_IPD\_QHDA](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/qh/HWC_IPD_QHDA.ipynb)|[HWC\_IPD\_QHDA](#)|
|HWC|PCC.3|I\_PCC|Power Converter Configuration 1Q: Calibration of the PC|-|-|
|HWC|PIC2|I\_MIN\_OP|Powering Interlock Controller check with standby current in the circuits.|[AN\_IPD\_PIC2](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/ipd/AN_IPD_PIC2.ipynb)|[AN_IPD_PIC2](https://edms.cern.ch/ui/file/2464883/1/AN_IPD_PIC2.pdf)|
|HWC|PLI1.c2|I\_INJECTION|Fast Power Abort at injection current.|[AN\_IPD\_PLI1.c2](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/ipd/AN_IPD_PLI1.c2.ipynb)|[AN_IPD_PLI1.c2](https://edms.cern.ch/ui/file/2464883/1/AN_IPD_PLI1.c2.pdf)|
|HWC|PLI2.f2|I\_INTERM\_1|Heater Provoked Quench|[AN\_IPD\_PLI2.f2](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/ipd/AN_IPD_PLI2.f2.ipynb)|[AN_IPD_PLI2.f2](https://edms.cern.ch/ui/file/2464883/1/AN_IPD_PLI2.f2.pdf)|
|HWC|PLI3.c5|I\_INTERM\_3|Measurement of splice resistance and Fast Power Abort at intermediate current|[AN\_IPD\_PLI3.c5](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/ipd/AN_IPD_PLI3.c5.ipynb)|[AN_IPD_PLI3.c5](https://edms.cern.ch/ui/file/2464883/1/AN_IPD_PLI3.c5.pdf)|
|HWC|PNO.a8|I\_PNO+I\_DELTA|Powering to I\_PNO + I\_DELTA|[AN\_IPD\_PNO.a8](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/ipd/AN_IPD_PNO.a8.ipynb)|[AN_IPD_PNO.a8](https://edms.cern.ch/ui/file/2464883/1/AN_IPD_PNO.a8.pdf)|
|HWC|PNO.c6|I\_PNO|Fast Power Abort at Nominal Current and Lead Test|[AN\_IPD\_PNO.c6](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/ipd/AN_IPD_PNO.c6.ipynb)|[AN_IPD_PNO.c6](https://edms.cern.ch/ui/file/2464883/1/AN_IPD_PNO.c6.pdf)|
|Operation|FPA|I\_PNO|FPA during operation with magnets quenching|[AN\_IPD\_FPA](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/ipd/AN_IPD_FPA.ipynb)|[AN_IPD_FPA](https://edms.cern.ch/ui/file/2464883/1/AN_IPD_FPA.pdf)|

source: Test Procedure and Acceptance Criteria for the Separation Dipoles Circuits, MP3 Procedure, <a href="https://edms.cern.ch/document/874885">https://edms.cern.ch/document/874885</a>

## 6. 600A Circuits
The 600-A circuits come in one of two main variants: 

- circuits with 
- and without EE. 

Each variant may or may not be equipped with a DC contactor ensuring the effectiveness of the crowbar in case of a PC short circuit. Moreover, the magnets of several circuits are equipped with parallel resistors, in order to decouple the current decay in a quenching magnet from that in the rest of the circuit. Figure below shows a generic circuit diagram, equipped with EE and parallel resistor, as well as lead resistances and a quench resistance.

<center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/600A/600A.png" width=75%></center>

source: Test Procedure and Acceptance Criteria for the 600 A Circuits, MP3 Procedure, <a href="https://edms.cern.ch/document/874716">https://edms.cern.ch/document/874716</a>


Table below provides a list of circuits to be used with these analysis notebooks

|RCBX family|RCD/O family|Remaining 600A circuits with EE|Remaining 600A circuits without EE|
|-----------|------------|-------------------------------|----------------------------------|
|RCBXH1|RCD|RCS|RQS (RQS.L)|
|RCBXH2|RCO|RSS|RQSX3|
|RCBXH3| |ROD|RQT12|
|RCBXV1| |ROF|RQT13|
|RCBXV2| |RQTL9|RQTL7|
|RCBXV3| |RQS (RQS.A)|RQTL8|
| | |RQTD|RQTL10|
| | |RQTF|RQTL11|
| | |RSD1|
| | |RSD2|
| | |RSF1|
| | |RSF2|
| | |RU|

Another useful resource to find out which 600 A circuits belong to which category is the circuit tree on the MP3 website http://cern.ch/mp3

|Type|Test|Current|Description|Notebook|Example report|
|----|----|-------|-----------|--------|--------------|
|Operation|FPA|I\_PNO|FPA during operation with magnets quenching|[AN\_600A\_with\_without\_EE\_FPA](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/600A/AN_600A_with_without_EE_FPA.ipynb)|[AN\_600A\_with\_without\_EE\_FPA](https://edms.cern.ch/ui/file/2464883/1/AN_600A_with_without_EE_FPA.pdf)|
|Operation|FPA|I\_PNO|FPA during operation with magnets quenching|[AN\_600A\_RCDO\_FPA](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/600A/AN_600A_RCDO_FPA.ipynb)|[AN\_600A\_RCDO\_FPA](https://edms.cern.ch/ui/file/2464883/1/AN_600A_RCDO_FPA.pdf)|
|Operation|FPA|I\_PNO|FPA during operation with magnets quenching|[AN\_600A\_RCBXHV\_FPA](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/600A/AN_600A_RCBXHV_FPA.ipynb)|[AN\_600A\_RCBXHV\_FPA](https://edms.cern.ch/ui/file/2464883/1/AN_600A_RCBXHV_FPA.pdf)|

## 7. 80-120A Circuits
Figure below shows the electrical diagram of the 80-120 A corrector circuits including the connection to the PC. It’s important to note the positioning of the crowbar with respect to the DCCTs. During a power abort the current will transfer from the PC into the crowbar and the measured current (I_MEAS) goes immediately to almost 0 A, and is therefore not representative for the current in the cold part of the circuit including the magnet. Note that there is no QPS present in these circuits but that the PC will shut-down in case of overvoltage.

<center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/80-120A/80-120A.png" width=70%></center>

|Type|Test|Current|Description|Notebook|Example report|
|----|----|-------|-----------|--------|--------------|
|Operation|FPA|I\_PNO|FPA during operation with magnets quenching|[AN\_80-120A\_FPA](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/80-120A/AN_80-120A_FPA.ipynb)|[AN\_80-120A\_FPA](https://edms.cern.ch/ui/file/2464883/1/AN_80-120A_FPA.pdf)|

## 8. 60A Circuits
The LHC comprises a total of 376 pairs of horizontal and vertical orbit correctors which are installed at each focusing and defocusing main quadrupole magnet in the arcs. Quenches on 60 A magnets are detected by the power converter through magnet impedance growing. In addition, the power converter also provides current lead protection. The Figure below shows the circuit diagram of the 60 A arc orbit correctors.

<center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master/figures/60A/60A.png" width=70%></center>

|Type|Test|Current|Description|Notebook|Example report|
|----|----|-------|-----------|--------|--------------|
|Operation|FPA|I\_PNO|FPA during operation with magnets quenching|[AN\_60A\_FPA](https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/blob/master/60A/AN_60A_FPA.ipynb)|[AN\_60A\_FPA](https://edms.cern.ch/ui/file/2464883/1/AN_60A_FPA.pdf)|
