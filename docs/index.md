# Signal Monitoring Documentation

In this website we store user documentation of the LHC Signal Monitoring project. 
The website is divided into nine subpages with useful information about the project:

1. **Overview** - project overview with links to the main project repositories
2. **API Documentation** - a link to the API documentation auto-generated from code
3. **API User Guide** - an extensive user guide with numerous code examples of key API modules
4. **HWC&FPA Notebooks** - a description on how to use HWC and FPA notebooks
5. **HWC&FPA Circuits** - a list of supported notebooks for HWC and FPA
6. **Project Conventions** - a set of main project conventions followed during the development
7. **Development Process** - information on how the project is developed
8. **Help** - information on where to look for help
9. **FAQ** - a growing summary of frequently asked questions

For more information about the project itself, please visit [https://cern.ch/sigmon](https://cern.ch/sigmon).

We suggest to use the site `Search` feature in the top bar to look for a specific keyword.

The platform is based on mkdocs + GitLab. To edit this site just click the pencil symbol at the top-right.