# Signal Monitoring Project Overview

<center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-api/raw/master/figures/logo.png" width=25%></center>

The Signal Monitoring project consists of four elements:

1. API for logging db query and signal processing ([https://gitlab.cern.ch/lhcdata/lhc-sm-api](https://gitlab.cern.ch/lhcdata/lhc-sm-api))
2. Signal Monitoring notebooks ([https://gitlab.cern.ch/lhcdata/lhc-sm-apps](https://gitlab.cern.ch/lhcdata/lhc-sm-apps))
3. HWC and Operation notebooks ([https://gitlab.cern.ch/lhcdata/lhc-sm-hwc](https://gitlab.cern.ch/lhcdata/lhc-sm-hwc))
4. Scheduler for execution of HWC notebooks and monitoring applications ([https://gitlab.cern.ch/lhcdata/lhc-sm-scheduler](https://gitlab.cern.ch/lhcdata/lhc-sm-scheduler))

<center><img src="https://gitlab.cern.ch/LHCData/lhc-sm-api/raw/master/figures/lhc-sm-architecture.png" width=50%></center>